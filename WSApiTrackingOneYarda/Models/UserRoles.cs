﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("adm.tblUserRoles")]
    public class UserRoles
    {
        public const int ADMINISTRATOR = 0;
        public const int STORER = 1;
        public const int DRIVER = 2;
        public const int CLIENT_HOST = 3;
        public const int OPERATIVE = 4;
        public const int SUPER_ADMIN = 5;
        public const int CARRIER = 6;
        public const int TRAFFIC = 7;
        public const int YARD_MULE_DRIVER = 8;
        public const int PROVIDER = 9;

        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [Column(TypeName = "bigint")]
        public int auth { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        [Required]
        [StringLength(50)]
        public string label { get; set; }
    }
}
