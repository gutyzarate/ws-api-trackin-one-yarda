﻿using System;

namespace WSApiTrackingOneYarda.Models
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    internal class SqlDefaultValueAttribute : Attribute
    {
        public string DefaultValue { get; set; }
    }
}