﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblActivityLog")]
    public class ActivityLog
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(255)]
        public string log_name { get; set; }
        [StringLength(255)]
        public string description { get; set; }
        [StringLength(255)]
        public string comments { get; set; }
        [Column(TypeName = "bigint")]
        public int subject_id { get; set; }
        [StringLength(255)]
        public string subject_table { get; set; }
        [Column(TypeName = "bigint")]
        public int causer_id { get; set; }
        [StringLength(255)]
        public string causer_table { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? updated_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }

        public ActivityLog()
        {
            this.created_at = DateTime.Now;
            this.updated_at = DateTime.Now;
        }

        [ForeignKey("causer_id")]
        public virtual User user { get; set; }

        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", created_at);
            }
        }
        public string formatted_updated_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", updated_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", deleted_at);
            }
        }

    }
}
