﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblWarehouse")]
    public class Warehouse
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        public string name { get; set; }
        [Column(TypeName = "bigint")]
        public int? plant_id { get; set; }
        [Column(TypeName = "bigint")]
        public int? address_id { get; set; }
        public bool? deleted { get; set; }

        [ForeignKey("address_id")]
        public virtual Address address { get; set; }
    }
}
