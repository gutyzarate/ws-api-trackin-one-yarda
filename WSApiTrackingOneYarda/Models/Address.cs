﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblAddress")]
    public class Address
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        public int? zipcode { get; set; }
        [StringLength(100)]
        public string street { get; set; }
        public int? ext_number { get; set; }
        public int? int_number { get; set; }
        [StringLength(100)]
        public string neighborhood { get; set; }
        //[Range(typeof(decimal), "18","6")]
        public decimal? latitud { get; set; }
        //[Range(typeof(decimal), "18", "6")]
        public decimal? longitude { get; set; }
        [StringLength(50)]
        public string state { get; set; }
        [StringLength(50)]
        public string country { get; set; }
        [StringLength(50)]
        public string city { get; set; }
    }
}
