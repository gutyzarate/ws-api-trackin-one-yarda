﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    //[Table("TOY_Usuarios")]
    public class TOY_Usuarios
    {
        public long id { get; set; }
        public String usuario { get; set; }
        public String contrasena { get; set; }
        public String nombre { get; set; }
        public String apellidos { get; set; }
        public int perfil_id { get; set; }
        public DateTime creado_el { get; set; }
        public DateTime? actualizado_el { get; set; }
        public DateTime? eliminado_el { get; set; }
        [ForeignKey("perfil_id")]
        public TOY_Perfiles perfil { get; set; }
    }
}
