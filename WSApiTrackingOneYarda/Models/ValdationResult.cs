﻿using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace WSApiTrackingOneYarda.Models
{
    public class ValdationResult
    {
        public string Message { get; }
        public HttpError Errors { get; }

        public ValdationResult(string message, ModelStateDictionary modelState)
        {
            Message = message;
            Errors = new HttpError(modelState, includeErrorDetail: true).ModelState;
        }
    }
}
