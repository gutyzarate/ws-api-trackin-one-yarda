﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblIncidence")]
    public class Incidence
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [Column(TypeName = "bigint")]
        public int? object_id { get; set; }
        [StringLength(100)]
        public string table { get; set; }
        [Column(TypeName = "bigint")]
        public int client_id { get; set; }
        public string comments { get; set; }
        [Column(TypeName = "bigint")]
        public int creator_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? updated_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }


        [ForeignKey("client_id")]
        public virtual Client client { get; set; }
        [ForeignKey("creator_id")]
        public virtual User creator { get; set; }

        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", created_at);
            }
        }
        public string formatted_updated_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", updated_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", deleted_at);
            }
        }
    }
}