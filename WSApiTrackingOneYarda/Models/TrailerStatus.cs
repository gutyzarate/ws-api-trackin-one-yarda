﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblTrailerStatus")]
    public class TrailerStatus
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        [Required]
        [StringLength(50)]
        public string label { get; set; }
    }
}
