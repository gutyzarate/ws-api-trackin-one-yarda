﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    public class shipping_priority
    {
        public const int alta = 1;
        public const int media = 2;
        public const int baja = 3;
    }

    [Table("tblShippingSchedule")]
    public class ShippingSchedule
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        public bool? is_marked_as_rejected { get; set; }
        [Required]
        [StringLength(50)]
        public string shipping_number { get; set; } // required
        [Column(TypeName = "bigint")]
        public int? shipping_status_id { get; set; } // required // ForeignKey: tblShippingStatus -> OK!
        [Required]
        public DateTime scheduled_date { get; set; } // required
        [Required]
        [Column(TypeName = "bigint")]
        public int warehouse_id { get; set; }  // required // ForeignKey: tblWarehouse -> OK!
        [Required]
        [Column(TypeName = "bigint")]
        public int? carrier_id { get; set; }  // required // ForeignKey: tblUser -> OK!
        [Required]
        [Column(TypeName = "bigint")]
        public int? driver_id { get; set; } // required // ForeignKey: tblUser -> OK!
        [Required]
        [Column(TypeName = "bigint")]
        public int? client_id { get; set; }  // required // ForeignKey: tblUser -> OK!
        [Required]
        [StringLength(50)]
        public string trailer_number { get; set; } // required
        [Required]
        [StringLength(50)]
        public string truck_number { get; set; } // required
        [Required]
        [StringLength(50)]
        public string license_plate { get; set; } // required
        [Required]
        [StringLength(50)]
        public string container { get; set; }  // required
        [Required]
        [Column(TypeName = "bigint")]
        public int trailer_status_id { get; set; } // required // ForeignKey: tblTrailerStatus -> OK!
        [Required]
        [StringLength(50)]
        public string origin { get; set; }  // required
        [Required]
        [StringLength(50)]
        public string destination { get; set; }  // required
        [Column(TypeName = "bigint")]
        public int? initial_yard_mule_driver_id { get; set; } // ForeignKey: tblUser -> OK!
        [StringLength(50)]
        public string initial_parking_space { get; set; }
        [StringLength(50)]
        public string ramp { get; set; }
        [StringLength(50)]
        public string final_parking_space { get; set; }
        public int? shipping_priority { get; set; }
        [Column(TypeName = "bigint")]
        public int? final_yard_mule_driver_id { get; set; } // ForeignKey: tblUser -> OK!
        public DateTime? ramp_and_yard_mule_driver_assigned_at { get; set; }
        public DateTime? moved_from_parking_space_at { get; set; }
        public DateTime? in_ramp_at { get; set; }
        public DateTime? moved_from_ramp_at { get; set; }
        public DateTime? in_parking_space_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "bigint")]
        public int? creator_id { get; set; } // ForeignKey: tblUser -> OK!
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? updated_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }
        [Column(TypeName = "bigint")]
        public int? editor_id { get; set; } // ForeignKey: tblUser -> OK!


        [ForeignKey("shipping_status_id")]
        public virtual ShippingStatus shipping_status { get; set; }

        [ForeignKey("warehouse_id")]
        public virtual Warehouse warehouse { get; set; }

        [ForeignKey("carrier_id")]
        public virtual Carrier carrier { get; set; }

        [ForeignKey("driver_id")]
        public virtual Driver driver { get; set; }

        [ForeignKey("client_id")]
        public virtual Client client { get; set; }

        [ForeignKey("trailer_status_id")]
        public virtual TrailerStatus trailer_status { get; set; }

        [ForeignKey("initial_yard_mule_driver_id")]
        public virtual YardMuleDriver initial_yard_mule_driver { get; set; }

        [ForeignKey("final_yard_mule_driver_id")]
        public virtual YardMuleDriver final_yard_mule_driver { get; set; }

        [ForeignKey("creator_id")]
        public virtual User creator { get; set; }

        [ForeignKey("editor_id")]
        public virtual User editor { get; set; }

        public string formatted_scheduled_date
        {
            get
            {
                //return string.Format("{0:yyyy-MM-dd HH:mm:ss}", scheduled_date);
                return string.Format("{0:dd-MM-yyyy}", scheduled_date);
            }
        }
        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", created_at);
            }
        }
        public string formatted_updated_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", updated_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", deleted_at);
            }
        }
        public string formatted_ramp_and_yard_mule_driver_assigned_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", ramp_and_yard_mule_driver_assigned_at);
            }
        }
        public string formatted_moved_from_parking_space_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", moved_from_parking_space_at);
            }
        }
        public string formatted_in_ramp_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", in_ramp_at);
            }
        }
        public string formatted_moved_from_ramp_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", moved_from_ramp_at);
            }
        }
        public string formatted_in_parking_space_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", in_parking_space_at);
            }
        }

    }

    public class FilterShippingSchedule
    {
        public string shipping_number { get; set; }
        public string carrier_name { get; set; }
        public string license_plate { get; set; }
        public string container { get; set; }
        public string trailer_number { get; set; }
        public string truck_number { get; set; }
    }

    public class Scheduble
    {
        [Required]
        public int initial_yard_mule_driver_id { get; set; }
        [Required]
        public string initial_parking_space { get; set; }
        [Required]
        public string ramp { get; set; }
        [Required]
        public int final_yard_mule_driver_id { get; set; }
        [Required]
        public string final_parking_space { get; set; }
        [Required]
        public int shipping_priority { get; set; }
    }
}
