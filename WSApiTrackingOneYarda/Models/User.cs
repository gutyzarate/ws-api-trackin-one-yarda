﻿using onecore.security.utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using WSApiTrackingOneYarda.Data;

namespace WSApiTrackingOneYarda.Models
{
    public enum auth
    {
        ADMINISTRADOR,
        STORER,
        DRIVER,
        CLIENT_HOST,
        OPERATIVE,
        SUPER_ADMIN,
        CARRIER,
        TRAFFIC,
        YARD_MULE_DRIVER,
    }
    [Table("adm.tblUser")]
    public class User
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        public string name { get; set; }
        public string first_last_name { get; set; }
        public string second_last_name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        [Column(TypeName = "bigint")]
        public int? auth { get; set; } // ForeignKey: tblProfiles
        public bool? status { get; set; }
        [Column(TypeName = "bigint")]
        public int? contact_id { get; set; }
        [Column(TypeName = "bigint")]
        public int? company_id { get; set; }
        [NotMapped]
        public int? activity_log_id { get; set; }


        //[ForeignKey("auth")]
        //public virtual UserRoles user_role { get; set; }

        [ForeignKey("contact_id")]
        public virtual Contact contact { get; set; }

        [NotMapped]
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();
        [NotMapped]
        public virtual UserRoles user_role{

            get
            {
                return db.UserRoles.Where(t => t.auth == auth).FirstOrDefault();
            }
        }
        [NotMapped]
        private string encryptionKey = "0n3c0r33ncr1pt201804203i0qaw0e2e";
        [NotMapped]
        public string password_d {
            get { 
                return string.IsNullOrEmpty(password) ? null : EncryptService.Decrypt(password, encryptionKey);
            } 
        }

        // Virtuals Propertyes no mapping in DB
        public bool is_admin
        {
            get
            {
                return this.auth == UserRoles.ADMINISTRATOR ? true : false;
            }
        }
        public bool is_storer
        {
            get
            {
                return this.auth == UserRoles.STORER ? true : false;
            }
        }
        public bool is_driver
        {
            get
            {
                return this.auth == UserRoles.DRIVER ? true : false;
            }
        }
        public bool is_client_host
        {
            get
            {
                return this.auth == UserRoles.CLIENT_HOST ? true : false;
            }
        }
        public bool is_operative
        {
            get
            {
                return this.auth == UserRoles.OPERATIVE ? true : false;
            }
        }
        public bool is_super_admin
        {
            get
            {
                return this.auth == UserRoles.SUPER_ADMIN ? true : false;
            }
        }
        public bool is_carrier
        {
            get
            {
                return this.auth == UserRoles.CARRIER ? true : false;
            }
        }
        public bool is_traffic
        {
            get
            {
                return this.auth == UserRoles.TRAFFIC ? true : false;
            }
        }
        public bool is_yard_mule_driver
        {
            get
            {
                return this.auth == UserRoles.YARD_MULE_DRIVER ? true : false;
            }
        }
        public bool is_provider
        {
            get
            {
                return this.auth == UserRoles.PROVIDER ? true : false;
            }
        }
        // End Virtuals Propertyes no mapping in DB

    }
}
