﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    public class ActivityLogComment
    {
        [Key]
        public int id { get; set; }
        public string comment { get; set; }
    }
}