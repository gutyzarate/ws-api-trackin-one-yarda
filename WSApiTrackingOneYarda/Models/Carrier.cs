﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblCarrier")]
    public class Carrier
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(100)]
        public string name { get; set; }
        [StringLength(50)]
        public string rfc { get; set; }
        [Column(TypeName = "bigint")]
        public int? address_id { get; set; }
        [Column(TypeName = "bigint")]
        public int? user_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? updated_at { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }

        [ForeignKey("user_id")]
        public virtual User user { get; set; }
        [ForeignKey("address_id")]
        public virtual Address address { get; set; }

        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", created_at);
            }
        }
        public string formatted_updated_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", updated_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", deleted_at);
            }
        }
    }
}