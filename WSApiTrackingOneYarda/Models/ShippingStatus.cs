﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblShippingStatus")]
    public class ShippingStatus
    {
        public const int ON_REQUEST = 1;
        public const int RAMP_YARD_MULE_DRIVER_PENDING = 2;
        public const int IN_PARKING_SPACE = 3;
        public const int IN_RAMP = 4;
        public const int FINISHED_IN_PARKING_SPACE = 5;

        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        [Required]
        [StringLength(50)]
        public string label { get; set; }

    }
}
