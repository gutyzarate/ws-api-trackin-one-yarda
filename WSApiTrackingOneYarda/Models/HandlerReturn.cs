﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Results;

namespace WSApiTrackingOneYarda.Models
{
    public class HandlerReturn
    {     

        public HttpStatusCode status { get; }
        public bool error { get; set; }
        public string error_code { get; set; }
        public string message { get; set; }
        public string data { get; set; }

        public HandlerReturn(HttpStatusCode statusCode, bool error, string error_code, string message, string data = null)
        {
            this.status = statusCode;
            this.error = error;
            this.error_code = error_code;
            this.message = message;
            this.data = data == null ? null : data;
        }

    }
}