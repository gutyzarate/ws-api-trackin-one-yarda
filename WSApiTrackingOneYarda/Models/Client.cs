﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblClient")]
    public class Client
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(50)]
        public string rfc { get; set; }
        [StringLength(100)]
        public string name { get; set; }
        [StringLength(100)]
        public string first_lastname { get; set; }
        [StringLength(100)]
        public string second_lastname { get; set; }
        [Column(TypeName = "bigint")]
        public int? contact_id { get; set; }
        [Column(TypeName = "bigint")]
        public int? provider_id { get; set; }
        public bool? active { get; set; }

        [ForeignKey("contact_id")]
        public virtual Contact contact { get; set; }

        [ForeignKey("provider_id")]
        public virtual Provider provider { get; set; }
    }
}
