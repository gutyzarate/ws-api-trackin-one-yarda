﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblYardMuleDriver")]
    public class YardMuleDriver
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(50)]
        public string first_name { get; set; }
        [StringLength(50)]
        public string last_name { get; set; }
        [Column(TypeName = "bigint")]
        public int? incidence_id { get; set; }
        [StringLength(100)]
        public string social_security_number { get; set; }
        [StringLength(100)]
        public string driver_license_number { get; set; }
        [Column(TypeName = "bigint")]
        public int? user_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? updated_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }


        [ForeignKey("incidence_id")]
        public virtual Incidence incidence { get; set; }
        [ForeignKey("user_id")]
        public virtual User user { get; set; }

        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", created_at);
            }
        }
        public string formatted_updated_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", updated_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", deleted_at);
            }
        }

        //Virtuals
        [NotMapped]
        public virtual int? shipping_schedules_assigned { get; set; }
        public virtual string label
        {
            get
            {
                return first_name + " " + last_name + " (" + Convert.ToString(shipping_schedules_assigned == null ? 0 : shipping_schedules_assigned) + ")";
            }
        }
    }
}
