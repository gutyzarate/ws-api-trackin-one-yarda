﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    public class layoutErrors
    {
        public List<string> shipping_number { get; set; }
        public List<string> scheduled_date { get; set; }
        public List<string> warehouse_id { get; set; }
        public List<string> carrier_id { get; set; }
        public List<string> trailer_number { get; set; }
        public List<string> truck_number { get; set; }
        public List<string> license_plate { get; set; }
        public List<string> driver_id { get; set; }
        public List<string> initial_yard_mule_driver_id { get; set; }
        public List<string> trailer_status_id { get; set; }
        public List<string> client_id { get; set; }
        public List<string> origin { get; set; }
        public List<string> destination { get; set; }
        public List<string> container { get; set; }
    }
}