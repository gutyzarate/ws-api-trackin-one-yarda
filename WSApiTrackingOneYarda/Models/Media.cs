﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblMedia")]
    public class Media
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(100)]
        public string collection_name { get; set; }
        [StringLength(100)]
        public string name { get; set; }
        [StringLength(100)]
        public string mime_type { get; set; }
        [StringLength(100)]
        public string file_name { get; set; }
        [StringLength(50)]
        public string table { get; set; }
        [Column(TypeName = "bigint")]
        public int object_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }

        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss}", created_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss}", deleted_at);
            }
        }

    }

    public class FileMedia
    {
        public string collection { get; set; }
        public string name { get; set; }
        public string file_name { get; set; }
        public string uri { get; set; }
    }
}