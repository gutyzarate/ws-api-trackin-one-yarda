﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblDriver")]
    public class Driver
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string first_name { get; set; }
        [Required]
        [StringLength(50)]
        public string last_name { get; set; }
        [Column(TypeName = "bigint")]
        public int carrier_id { get; set; }
        [Required]
        [StringLength(100)]
        public string social_security_number { get; set; }
        [Required]
        [StringLength(100)]
        public string driver_license_number { get; set; }
        [Column(TypeName = "bigint")]
        public int? user_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? created_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? updated_at { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? deleted_at { get; set; }


        [ForeignKey("carrier_id")]
        public virtual Carrier carrier { get; set; }

        [ForeignKey("user_id")]
        public virtual User user { get; set; }

        public string formatted_created_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", created_at);
            }
        }
        public string formatted_updated_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", updated_at);
            }
        }
        public string formatted_deleted_at
        {
            get
            {
                return string.Format("{0:dd-MM-yyyy HH:mm:ss tt}", deleted_at);
            }
        }

    }

    public class Collection
    {
        public const string Picture = "picture";
        public const string Document = "document";
        public const string Singnature = "singnatures";
        public const string Evidence = "evidences";
    }
}