﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblContact")]
    public class Contact
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [Column(TypeName = "bigint")]
        public int? email_id { get; set; }
        [Column(TypeName = "bigint")]
        public int? address_id { get; set; }


        [ForeignKey("email_id")]
        public virtual Email email { get; set; }
        [ForeignKey("address_id")]
        public virtual Address address { get; set; }
    }
}