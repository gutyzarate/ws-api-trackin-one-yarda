﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblProvider")]
    public class Provider
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(100)]
        public string name { get; set; }
        [StringLength(15)]
        public string rfc { get; set; }
        [Column(TypeName = "bigint")]
        public int? address_id { get; set; }
        [Column(TypeName = "bigint")]
        public int? contact_id { get; set; }
        [StringLength(100)]
        public string activity { get; set; }
        [StringLength(30)]
        public string account { get; set; }
        [StringLength(100)]
        public string bank { get; set; }
        [StringLength(30)]
        public string clabe { get; set; }
        [StringLength(100)]
        public string certification { get; set; }
        public bool? deleted { get; set; }
        [Column(TypeName = "bigint")]
        public int? user_id { get; set; }


        [ForeignKey("address_id")]
        public virtual Address address { get; set; }

        [ForeignKey("contact_id")]
        public virtual Contact contact { get; set; }

        [ForeignKey("user_id")]
        public virtual User user { get; set; }
    }
}
