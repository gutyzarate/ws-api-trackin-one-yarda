﻿namespace WSApiTrackingOneYarda.Models.ModelResult
{
    public class Login
    {
        public int idClient { get; set; }
        public string RFCCliente { get; set; }
        public string Razon_Social { get; set; }
        public int idUsuarioCliente { get; set; }
        public int idPerfil { get; set; }
        public string Perfil { get; set; }
        public int Entidad { get; set; }
        public string TipoPerfil { get; set; }
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Correo_Electronico { get; set; }
    }
}
