﻿namespace WSApiTrackingOneYarda.Models.ModelResult
{
    public class Respuesta
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Value { get; set; }

        public Respuesta()
        {
            Success = false;
            Message = "Error";
        }
    }
}
