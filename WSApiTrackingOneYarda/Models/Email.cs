﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WSApiTrackingOneYarda.Models
{
    [Table("tblEmail")]
    public class Email
    {
        [Column(TypeName = "bigint")]
        public int id { get; set; }
        [StringLength(100)]
        public string email_address { get; set; }
    }
}