﻿using System.ComponentModel.DataAnnotations;

namespace WSApiTrackingOneYarda.Models
{
    public class Login
    {
        [Key]
        public string username { get; set; }
        public string password { get; set; }
    }
}
