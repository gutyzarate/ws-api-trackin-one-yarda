﻿using System;
using System.Data;
using System.Data.SqlClient;
using WSApiTrackingOneYarda.Models.ModelResult;

namespace WSApiTrackingOneYarda.Servicios
{
    public static class Login
    {
        private static string conn_string_perfiles = System.Configuration.ConfigurationManager.ConnectionStrings["PERFILES"].ConnectionString;

        public static Respuesta csp_GetInfoSession(int idUsuarioCliente)
        {

            var r = new Respuesta();
            try
            {

                using (SqlConnection conn = new SqlConnection(conn_string_perfiles))
                {

                    SqlCommand cmd = new SqlCommand("[csp_GetInfoSession]", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@idUsuarioCliente", idUsuarioCliente));


                    conn.Open();
                    var reader = cmd.ExecuteReader();
                    var login = new WSApiTrackingOneYarda.Models.ModelResult.Login();
                    while (reader.Read())
                    {
                        var id = reader[0];
                        login.idClient = !reader.IsDBNull(reader.GetOrdinal("idClient")) ? reader.GetInt32(reader.GetOrdinal("idClient")) : 0;
                        login.idUsuarioCliente = !reader.IsDBNull(reader.GetOrdinal("idUsuarioCliente")) ? reader.GetInt32(reader.GetOrdinal("idUsuarioCliente")) : 0;
                        login.idPerfil = !reader.IsDBNull(reader.GetOrdinal("idPerfil")) ? reader.GetInt32(reader.GetOrdinal("idPerfil")) : 0;
                        login.Entidad = !reader.IsDBNull(reader.GetOrdinal("Entidad")) ? reader.GetInt32(reader.GetOrdinal("Entidad")) : 0;
                        login.IdUsuario = !reader.IsDBNull(reader.GetOrdinal("IdUsuario")) ? reader.GetInt32(reader.GetOrdinal("IdUsuario")) : 0;

                        login.RFCCliente = !reader.IsDBNull(reader.GetOrdinal("RFCCliente")) ? reader.GetString(reader.GetOrdinal("RFCCliente")) : "";
                        login.Razon_Social = !reader.IsDBNull(reader.GetOrdinal("Razon_Social")) ? reader.GetString(reader.GetOrdinal("Razon_Social")) : "";
                        login.Perfil = !reader.IsDBNull(reader.GetOrdinal("Perfil")) ? reader.GetString(reader.GetOrdinal("Perfil")) : "";
                        login.TipoPerfil = !reader.IsDBNull(reader.GetOrdinal("TipoPerfil")) ? reader.GetString(reader.GetOrdinal("TipoPerfil")) : "";
                        login.Usuario = !reader.IsDBNull(reader.GetOrdinal("Usuario")) ? reader.GetString(reader.GetOrdinal("Usuario")) : "";
                        login.Nombre = !reader.IsDBNull(reader.GetOrdinal("Nombre")) ? reader.GetString(reader.GetOrdinal("Nombre")) : "";
                        login.Correo_Electronico = !reader.IsDBNull(reader.GetOrdinal("Correo_Electronico")) ? reader.GetString(reader.GetOrdinal("Correo_Electronico")) : "";


                    }
                    conn.Close();
                    r.Value = login;
                    r.Message = "Ok";
                    r.Success = true;

                }

            }
            catch (Exception ex)
            {

                r.Success = false;
                r.Message = ex.Message;
            }



            return r;
        }
    }
}
