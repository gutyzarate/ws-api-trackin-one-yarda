﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WSApiTrackingOneYarda.Data
{
    public class LogisticsTrackingDBContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public LogisticsTrackingDBContext() : base("name=LogisticsTrackingDB")
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public DbSet<Models.ShippingSchedule> ShippingSchedules { get; set; }
        public DbSet<Models.ShippingStatus> ShippingStatus { get; set; }
        public DbSet<Models.TrailerStatus> TrailerStatus { get; set; }
        public DbSet<Models.User> User { get; set; }
        public DbSet<Models.UserRoles> UserRoles { get; set; }
        public DbSet<Models.Warehouse> Warehouse { get; set; }
        public DbSet<Models.Client> Client { get; set; }
        public DbSet<Models.Login> Login { get; set; }
        public DbSet<Models.ActivityLog> ActivityLog { get; set; }
        public DbSet<Models.Media> Media { get; set; }
        public DbSet<Models.Carrier> Carrier { get; set; }
        public DbSet<Models.Driver> Driver { get; set; }
        public DbSet<Models.Incidence> Incidence { get; set; }
        public DbSet<Models.YardMuleDriver> YardMuleDriver { get; set; }
        public DbSet<Models.Provider> Provider { get; set; }
        public DbSet<Models.Address> Address { get; set; }
        public DbSet<Models.Contact> Contact { get; set; }
        public DbSet<Models.Email> Email { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // RELATIONSHIP FOREIGN

            // Carrier to User
            modelBuilder.Entity<Models.Carrier>()
                .HasOptional(t => t.user);
            // Carrier to Address
            modelBuilder.Entity<Models.Carrier>()
                .HasOptional(t => t.address);

            // Client to Contact
            modelBuilder.Entity<Models.Client>()
                .HasOptional(t => t.contact);
            // Client to Provider
            modelBuilder.Entity<Models.Client>()
                .HasOptional(t => t.provider);

            // Contact to Address
            modelBuilder.Entity<Models.Contact>()
                .HasOptional(t => t.address);

            // Driver to Carrier
            modelBuilder.Entity<Models.Driver>()
                .HasRequired(t => t.carrier)
                .WithMany()
                .WillCascadeOnDelete(false);
            // Driver to User
            modelBuilder.Entity<Models.Driver>()
                .HasOptional(t => t.user);

            // Incidence to Client
            modelBuilder.Entity<Models.Incidence>()
                .HasRequired(t => t.client)
                .WithMany()
                .WillCascadeOnDelete(false);
            // Incidence to Creator User
            modelBuilder.Entity<Models.Incidence>()
                .HasRequired(t => t.creator)
                .WithMany()
                .WillCascadeOnDelete(false);

            // Provider to Address
            modelBuilder.Entity<Models.Provider>()
                .HasOptional(t => t.address);
            // Provider to Contact
            modelBuilder.Entity<Models.Provider>()
                .HasOptional(t => t.contact);

            // ShippingSchedule to ShippingStatus
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasOptional(t => t.shipping_status);
            // ShippingSchedule to Warehouse
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasRequired(t => t.warehouse)
                .WithMany()
                .WillCascadeOnDelete(false);
            // ShippingSchedule to Carrier
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasRequired(t => t.carrier)
                .WithMany()
                .WillCascadeOnDelete(false);
            // ShippingSchedule to Driver
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasRequired(t => t.driver)
                .WithMany()
                .WillCascadeOnDelete(false);
            // ShippingSchedule to Client
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasRequired(t => t.client)
                .WithMany()
                .WillCascadeOnDelete(false);
            // ShippingSchedule to TrailerStatus
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasRequired(t => t.trailer_status)
                .WithMany()
                .WillCascadeOnDelete(false);
            // ShippingSchedule to YardMuleDriver
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasOptional(t => t.initial_yard_mule_driver);
            // ShippingSchedule to YardMuleDriver
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasOptional(t => t.final_yard_mule_driver);
            // ShippingSchedule to User Creator
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasOptional(t => t.creator);
            // ShippingSchedule to User Editor
            modelBuilder.Entity<Models.ShippingSchedule>()
                .HasOptional(t => t.editor);

            // User to UserRoles
            //modelBuilder.Entity<Models.User>()
            //    .HasOptional(t => t.user_role);
            // User to Contact
            modelBuilder.Entity<Models.User>()
                .HasOptional(t => t.contact);

            // Warehouse to Address
            modelBuilder.Entity<Models.Warehouse>()
                .HasOptional(t => t.address);

            // YardMuleDriver to Incidence One to One
            modelBuilder.Entity<Models.YardMuleDriver>()
                .HasOptional(y => y.incidence);
            modelBuilder.Entity<Models.YardMuleDriver>()
                .HasOptional(t => t.user);
            // END RELATIONSHIP FOREIGN


            //modelBuilder.Entity<ShippingSchedule>()
            //    .HasOptional(t => t.carrier)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            // STORED PROCEDURES

            modelBuilder.Entity<Models.ActivityLog>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_ActivityLog_Update"));
                s.Delete(d => d.HasName("TOY_ActivityLog_SoftDelete"));
                s.Insert(i => i.HasName("TOY_ActivityLog_Insert"));
            });

            modelBuilder.Entity<Models.Carrier>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Carrier_Update"));
                s.Delete(d => d.HasName("TOY_Carrier_SoftDelete"));
                s.Insert(i => i.HasName("TOY_Carrier_Insert"));
            });

            modelBuilder.Entity<Models.Client>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Client_Update"));
                s.Delete(d => d.HasName("TOY_Client_Delete"));
                s.Insert(i => i.HasName("TOY_Client_Insert"));
            });

            modelBuilder.Entity<Models.Driver>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Driver_Update"));
                s.Delete(d => d.HasName("TOY_Driver_SoftDelete"));
                s.Insert(i => i.HasName("TOY_Driver_Insert"));
            });

            modelBuilder.Entity<Models.Incidence>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Incidence_Update"));
                s.Delete(d => d.HasName("TOY_Incidence_SoftDelete"));
                s.Insert(i => i.HasName("TOY_Incidence_Insert"));
            });

            modelBuilder.Entity<Models.Media>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Media_Update"));
                s.Delete(d => d.HasName("TOY_Media_SoftDelete"));
                s.Insert(i => i.HasName("TOY_Media_Insert"));
            });

            modelBuilder.Entity<Models.Provider>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Provider_Update"));
                s.Delete(d => d.HasName("TOY_Provider_SoftDelete"));
                s.Insert(i => i.HasName("TOY_Provider_Insert"));
            });

            modelBuilder.Entity<Models.ShippingSchedule>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_ShippingSchedule_Update"));
                s.Delete(d => d.HasName("TOY_ShippingSchedule_SoftDelete"));
                s.Insert(i => i.HasName("TOY_ShippingSchedule_Insert"));
            });

            modelBuilder.Entity<Models.ShippingStatus>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_ShippingStatus_Update"));
                s.Delete(d => d.HasName("TOY_ShippingStatus_Delete"));
                s.Insert(i => i.HasName("TOY_ShippingStatus_Insert"));
            });

            modelBuilder.Entity<Models.TrailerStatus>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_TrailerStatus_Update"));
                s.Delete(d => d.HasName("TOY_TrailerStatus_Delete"));
                s.Insert(i => i.HasName("TOY_TrailerStatus_Insert"));
            });

            modelBuilder.Entity<Models.User>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_User_Update"));
                s.Delete(d => d.HasName("TOY_User_Delete"));
                s.Insert(i => i.HasName("TOY_User_Insert"));
            });

            modelBuilder.Entity<Models.UserRoles>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_UserRoles_Update"));
                s.Delete(d => d.HasName("TOY_UserRoles_Delete"));
                s.Insert(i => i.HasName("TOY_UserRoles_Insert"));
            });

            modelBuilder.Entity<Models.Warehouse>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Warehouse_Update"));
                s.Delete(d => d.HasName("TOY_Warehouse_SoftDelete"));
                s.Insert(i => i.HasName("TOY_Warehouse_Insert"));
            });

            modelBuilder.Entity<Models.YardMuleDriver>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_YardMuleDriver_Update"));
                s.Delete(d => d.HasName("TOY_YardMuleDriver_SoftDelete"));
                s.Insert(i => i.HasName("TOY_YardMuleDriver_Insert"));
            });

            modelBuilder.Entity<Models.Email>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Email_Update"));
                s.Delete(d => d.HasName("TOY_Email_Delete"));
                s.Insert(i => i.HasName("TOY_Email_Insert"));
            });

            modelBuilder.Entity<Models.Address>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Address_Update"));
                s.Delete(d => d.HasName("TOY_Address_Delete"));
                s.Insert(i => i.HasName("TOY_Address_Insert"));
            });

            modelBuilder.Entity<Models.Contact>().MapToStoredProcedures(s =>
            {
                s.Update(u => u.HasName("TOY_Contact_Update"));
                s.Delete(d => d.HasName("TOY_Contact_Delete"));
                s.Insert(i => i.HasName("TOY_Contact_Insert"));
            });

            // END STORED PROCEDURES

        }
    }
}
