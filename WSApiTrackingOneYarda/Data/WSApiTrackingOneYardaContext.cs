﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Data
{
    public class WSApiTrackingOneYardaContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public WSApiTrackingOneYardaContext() : base("name=DB_DesarrolloDB")
        {
        }

        public System.Data.Entity.DbSet<WSApiTrackingOneYarda.Models.TOY_Usuarios> TOY_Usuarios { get; set; }

        public System.Data.Entity.DbSet<WSApiTrackingOneYarda.Models.TOY_Perfiles> TOY_Perfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Entity<TOY_Usuarios>()
            //    .HasRequired(u => u.perfil)
            //    .WithMany(b => b.usuarios)
            //    .HasForeignKey(c => c.perfil_id);

            //modelBuilder.Entity<TOY_Usuarios>()
            //    .HasOptional(u => u.perfil_id);

            //modelBuilder.Entity<TOY_Perfiles>()
            //    .HasRequired(b => b.usuarios);

            modelBuilder.Entity<TOY_Usuarios>().MapToStoredProcedures();
        }
    }
}
