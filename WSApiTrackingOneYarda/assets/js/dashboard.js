function mostrarAlertDeGuardadoExitoso() {
    Swal.fire({
        text: 'Guardado correctamente.',
        icon: 'success',
        buttons: false,
        timer: 2000,
    });
}

$.fn.serializeObject = function () {
    "use strict";
    var a = {}, b = function (b, c) {
        var d = a[c.name];
        "undefined" != typeof d && d !== null ? $.isArray(d) ? d.push(c.value) : a[c.name] = [d, c.value] : a[c.name] = c.value
    };
    return $.each(this.serializeArray(), b), a
};

$(document).ready(function () {
    var loc = window.location.href.replace(/\/\s*$/, "");

    $('.sidebar .menu').find('a').each(function () {
        if ($(this).attr('href') == loc) {
            $(this).toggleClass('menu-item-active');

            if ($(this).parent().hasClass('has-submenu')) {
                $(this).parent().find('ul.submenu').addClass('active');
            }

            if ($(this).parent().parent().hasClass('submenu')) {
                console.log('si tiene class submenu');
                $(this).parent().parent().addClass('active');
                $(this).parent().parent().parent().find('>a').addClass('menu-item-active');
            }
        }
    });

    $('.menu-toggle').click(function (event) {
        event.preventDefault();
        $(this).toggleClass('active');

        $('.wrapper').toggleClass('menu-closed');
    });

    $('.datepicker').datetimepicker({
        //format: 'LT',
        //locale: 'es'
    });

    $('.form_date').datetimepicker({
        language: 'es',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        //forceParse: 0,
        format: 'dd-mm-yyyy'
    });

    $('.select2').select2({
        placeholder: 'Selecciona una opción...',
        allowClear: true,
        language: "es"
    });

    $('.modal').on('hidden.bs.modal', function () {
        //If there are any visible
        if ($(".modal:visible").length > 0) {
            //Slap the class on it (wait a moment for things to settle)
            //setTimeout(function() {
            $('body').addClass('modal-open');
            //},200)
        }
    });

    $(".profilephoto-inputfile").fileinput({
        language: "es"
    });

    if (document.getElementById('sign')) {
        var canvas = document.getElementById('sign');
        var signaturePad = new SignaturePad(canvas);

        $('#undoSignature').click(function (event) {
            event.preventDefault();
            var data = signaturePad.toData();

            if (data) {
                data.pop(); // remove the last dot or line
                signaturePad.fromData(data);
            }
        });

        $('#clearSignature').click(function (event) {
            event.preventDefault();
            signaturePad.clear();
        });

        $('#saveSignature').click(function (event) {
            event.preventDefault();
            if (signaturePad.isEmpty()) {
                alert('Ingresa una firma antes de continuar');
                return false;
            }

            var signature = signaturePad.toDataURL("image/png"); // save image as PNG
            var blob = dataURLToBlob(signature);
            var url = window.URL.createObjectURL(blob);
            console.log("save");
            console.log(url);
            window.URL.revokeObjectURL(url);
            signaturePad.clear();
        });
    }

    if (document.getElementById('shipments')) {
        var shipmentsChart = document.getElementById('shipments').getContext('2d');
        var schart = new Chart(shipmentsChart, {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                //labels: ['Día en curso'],
                datasets: [
                    {
                        label: 'Total de embarques programados',
                        data: [50],
                        backgroundColor: ['#138ba6']
                    },
                    {
                        label: 'Total de embarques cargados',
                        data: [20],
                        backgroundColor: ['#5658bc']
                    },
                    {
                        label: 'Total de embarques descargados',
                        data: [12],
                        backgroundColor: ['#8BC34A']
                    },
                    {
                        label: 'Total de embarques pendientes de documentos',
                        data: [5],
                        backgroundColor: ['#fd5a5b']
                    },
                    {
                        label: 'Total faltantes',
                        data: [8],
                        backgroundColor: ['#ffc312']
                    }]
            },

            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                legend: {
                    position: 'bottom'
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            //console.log(tooltipItem);
                            //console.log(datasetsItem);
                            return tooltipItem.yLabel;
                        },
                        title: function (tooltipItem, datasets) {
                            //console.log(tooltipItem);
                            //console.log(datasets);

                            return datasets.datasets[tooltipItem[0].datasetIndex].label;
                        }
                    }
                },

                /*events: false,
                showTooltips: false,
                animation: {
                    duration: 0,
                    onComplete: function () {
                        console.log('esto es this');
                        console.log(this);
                        console.log('esto es data');
                        console.log(this.data);
                        var ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset) {
                            console.log(dataset);
                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[0].data[0]._model;
                                ctx.fillText(dataset.data[i], model.x, model.y - 5);
                            }
                        });               
                    }
                }*/
            }
        });
    }

    if (document.getElementById('boxes')) {
        var boxesChart = document.getElementById('boxes').getContext('2d');
        var bchart = new Chart(boxesChart, {
            type: 'bar',

            data: {
                datasets: [
                    {
                        label: 'Cajones totales',
                        data: [100],
                        backgroundColor: ['#0651de']
                    },
                    {
                        label: 'Cajones disponibles',
                        data: [26],
                        backgroundColor: ['#9880fc']
                    },
                    {
                        label: 'Cajones usados',
                        data: [74],
                        backgroundColor: ['#feb938']
                    }]
            },

            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                legend: {
                    position: 'bottom'
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            //console.log(tooltipItem);
                            //console.log(datasetsItem);
                            return tooltipItem.yLabel;
                        },
                        title: function (tooltipItem, datasets) {
                            //console.log(tooltipItem);
                            //console.log(datasets);

                            return datasets.datasets[tooltipItem[0].datasetIndex].label;
                        }
                    }
                }
            }
        });
    }

    if (document.getElementById('ramps')) {
        var rampsChart = document.getElementById('ramps').getContext('2d');
        var rchart = new Chart(rampsChart, {
            type: 'bar',

            data: {
                datasets: [
                    {
                        label: 'Rampas totales',
                        data: [100],
                        backgroundColor: ['#843471']
                    },
                    {
                        label: 'Rampas disponibles',
                        data: [26],
                        backgroundColor: ['#138ba6']
                    },
                    {
                        label: 'Rampas usados',
                        data: [74],
                        backgroundColor: ['#ed2228']
                    }]
            },

            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                legend: {
                    position: 'bottom'
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            //console.log(tooltipItem);
                            //console.log(datasetsItem);
                            return tooltipItem.yLabel;
                        },
                        title: function (tooltipItem, datasets) {
                            //console.log(tooltipItem);
                            //console.log(datasets);

                            return datasets.datasets[tooltipItem[0].datasetIndex].label;
                        }
                    }
                }
            }
        });
    }

    if (document.getElementById('lift')) {
        var liftChart = document.getElementById('lift').getContext('2d');
        var lchart = new Chart(liftChart, {
            type: 'bar',

            data: {
                datasets: [
                    {
                        label: 'Montacargas totales',
                        data: [100],
                        backgroundColor: ['#0651de']
                    },
                    {
                        label: 'Montacargas disponibles',
                        data: [26],
                        backgroundColor: ['#9880fc']
                    },
                    {
                        label: 'Montacargas usados',
                        data: [74],
                        backgroundColor: ['#feb938']
                    }]
            },

            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                legend: {
                    position: 'bottom'
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            //console.log(tooltipItem);
                            //console.log(datasetsItem);
                            return tooltipItem.yLabel;
                        },
                        title: function (tooltipItem, datasets) {
                            //console.log(tooltipItem);
                            //console.log(datasets);

                            return datasets.datasets[tooltipItem[0].datasetIndex].label;
                        }
                    }
                }
            }
        });
    }

    // automaticaly open the select2 when it gets focus
    $(document).on('focus', '.select2', function () {
        $(this).siblings('select').select2('open');
    });

});

//if ($("#catalog-clients").length == 0) {
    $(document).ajaxStart(function () {
        $(".dataTables_scrollBody").prepend("<div class='loader-ajax'>Cargando, Por favor espere&nbsp;<i class='fas fa-spinner fa-pulse'></i></div>");
    });

$(document).ajaxStop(function () {
    $(".dataTables_scrollBody .loader-ajax").remove();
});
//}

function dataURLToBlob(dataURL) {
    // Code taken from https://github.com/ebidel/filer.js
    var parts = dataURL.split(';base64,');
    var contentType = parts[0].split(":")[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
