require = (function () { function r(e, n, t) { function o(i, f) { if (!n[i]) { if (!e[i]) { var c = "function" == typeof require && require; if (!f && c) return c(i, !0); if (u) return u(i, !0); var a = new Error("Cannot find module '" + i + "'"); throw a.code = "MODULE_NOT_FOUND", a } var p = n[i] = { exports: {} }; e[i][0].call(p.exports, function (r) { var n = e[i][1][r]; return o(n || r) }, p, p.exports, r, e, n, t) } return n[i].exports } for (var u = "function" == typeof require && require, i = 0; i < t.length; i++)o(t[i]); return o } return r })()({
    "./lang/es": [function (require, module, exports) {
        module.exports = {
            accepted: 'El campo debe ser aceptado.',
            after: 'El campo debe ser una fecha posterior a :after.',
            alpha: 'El campo solo debe contener letras.',
            alpha_dash: 'El campo solo debe contener letras, números y guiones.',
            alpha_num: 'El campo solo debe contener letras y números.',
            attributes: {},
            between: 'El campo tiene que estar entre :min - :max.',
            confirmed: 'La confirmación de no coincide.',
            different: 'El campo y :other deben ser diferentes.',
            digits: 'El campo debe tener :digits dígitos.',
            digits_between: 'El campo debe tener entre :min y :max dígitos.',
            email: 'El campo no es un correo válido.',
            in: 'El campo es inválido.',
            integer: 'El campo debe ser un número entero.',
            hex: 'El campo debe tener formato hexadecimal.',
            max: {
                numeric: 'El campo no debe ser mayor a :max.',
                string: 'El campo no debe ser mayor que :max caracteres.'
            },
            min: {
                numeric: 'El tamaño del campo debe ser de al menos :min.',
                string: 'El campo debe contener al menos :min caracteres.'
            },
            not_in: 'El campo es inválido.',
            numeric: 'El campo debe ser numérico.',
            present: 'El campo de debe estar presente (pero puede estar vacío).',
            regex: 'El formato del campo es inválido.',
            required: 'El campo es obligatorio.',
            required_if: 'El campo es obligatorio cuando :other es :value.',
            same: 'El campo y :other deben coincidir.',
            size: {
                numeric: 'El tamaño del campo debe ser :size.',
                string: 'El campo debe contener :size caracteres.'
            },
            url: 'El formato es inválido.'
        };

    }, {}]
}, {}, []);
