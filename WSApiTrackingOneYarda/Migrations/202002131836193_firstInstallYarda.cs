﻿namespace WSApiTrackingOneYarda.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstInstallYarda : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tblActivityLog",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        log_name = c.String(maxLength: 255),
                        description = c.String(maxLength: 255),
                        subject_id = c.Long(nullable: false),
                        subject_table = c.String(maxLength: 255),
                        causer_id = c.Long(nullable: false),
                        causer_table = c.String(maxLength: 255),
                        created_at = c.DateTime(),
                        updated_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.tblUser", t => t.causer_id, cascadeDelete: true)
                .Index(t => t.causer_id);
            
            CreateTable(
                "adm.tblUser",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(),
                        first_last_name = c.String(),
                        second_last_name = c.String(),
                        username = c.String(),
                        password = c.String(),
                        auth = c.Long(),
                        status = c.Boolean(),
                        contact_id = c.Long(),
                        company_id = c.Long(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblContact", t => t.contact_id)
                .Index(t => t.contact_id);
            
            CreateTable(
                "dbo.tblContact",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        email_id = c.Long(),
                        address_id = c.Long(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblAddress", t => t.address_id)
                .ForeignKey("dbo.tblEmail", t => t.email_id)
                .Index(t => t.email_id)
                .Index(t => t.address_id);
            
            CreateTable(
                "dbo.tblAddress",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        zipcode = c.Int(),
                        street = c.String(maxLength: 100),
                        ext_number = c.Int(),
                        int_number = c.Int(),
                        neighborhood = c.String(maxLength: 100),
                        latitud = c.Decimal(precision: 18, scale: 2),
                        longitude = c.Decimal(precision: 18, scale: 2),
                        state = c.String(maxLength: 50),
                        country = c.String(maxLength: 50),
                        city = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tblEmail",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        email_address = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tblCarrier",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(maxLength: 100),
                        rfc = c.String(maxLength: 50),
                        address_id = c.Long(),
                        user_id = c.Long(),
                        created_at = c.DateTime(),
                        updated_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblAddress", t => t.address_id)
                .ForeignKey("adm.tblUser", t => t.user_id)
                .Index(t => t.address_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.tblClient",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        rfc = c.String(maxLength: 50),
                        name = c.String(maxLength: 100),
                        first_lastname = c.String(maxLength: 100),
                        second_lastname = c.String(maxLength: 100),
                        contact_id = c.Long(),
                        provider_id = c.Long(),
                        active = c.Boolean(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblContact", t => t.contact_id)
                .ForeignKey("dbo.tblProvider", t => t.provider_id)
                .Index(t => t.contact_id)
                .Index(t => t.provider_id);
            
            CreateTable(
                "dbo.tblProvider",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(maxLength: 100),
                        rfc = c.String(maxLength: 15),
                        address_id = c.Long(),
                        contact_id = c.Long(),
                        activity = c.String(maxLength: 100),
                        account = c.String(maxLength: 30),
                        bank = c.String(maxLength: 100),
                        clabe = c.String(maxLength: 30),
                        certification = c.String(maxLength: 100),
                        deleted = c.Boolean(),
                        user_id = c.Long(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblAddress", t => t.address_id)
                .ForeignKey("dbo.tblContact", t => t.contact_id)
                .ForeignKey("adm.tblUser", t => t.user_id)
                .Index(t => t.address_id)
                .Index(t => t.contact_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.tblDriver",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        first_name = c.String(nullable: false, maxLength: 50),
                        last_name = c.String(nullable: false, maxLength: 50),
                        carrier_id = c.Long(nullable: false),
                        social_security_number = c.String(nullable: false, maxLength: 100),
                        driver_license_number = c.String(nullable: false, maxLength: 100),
                        user_id = c.Long(),
                        created_at = c.DateTime(),
                        updated_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblCarrier", t => t.carrier_id)
                .ForeignKey("adm.tblUser", t => t.user_id)
                .Index(t => t.carrier_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.tblIncidence",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        object_id = c.Long(),
                        table = c.String(maxLength: 100),
                        client_id = c.Long(nullable: false),
                        comments = c.String(),
                        creator_id = c.Long(nullable: false),
                        created_at = c.DateTime(),
                        updated_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblClient", t => t.client_id)
                .ForeignKey("adm.tblUser", t => t.creator_id)
                .Index(t => t.client_id)
                .Index(t => t.creator_id);
            
            CreateTable(
                "dbo.Login",
                c => new
                    {
                        username = c.String(nullable: false, maxLength: 128),
                        password = c.String(),
                    })
                .PrimaryKey(t => t.username);
            
            CreateTable(
                "dbo.tblMedia",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        collection_name = c.String(maxLength: 100),
                        name = c.String(maxLength: 100),
                        mime_type = c.String(maxLength: 100),
                        file_name = c.String(maxLength: 100),
                        table = c.String(maxLength: 50),
                        object_id = c.Long(nullable: false),
                        created_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tblShippingSchedule",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        is_marked_as_rejected = c.Boolean(),
                        shipping_number = c.String(nullable: false, maxLength: 50),
                        shipping_status_id = c.Long(),
                        scheduled_date = c.DateTime(nullable: false),
                        warehouse_id = c.Long(nullable: false),
                        carrier_id = c.Long(nullable: false),
                        driver_id = c.Long(nullable: false),
                        client_id = c.Long(nullable: false),
                        trailer_number = c.String(nullable: false, maxLength: 50),
                        truck_number = c.String(nullable: false, maxLength: 50),
                        license_plate = c.String(nullable: false, maxLength: 50),
                        container = c.String(nullable: false, maxLength: 50),
                        trailer_status_id = c.Long(nullable: false),
                        origin = c.String(nullable: false, maxLength: 50),
                        destination = c.String(nullable: false, maxLength: 50),
                        initial_yard_mule_driver_id = c.Long(),
                        initial_parking_space = c.String(maxLength: 50),
                        ramp = c.String(maxLength: 50),
                        final_parking_space = c.String(maxLength: 50),
                        shipping_priority = c.Int(),
                        final_yard_mule_driver_id = c.Long(),
                        ramp_and_yard_mule_driver_assigned_at = c.DateTime(),
                        moved_from_parking_space_at = c.DateTime(),
                        in_ramp_at = c.DateTime(),
                        moved_from_ramp_at = c.DateTime(),
                        in_parking_space_at = c.DateTime(),
                        created_at = c.DateTime(),
                        creator_id = c.Long(),
                        updated_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                        editor_id = c.Long(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblCarrier", t => t.carrier_id)
                .ForeignKey("dbo.tblClient", t => t.client_id)
                .ForeignKey("adm.tblUser", t => t.creator_id)
                .ForeignKey("dbo.tblDriver", t => t.driver_id)
                .ForeignKey("adm.tblUser", t => t.editor_id)
                .ForeignKey("dbo.tblYardMuleDriver", t => t.final_yard_mule_driver_id)
                .ForeignKey("dbo.tblYardMuleDriver", t => t.initial_yard_mule_driver_id)
                .ForeignKey("dbo.tblShippingStatus", t => t.shipping_status_id)
                .ForeignKey("dbo.tblTrailerStatus", t => t.trailer_status_id)
                .ForeignKey("dbo.tblWarehouse", t => t.warehouse_id)
                .Index(t => t.shipping_status_id)
                .Index(t => t.warehouse_id)
                .Index(t => t.carrier_id)
                .Index(t => t.driver_id)
                .Index(t => t.client_id)
                .Index(t => t.trailer_status_id)
                .Index(t => t.initial_yard_mule_driver_id)
                .Index(t => t.final_yard_mule_driver_id)
                .Index(t => t.creator_id)
                .Index(t => t.editor_id);
            
            CreateTable(
                "dbo.tblYardMuleDriver",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        first_name = c.String(maxLength: 50),
                        last_name = c.String(maxLength: 50),
                        incidence_id = c.Long(),
                        social_security_number = c.String(maxLength: 100),
                        driver_license_number = c.String(maxLength: 100),
                        user_id = c.Long(),
                        created_at = c.DateTime(),
                        updated_at = c.DateTime(),
                        deleted_at = c.DateTime(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblIncidence", t => t.incidence_id)
                .ForeignKey("adm.tblUser", t => t.user_id)
                .Index(t => t.incidence_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.tblShippingStatus",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50),
                        label = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tblTrailerStatus",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50),
                        label = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.tblWarehouse",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(),
                        plant_id = c.Long(),
                        address_id = c.Long(),
                        deleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tblAddress", t => t.address_id)
                .Index(t => t.address_id);
            
            CreateTable(
                "adm.tblUserRoles",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        auth = c.Long(nullable: false),
                        name = c.String(nullable: false, maxLength: 50),
                        label = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateStoredProcedure(
                "dbo.TOY_ActivityLog_Insert",
                p => new
                    {
                        log_name = p.String(maxLength: 255),
                        description = p.String(maxLength: 255),
                        subject_id = p.Long(),
                        subject_table = p.String(maxLength: 255),
                        causer_id = p.Long(),
                        causer_table = p.String(maxLength: 255),
                    },
                body:
                    @"INSERT [dbo].[tblActivityLog]([log_name], [description], [subject_id], [subject_table], [causer_id], [causer_table])
                      VALUES (@log_name, @description, @subject_id, @subject_table, @causer_id, @causer_table)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblActivityLog]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblActivityLog] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ActivityLog_Update",
                p => new
                    {
                        id = p.Long(),
                        log_name = p.String(maxLength: 255),
                        description = p.String(maxLength: 255),
                        subject_id = p.Long(),
                        subject_table = p.String(maxLength: 255),
                        causer_id = p.Long(),
                        causer_table = p.String(maxLength: 255),
                    },
                body:
                    @"UPDATE [dbo].[tblActivityLog]
                      SET [log_name] = @log_name, [description] = @description, [subject_id] = @subject_id, [subject_table] = @subject_table, [causer_id] = @causer_id, [causer_table] = @causer_table
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblActivityLog] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ActivityLog_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblActivityLog]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_User_Insert",
                p => new
                    {
                        name = p.String(),
                        first_last_name = p.String(),
                        second_last_name = p.String(),
                        username = p.String(),
                        password = p.String(),
                        auth = p.Long(),
                        status = p.Boolean(),
                        contact_id = p.Long(),
                        company_id = p.Long(),
                    },
                body:
                    @"INSERT [adm].[tblUser]([name], [first_last_name], [second_last_name], [username], [password], [auth], [status], [contact_id], [company_id])
                      VALUES (@name, @first_last_name, @second_last_name, @username, @password, @auth, @status, @contact_id, @company_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [adm].[tblUser]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [adm].[tblUser] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_User_Update",
                p => new
                    {
                        id = p.Long(),
                        name = p.String(),
                        first_last_name = p.String(),
                        second_last_name = p.String(),
                        username = p.String(),
                        password = p.String(),
                        auth = p.Long(),
                        status = p.Boolean(),
                        contact_id = p.Long(),
                        company_id = p.Long(),
                    },
                body:
                    @"UPDATE [adm].[tblUser]
                      SET [name] = @name, [first_last_name] = @first_last_name, [second_last_name] = @second_last_name, [username] = @username, [password] = @password, [auth] = @auth, [status] = @status, [contact_id] = @contact_id, [company_id] = @company_id
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_User_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [adm].[tblUser]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Contact_Insert",
                p => new
                    {
                        email_id = p.Long(),
                        address_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblContact]([email_id], [address_id])
                      VALUES (@email_id, @address_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblContact]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblContact] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Contact_Update",
                p => new
                    {
                        id = p.Long(),
                        email_id = p.Long(),
                        address_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblContact]
                      SET [email_id] = @email_id, [address_id] = @address_id
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Contact_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblContact]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Address_Insert",
                p => new
                    {
                        zipcode = p.Int(),
                        street = p.String(maxLength: 100),
                        ext_number = p.Int(),
                        int_number = p.Int(),
                        neighborhood = p.String(maxLength: 100),
                        latitud = p.Decimal(precision: 18, scale: 2),
                        longitude = p.Decimal(precision: 18, scale: 2),
                        state = p.String(maxLength: 50),
                        country = p.String(maxLength: 50),
                        city = p.String(maxLength: 50),
                    },
                body:
                    @"INSERT [dbo].[tblAddress]([zipcode], [street], [ext_number], [int_number], [neighborhood], [latitud], [longitude], [state], [country], [city])
                      VALUES (@zipcode, @street, @ext_number, @int_number, @neighborhood, @latitud, @longitude, @state, @country, @city)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblAddress]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblAddress] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Address_Update",
                p => new
                    {
                        id = p.Long(),
                        zipcode = p.Int(),
                        street = p.String(maxLength: 100),
                        ext_number = p.Int(),
                        int_number = p.Int(),
                        neighborhood = p.String(maxLength: 100),
                        latitud = p.Decimal(precision: 18, scale: 2),
                        longitude = p.Decimal(precision: 18, scale: 2),
                        state = p.String(maxLength: 50),
                        country = p.String(maxLength: 50),
                        city = p.String(maxLength: 50),
                    },
                body:
                    @"UPDATE [dbo].[tblAddress]
                      SET [zipcode] = @zipcode, [street] = @street, [ext_number] = @ext_number, [int_number] = @int_number, [neighborhood] = @neighborhood, [latitud] = @latitud, [longitude] = @longitude, [state] = @state, [country] = @country, [city] = @city
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Address_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblAddress]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Email_Insert",
                p => new
                    {
                        email_address = p.String(maxLength: 100),
                    },
                body:
                    @"INSERT [dbo].[tblEmail]([email_address])
                      VALUES (@email_address)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblEmail]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblEmail] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Email_Update",
                p => new
                    {
                        id = p.Long(),
                        email_address = p.String(maxLength: 100),
                    },
                body:
                    @"UPDATE [dbo].[tblEmail]
                      SET [email_address] = @email_address
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Email_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblEmail]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Carrier_Insert",
                p => new
                    {
                        name = p.String(maxLength: 100),
                        rfc = p.String(maxLength: 50),
                        address_id = p.Long(),
                        user_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblCarrier]([name], [rfc], [address_id], [user_id])
                      VALUES (@name, @rfc, @address_id, @user_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblCarrier]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblCarrier] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Carrier_Update",
                p => new
                    {
                        id = p.Long(),
                        name = p.String(maxLength: 100),
                        rfc = p.String(maxLength: 50),
                        address_id = p.Long(),
                        user_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblCarrier]
                      SET [name] = @name, [rfc] = @rfc, [address_id] = @address_id, [user_id] = @user_id
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblCarrier] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Carrier_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblCarrier]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Client_Insert",
                p => new
                    {
                        rfc = p.String(maxLength: 50),
                        name = p.String(maxLength: 100),
                        first_lastname = p.String(maxLength: 100),
                        second_lastname = p.String(maxLength: 100),
                        contact_id = p.Long(),
                        provider_id = p.Long(),
                        active = p.Boolean(),
                    },
                body:
                    @"INSERT [dbo].[tblClient]([rfc], [name], [first_lastname], [second_lastname], [contact_id], [provider_id], [active])
                      VALUES (@rfc, @name, @first_lastname, @second_lastname, @contact_id, @provider_id, @active)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblClient]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblClient] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Client_Update",
                p => new
                    {
                        id = p.Long(),
                        rfc = p.String(maxLength: 50),
                        name = p.String(maxLength: 100),
                        first_lastname = p.String(maxLength: 100),
                        second_lastname = p.String(maxLength: 100),
                        contact_id = p.Long(),
                        provider_id = p.Long(),
                        active = p.Boolean(),
                    },
                body:
                    @"UPDATE [dbo].[tblClient]
                      SET [rfc] = @rfc, [name] = @name, [first_lastname] = @first_lastname, [second_lastname] = @second_lastname, [contact_id] = @contact_id, [provider_id] = @provider_id, [active] = @active
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Client_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblClient]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Provider_Insert",
                p => new
                    {
                        name = p.String(maxLength: 100),
                        rfc = p.String(maxLength: 15),
                        address_id = p.Long(),
                        contact_id = p.Long(),
                        activity = p.String(maxLength: 100),
                        account = p.String(maxLength: 30),
                        bank = p.String(maxLength: 100),
                        clabe = p.String(maxLength: 30),
                        certification = p.String(maxLength: 100),
                        deleted = p.Boolean(),
                        user_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblProvider]([name], [rfc], [address_id], [contact_id], [activity], [account], [bank], [clabe], [certification], [deleted], [user_id])
                      VALUES (@name, @rfc, @address_id, @contact_id, @activity, @account, @bank, @clabe, @certification, @deleted, @user_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblProvider]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblProvider] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Provider_Update",
                p => new
                    {
                        id = p.Long(),
                        name = p.String(maxLength: 100),
                        rfc = p.String(maxLength: 15),
                        address_id = p.Long(),
                        contact_id = p.Long(),
                        activity = p.String(maxLength: 100),
                        account = p.String(maxLength: 30),
                        bank = p.String(maxLength: 100),
                        clabe = p.String(maxLength: 30),
                        certification = p.String(maxLength: 100),
                        deleted = p.Boolean(),
                        user_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblProvider]
                      SET [name] = @name, [rfc] = @rfc, [address_id] = @address_id, [contact_id] = @contact_id, [activity] = @activity, [account] = @account, [bank] = @bank, [clabe] = @clabe, [certification] = @certification, [deleted] = @deleted, [user_id] = @user_id
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Provider_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblProvider]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Driver_Insert",
                p => new
                    {
                        first_name = p.String(maxLength: 50),
                        last_name = p.String(maxLength: 50),
                        carrier_id = p.Long(),
                        social_security_number = p.String(maxLength: 100),
                        driver_license_number = p.String(maxLength: 100),
                        user_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblDriver]([first_name], [last_name], [carrier_id], [social_security_number], [driver_license_number], [user_id])
                      VALUES (@first_name, @last_name, @carrier_id, @social_security_number, @driver_license_number, @user_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblDriver]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblDriver] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Driver_Update",
                p => new
                    {
                        id = p.Long(),
                        first_name = p.String(maxLength: 50),
                        last_name = p.String(maxLength: 50),
                        carrier_id = p.Long(),
                        social_security_number = p.String(maxLength: 100),
                        driver_license_number = p.String(maxLength: 100),
                        user_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblDriver]
                      SET [first_name] = @first_name, [last_name] = @last_name, [carrier_id] = @carrier_id, [social_security_number] = @social_security_number, [driver_license_number] = @driver_license_number, [user_id] = @user_id
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblDriver] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Driver_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblDriver]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Incidence_Insert",
                p => new
                    {
                        object_id = p.Long(),
                        table = p.String(maxLength: 100),
                        client_id = p.Long(),
                        comments = p.String(),
                        creator_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblIncidence]([object_id], [table], [client_id], [comments], [creator_id])
                      VALUES (@object_id, @table, @client_id, @comments, @creator_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblIncidence]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblIncidence] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Incidence_Update",
                p => new
                    {
                        id = p.Long(),
                        object_id = p.Long(),
                        table = p.String(maxLength: 100),
                        client_id = p.Long(),
                        comments = p.String(),
                        creator_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblIncidence]
                      SET [object_id] = @object_id, [table] = @table, [client_id] = @client_id, [comments] = @comments, [creator_id] = @creator_id
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblIncidence] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Incidence_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblIncidence]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Media_Insert",
                p => new
                    {
                        collection_name = p.String(maxLength: 100),
                        name = p.String(maxLength: 100),
                        mime_type = p.String(maxLength: 100),
                        file_name = p.String(maxLength: 100),
                        table = p.String(maxLength: 50),
                        object_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblMedia]([collection_name], [name], [mime_type], [file_name], [table], [object_id])
                      VALUES (@collection_name, @name, @mime_type, @file_name, @table, @object_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblMedia]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[deleted_at]
                      FROM [dbo].[tblMedia] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Media_Update",
                p => new
                    {
                        id = p.Long(),
                        collection_name = p.String(maxLength: 100),
                        name = p.String(maxLength: 100),
                        mime_type = p.String(maxLength: 100),
                        file_name = p.String(maxLength: 100),
                        table = p.String(maxLength: 50),
                        object_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblMedia]
                      SET [collection_name] = @collection_name, [name] = @name, [mime_type] = @mime_type, [file_name] = @file_name, [table] = @table, [object_id] = @object_id
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[deleted_at]
                      FROM [dbo].[tblMedia] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Media_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblMedia]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ShippingSchedule_Insert",
                p => new
                    {
                        is_marked_as_rejected = p.Boolean(),
                        shipping_number = p.String(maxLength: 50),
                        shipping_status_id = p.Long(),
                        scheduled_date = p.DateTime(),
                        warehouse_id = p.Long(),
                        carrier_id = p.Long(),
                        driver_id = p.Long(),
                        client_id = p.Long(),
                        trailer_number = p.String(maxLength: 50),
                        truck_number = p.String(maxLength: 50),
                        license_plate = p.String(maxLength: 50),
                        container = p.String(maxLength: 50),
                        trailer_status_id = p.Long(),
                        origin = p.String(maxLength: 50),
                        destination = p.String(maxLength: 50),
                        initial_yard_mule_driver_id = p.Long(),
                        initial_parking_space = p.String(maxLength: 50),
                        ramp = p.String(maxLength: 50),
                        final_parking_space = p.String(maxLength: 50),
                        shipping_priority = p.Int(),
                        final_yard_mule_driver_id = p.Long(),
                        ramp_and_yard_mule_driver_assigned_at = p.DateTime(),
                        moved_from_parking_space_at = p.DateTime(),
                        in_ramp_at = p.DateTime(),
                        moved_from_ramp_at = p.DateTime(),
                        in_parking_space_at = p.DateTime(),
                        creator_id = p.Long(),
                        editor_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblShippingSchedule]([is_marked_as_rejected], [shipping_number], [shipping_status_id], [scheduled_date], [warehouse_id], [carrier_id], [driver_id], [client_id], [trailer_number], [truck_number], [license_plate], [container], [trailer_status_id], [origin], [destination], [initial_yard_mule_driver_id], [initial_parking_space], [ramp], [final_parking_space], [shipping_priority], [final_yard_mule_driver_id], [ramp_and_yard_mule_driver_assigned_at], [moved_from_parking_space_at], [in_ramp_at], [moved_from_ramp_at], [in_parking_space_at], [creator_id], [editor_id])
                      VALUES (@is_marked_as_rejected, @shipping_number, @shipping_status_id, @scheduled_date, @warehouse_id, @carrier_id, @driver_id, @client_id, @trailer_number, @truck_number, @license_plate, @container, @trailer_status_id, @origin, @destination, @initial_yard_mule_driver_id, @initial_parking_space, @ramp, @final_parking_space, @shipping_priority, @final_yard_mule_driver_id, @ramp_and_yard_mule_driver_assigned_at, @moved_from_parking_space_at, @in_ramp_at, @moved_from_ramp_at, @in_parking_space_at, @creator_id, @editor_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblShippingSchedule]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblShippingSchedule] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ShippingSchedule_Update",
                p => new
                    {
                        id = p.Long(),
                        is_marked_as_rejected = p.Boolean(),
                        shipping_number = p.String(maxLength: 50),
                        shipping_status_id = p.Long(),
                        scheduled_date = p.DateTime(),
                        warehouse_id = p.Long(),
                        carrier_id = p.Long(),
                        driver_id = p.Long(),
                        client_id = p.Long(),
                        trailer_number = p.String(maxLength: 50),
                        truck_number = p.String(maxLength: 50),
                        license_plate = p.String(maxLength: 50),
                        container = p.String(maxLength: 50),
                        trailer_status_id = p.Long(),
                        origin = p.String(maxLength: 50),
                        destination = p.String(maxLength: 50),
                        initial_yard_mule_driver_id = p.Long(),
                        initial_parking_space = p.String(maxLength: 50),
                        ramp = p.String(maxLength: 50),
                        final_parking_space = p.String(maxLength: 50),
                        shipping_priority = p.Int(),
                        final_yard_mule_driver_id = p.Long(),
                        ramp_and_yard_mule_driver_assigned_at = p.DateTime(),
                        moved_from_parking_space_at = p.DateTime(),
                        in_ramp_at = p.DateTime(),
                        moved_from_ramp_at = p.DateTime(),
                        in_parking_space_at = p.DateTime(),
                        creator_id = p.Long(),
                        editor_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblShippingSchedule]
                      SET [is_marked_as_rejected] = @is_marked_as_rejected, [shipping_number] = @shipping_number, [shipping_status_id] = @shipping_status_id, [scheduled_date] = @scheduled_date, [warehouse_id] = @warehouse_id, [carrier_id] = @carrier_id, [driver_id] = @driver_id, [client_id] = @client_id, [trailer_number] = @trailer_number, [truck_number] = @truck_number, [license_plate] = @license_plate, [container] = @container, [trailer_status_id] = @trailer_status_id, [origin] = @origin, [destination] = @destination, [initial_yard_mule_driver_id] = @initial_yard_mule_driver_id, [initial_parking_space] = @initial_parking_space, [ramp] = @ramp, [final_parking_space] = @final_parking_space, [shipping_priority] = @shipping_priority, [final_yard_mule_driver_id] = @final_yard_mule_driver_id, [ramp_and_yard_mule_driver_assigned_at] = @ramp_and_yard_mule_driver_assigned_at, [moved_from_parking_space_at] = @moved_from_parking_space_at, [in_ramp_at] = @in_ramp_at, [moved_from_ramp_at] = @moved_from_ramp_at, [in_parking_space_at] = @in_parking_space_at, [creator_id] = @creator_id, [editor_id] = @editor_id
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblShippingSchedule] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ShippingSchedule_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblShippingSchedule]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_YardMuleDriver_Insert",
                p => new
                    {
                        first_name = p.String(maxLength: 50),
                        last_name = p.String(maxLength: 50),
                        incidence_id = p.Long(),
                        social_security_number = p.String(maxLength: 100),
                        driver_license_number = p.String(maxLength: 100),
                        user_id = p.Long(),
                    },
                body:
                    @"INSERT [dbo].[tblYardMuleDriver]([first_name], [last_name], [incidence_id], [social_security_number], [driver_license_number], [user_id])
                      VALUES (@first_name, @last_name, @incidence_id, @social_security_number, @driver_license_number, @user_id)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblYardMuleDriver]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id], t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblYardMuleDriver] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_YardMuleDriver_Update",
                p => new
                    {
                        id = p.Long(),
                        first_name = p.String(maxLength: 50),
                        last_name = p.String(maxLength: 50),
                        incidence_id = p.Long(),
                        social_security_number = p.String(maxLength: 100),
                        driver_license_number = p.String(maxLength: 100),
                        user_id = p.Long(),
                    },
                body:
                    @"UPDATE [dbo].[tblYardMuleDriver]
                      SET [first_name] = @first_name, [last_name] = @last_name, [incidence_id] = @incidence_id, [social_security_number] = @social_security_number, [driver_license_number] = @driver_license_number, [user_id] = @user_id
                      WHERE ([id] = @id)
                      
                      SELECT t0.[created_at], t0.[updated_at], t0.[deleted_at]
                      FROM [dbo].[tblYardMuleDriver] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_YardMuleDriver_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblYardMuleDriver]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ShippingStatus_Insert",
                p => new
                    {
                        name = p.String(maxLength: 50),
                        label = p.String(maxLength: 50),
                    },
                body:
                    @"INSERT [dbo].[tblShippingStatus]([name], [label])
                      VALUES (@name, @label)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblShippingStatus]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblShippingStatus] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ShippingStatus_Update",
                p => new
                    {
                        id = p.Long(),
                        name = p.String(maxLength: 50),
                        label = p.String(maxLength: 50),
                    },
                body:
                    @"UPDATE [dbo].[tblShippingStatus]
                      SET [name] = @name, [label] = @label
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_ShippingStatus_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblShippingStatus]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_TrailerStatus_Insert",
                p => new
                    {
                        name = p.String(maxLength: 50),
                        label = p.String(maxLength: 50),
                    },
                body:
                    @"INSERT [dbo].[tblTrailerStatus]([name], [label])
                      VALUES (@name, @label)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblTrailerStatus]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblTrailerStatus] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_TrailerStatus_Update",
                p => new
                    {
                        id = p.Long(),
                        name = p.String(maxLength: 50),
                        label = p.String(maxLength: 50),
                    },
                body:
                    @"UPDATE [dbo].[tblTrailerStatus]
                      SET [name] = @name, [label] = @label
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_TrailerStatus_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblTrailerStatus]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Warehouse_Insert",
                p => new
                    {
                        name = p.String(),
                        plant_id = p.Long(),
                        address_id = p.Long(),
                        deleted = p.Boolean(),
                    },
                body:
                    @"INSERT [dbo].[tblWarehouse]([name], [plant_id], [address_id], [deleted])
                      VALUES (@name, @plant_id, @address_id, @deleted)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [dbo].[tblWarehouse]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [dbo].[tblWarehouse] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Warehouse_Update",
                p => new
                    {
                        id = p.Long(),
                        name = p.String(),
                        plant_id = p.Long(),
                        address_id = p.Long(),
                        deleted = p.Boolean(),
                    },
                body:
                    @"UPDATE [dbo].[tblWarehouse]
                      SET [name] = @name, [plant_id] = @plant_id, [address_id] = @address_id, [deleted] = @deleted
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_Warehouse_SoftDelete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [dbo].[tblWarehouse]
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_UserRoles_Insert",
                p => new
                    {
                        auth = p.Long(),
                        name = p.String(maxLength: 50),
                        label = p.String(maxLength: 50),
                    },
                body:
                    @"INSERT [adm].[tblUserRoles]([auth], [name], [label])
                      VALUES (@auth, @name, @label)
                      
                      DECLARE @id bigint
                      SELECT @id = [id]
                      FROM [adm].[tblUserRoles]
                      WHERE @@ROWCOUNT > 0 AND [id] = scope_identity()
                      
                      SELECT t0.[id]
                      FROM [adm].[tblUserRoles] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[id] = @id"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_UserRoles_Update",
                p => new
                    {
                        id = p.Long(),
                        auth = p.Long(),
                        name = p.String(maxLength: 50),
                        label = p.String(maxLength: 50),
                    },
                body:
                    @"UPDATE [adm].[tblUserRoles]
                      SET [auth] = @auth, [name] = @name, [label] = @label
                      WHERE ([id] = @id)"
            );
            
            CreateStoredProcedure(
                "dbo.TOY_UserRoles_Delete",
                p => new
                    {
                        id = p.Long(),
                    },
                body:
                    @"DELETE [adm].[tblUserRoles]
                      WHERE ([id] = @id)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.TOY_UserRoles_Delete");
            DropStoredProcedure("dbo.TOY_UserRoles_Update");
            DropStoredProcedure("dbo.TOY_UserRoles_Insert");
            DropStoredProcedure("dbo.TOY_Warehouse_SoftDelete");
            DropStoredProcedure("dbo.TOY_Warehouse_Update");
            DropStoredProcedure("dbo.TOY_Warehouse_Insert");
            DropStoredProcedure("dbo.TOY_TrailerStatus_Delete");
            DropStoredProcedure("dbo.TOY_TrailerStatus_Update");
            DropStoredProcedure("dbo.TOY_TrailerStatus_Insert");
            DropStoredProcedure("dbo.TOY_ShippingStatus_Delete");
            DropStoredProcedure("dbo.TOY_ShippingStatus_Update");
            DropStoredProcedure("dbo.TOY_ShippingStatus_Insert");
            DropStoredProcedure("dbo.TOY_YardMuleDriver_SoftDelete");
            DropStoredProcedure("dbo.TOY_YardMuleDriver_Update");
            DropStoredProcedure("dbo.TOY_YardMuleDriver_Insert");
            DropStoredProcedure("dbo.TOY_ShippingSchedule_SoftDelete");
            DropStoredProcedure("dbo.TOY_ShippingSchedule_Update");
            DropStoredProcedure("dbo.TOY_ShippingSchedule_Insert");
            DropStoredProcedure("dbo.TOY_Media_SoftDelete");
            DropStoredProcedure("dbo.TOY_Media_Update");
            DropStoredProcedure("dbo.TOY_Media_Insert");
            DropStoredProcedure("dbo.TOY_Incidence_SoftDelete");
            DropStoredProcedure("dbo.TOY_Incidence_Update");
            DropStoredProcedure("dbo.TOY_Incidence_Insert");
            DropStoredProcedure("dbo.TOY_Driver_SoftDelete");
            DropStoredProcedure("dbo.TOY_Driver_Update");
            DropStoredProcedure("dbo.TOY_Driver_Insert");
            DropStoredProcedure("dbo.TOY_Provider_SoftDelete");
            DropStoredProcedure("dbo.TOY_Provider_Update");
            DropStoredProcedure("dbo.TOY_Provider_Insert");
            DropStoredProcedure("dbo.TOY_Client_Delete");
            DropStoredProcedure("dbo.TOY_Client_Update");
            DropStoredProcedure("dbo.TOY_Client_Insert");
            DropStoredProcedure("dbo.TOY_Carrier_SoftDelete");
            DropStoredProcedure("dbo.TOY_Carrier_Update");
            DropStoredProcedure("dbo.TOY_Carrier_Insert");
            DropStoredProcedure("dbo.TOY_Email_Delete");
            DropStoredProcedure("dbo.TOY_Email_Update");
            DropStoredProcedure("dbo.TOY_Email_Insert");
            DropStoredProcedure("dbo.TOY_Address_Delete");
            DropStoredProcedure("dbo.TOY_Address_Update");
            DropStoredProcedure("dbo.TOY_Address_Insert");
            DropStoredProcedure("dbo.TOY_Contact_Delete");
            DropStoredProcedure("dbo.TOY_Contact_Update");
            DropStoredProcedure("dbo.TOY_Contact_Insert");
            DropStoredProcedure("dbo.TOY_User_Delete");
            DropStoredProcedure("dbo.TOY_User_Update");
            DropStoredProcedure("dbo.TOY_User_Insert");
            DropStoredProcedure("dbo.TOY_ActivityLog_SoftDelete");
            DropStoredProcedure("dbo.TOY_ActivityLog_Update");
            DropStoredProcedure("dbo.TOY_ActivityLog_Insert");
            DropForeignKey("dbo.tblShippingSchedule", "warehouse_id", "dbo.tblWarehouse");
            DropForeignKey("dbo.tblWarehouse", "address_id", "dbo.tblAddress");
            DropForeignKey("dbo.tblShippingSchedule", "trailer_status_id", "dbo.tblTrailerStatus");
            DropForeignKey("dbo.tblShippingSchedule", "shipping_status_id", "dbo.tblShippingStatus");
            DropForeignKey("dbo.tblShippingSchedule", "initial_yard_mule_driver_id", "dbo.tblYardMuleDriver");
            DropForeignKey("dbo.tblShippingSchedule", "final_yard_mule_driver_id", "dbo.tblYardMuleDriver");
            DropForeignKey("dbo.tblYardMuleDriver", "user_id", "adm.tblUser");
            DropForeignKey("dbo.tblYardMuleDriver", "incidence_id", "dbo.tblIncidence");
            DropForeignKey("dbo.tblShippingSchedule", "editor_id", "adm.tblUser");
            DropForeignKey("dbo.tblShippingSchedule", "driver_id", "dbo.tblDriver");
            DropForeignKey("dbo.tblShippingSchedule", "creator_id", "adm.tblUser");
            DropForeignKey("dbo.tblShippingSchedule", "client_id", "dbo.tblClient");
            DropForeignKey("dbo.tblShippingSchedule", "carrier_id", "dbo.tblCarrier");
            DropForeignKey("dbo.tblIncidence", "creator_id", "adm.tblUser");
            DropForeignKey("dbo.tblIncidence", "client_id", "dbo.tblClient");
            DropForeignKey("dbo.tblDriver", "user_id", "adm.tblUser");
            DropForeignKey("dbo.tblDriver", "carrier_id", "dbo.tblCarrier");
            DropForeignKey("dbo.tblClient", "provider_id", "dbo.tblProvider");
            DropForeignKey("dbo.tblProvider", "user_id", "adm.tblUser");
            DropForeignKey("dbo.tblProvider", "contact_id", "dbo.tblContact");
            DropForeignKey("dbo.tblProvider", "address_id", "dbo.tblAddress");
            DropForeignKey("dbo.tblClient", "contact_id", "dbo.tblContact");
            DropForeignKey("dbo.tblCarrier", "user_id", "adm.tblUser");
            DropForeignKey("dbo.tblCarrier", "address_id", "dbo.tblAddress");
            DropForeignKey("dbo.tblActivityLog", "causer_id", "adm.tblUser");
            DropForeignKey("adm.tblUser", "contact_id", "dbo.tblContact");
            DropForeignKey("dbo.tblContact", "email_id", "dbo.tblEmail");
            DropForeignKey("dbo.tblContact", "address_id", "dbo.tblAddress");
            DropIndex("dbo.tblWarehouse", new[] { "address_id" });
            DropIndex("dbo.tblYardMuleDriver", new[] { "user_id" });
            DropIndex("dbo.tblYardMuleDriver", new[] { "incidence_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "editor_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "creator_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "final_yard_mule_driver_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "initial_yard_mule_driver_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "trailer_status_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "client_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "driver_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "carrier_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "warehouse_id" });
            DropIndex("dbo.tblShippingSchedule", new[] { "shipping_status_id" });
            DropIndex("dbo.tblIncidence", new[] { "creator_id" });
            DropIndex("dbo.tblIncidence", new[] { "client_id" });
            DropIndex("dbo.tblDriver", new[] { "user_id" });
            DropIndex("dbo.tblDriver", new[] { "carrier_id" });
            DropIndex("dbo.tblProvider", new[] { "user_id" });
            DropIndex("dbo.tblProvider", new[] { "contact_id" });
            DropIndex("dbo.tblProvider", new[] { "address_id" });
            DropIndex("dbo.tblClient", new[] { "provider_id" });
            DropIndex("dbo.tblClient", new[] { "contact_id" });
            DropIndex("dbo.tblCarrier", new[] { "user_id" });
            DropIndex("dbo.tblCarrier", new[] { "address_id" });
            DropIndex("dbo.tblContact", new[] { "address_id" });
            DropIndex("dbo.tblContact", new[] { "email_id" });
            DropIndex("adm.tblUser", new[] { "contact_id" });
            DropIndex("dbo.tblActivityLog", new[] { "causer_id" });
            DropTable("adm.tblUserRoles");
            DropTable("dbo.tblWarehouse");
            DropTable("dbo.tblTrailerStatus");
            DropTable("dbo.tblShippingStatus");
            DropTable("dbo.tblYardMuleDriver");
            DropTable("dbo.tblShippingSchedule");
            DropTable("dbo.tblMedia");
            DropTable("dbo.Login");
            DropTable("dbo.tblIncidence");
            DropTable("dbo.tblDriver");
            DropTable("dbo.tblProvider");
            DropTable("dbo.tblClient");
            DropTable("dbo.tblCarrier");
            DropTable("dbo.tblEmail");
            DropTable("dbo.tblAddress");
            DropTable("dbo.tblContact");
            DropTable("adm.tblUser");
            DropTable("dbo.tblActivityLog");
        }
    }
}
