﻿namespace WSApiTrackingOneYarda.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using WSApiTrackingOneYarda.Models;

    internal sealed class LogisticsTrackingConfiguration : DbMigrationsConfiguration<Data.LogisticsTrackingDBContext>
    {
        public LogisticsTrackingConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.LogisticsTrackingDBContext context)
        {
            // CREAMOS LOS ESTADOS DE EMBARQUE
            var shippingStatus = new List<ShippingStatus>
            {
                new ShippingStatus
                {
                    name="on_request",
                    label="En solicitud",
                },
                new ShippingStatus
                {
                    name="ramp_yard_mule_driver_pending",
                    label="Pendiente asignar rampa y mulero",
                },
                new ShippingStatus
                {
                    name="in_parking_space",
                    label="En cajón",
                },
                new ShippingStatus
                {
                    name="in_ramp",
                    label="En rampa",
                },
                new ShippingStatus
                {
                    name="finished_in_parking_space",
                    label="Finalizado en cajón",
                },
            };
            shippingStatus.ForEach(s => context.ShippingStatus.AddOrUpdate(ss => new { ss.name, ss.label }, s));
            context.SaveChanges();

            // CREAMOS LOS ESTADOS DE REMOLQUE
            var trailerStatus = new List<TrailerStatus>
            {
                new TrailerStatus
                {
                    name="on_download",
                    label="Descarga",
                },
                new TrailerStatus
                {
                    name="on_load",
                    label="Carga",
                },
                new TrailerStatus
                {
                    name="on_stock",
                    label="Stock",
                },
            };
            trailerStatus.ForEach(s => context.TrailerStatus.AddOrUpdate(ss => new { ss.name, ss.label }, s));
            context.SaveChanges();

            // CREAMOS LOS ROLES DE USUARIO
            var userRoles = new List<UserRoles>
            {
                new UserRoles
                {
                    auth = 0,
                    name="administrator",
                    label="Administrador",
                },
                new UserRoles
                {
                    auth = 1,
                    name="storer",
                    label="Almacenista",
                },
                new UserRoles
                {
                    auth = 2,
                    name="driver",
                    label="Operador",
                },
                new UserRoles
                {
                    auth = 3,
                    name="client_host",
                    label="Cliente anfitrión",
                },
                new UserRoles
                {
                    auth = 4,
                    name="operative",
                    label="Agente",
                },
                new UserRoles
                {
                    auth = 5,
                    name="super_admin",
                    label="Super Administrador",
                },
                new UserRoles
                {
                    auth = 6,
                    name="carrier",
                    label="Transportista",
                },
                new UserRoles
                {
                    auth = 7,
                    name="traffic",
                    label="Tráfico",
                },
                new UserRoles
                {
                    auth = 8,
                    name="yard_mule_driver",
                    label="Mulero",
                },
                new UserRoles
                {
                    auth = 9,
                    name="provider",
                    label="Proveedor",
                },
            };

            userRoles.ForEach(s => context.UserRoles.AddOrUpdate(ss => new { ss.name, ss.label }, s));
            context.SaveChanges();

            // CREAMOS USUARIO ADMIN
            var user = new List<User>
            {
                new User
                {
                    auth = 0,
                    name="Administrador",
                    username="Administrador",
                    password="eyJpdiI6IjlQeStjVVRRcjNYTkVhdUt5bWRKaVE9PSIsInZhbHVlIjoia1VjWEREODZjdjl5TDZPcGY3YUg4dz09IiwibWFjIjoiMDUyMWFhNWQ0MzE0NzY5MDk0MWU1YjEyMzY4MDE2M2RhOTU2YTM0NGIwYmU2ZjljNTFhYWYyZGFhZjgyZDI1MyJ9",
                }
            };

            user.ForEach(s => context.User.AddOrUpdate(ss => new { ss.username, ss.password }, s));
            context.SaveChanges();
        }
    }
}
