﻿using System.Web;
using System.Web.Mvc;

namespace WSApiTrackingOneYarda.ActionFilters
{
    public class AuthenticateActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["Login"] == null)
            {
                filterContext
                    .HttpContext
                    .Response
                    .RedirectToRoute(new { controller = "LoginAjax", action = "Index" });
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
