﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.ActionFilters
{
    public class CurrentUserActionFilter : ActionFilterAttribute
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["Login"] != null)
            {
                filterContext.Controller.ViewBag.currentUser = HttpContext.Current.Session["Login"] as User;
                var user = HttpContext.Current.Session["Login"] as User;
                user.password = null;
                filterContext.Controller.ViewBag.currentUserJson = JsonConvert.SerializeObject(user);

                if (user.is_provider)
                {
                    Provider provider = db.Provider.Where(t => t.user_id == user.id).FirstOrDefault();
                    List<Client> clients = db.Client.Where(s => s.provider_id == provider.id).ToList();
                    filterContext.Controller.ViewBag.clientsCurrentProvider = clients;
                }
            }
            else
            {
                filterContext.Controller.ViewBag.currentUser = new User { };
            }
        }
    }
}
