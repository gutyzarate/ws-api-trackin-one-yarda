﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;
using RouteAttribute = System.Web.Http.RouteAttribute;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("api/yard-mule-drivers")]
    public class YardMuleDriversApiController : ApiController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        [Route("available")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetYardMuleDrivers()
        {
            var endOfDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            var activityLogSubjectIds = db.ActivityLog.Where(s => s.created_at >= DateTime.Today && s.created_at <= endOfDay).Select(t => t.subject_id).ToList();
            // var yardMuleDriverUserIds = await db.User.Where(t => activityLogSubjectIds.Contains(t.id) && t.auth == UserRoles.YARD_MULE_DRIVER).Select(t => t.id).ToListAsync();
            // var yardMuleDrivers = await db.YardMuleDriver.Where(t => yardMuleDriverUserIds.Contains(t.user_id.ToString())).ToListAsync();

            var yardMuleDrivers = await db.YardMuleDriver.Where(t => activityLogSubjectIds.Contains(t.user_id.Value)).ToListAsync();

            foreach(YardMuleDriver yardMuleDriver in yardMuleDrivers)
            {
                // Obtenemos los embarques asignados al mulero
                List<ShippingSchedule> shippingSchedules = await db.ShippingSchedules.Where(s => s.initial_yard_mule_driver_id == yardMuleDriver.id).ToListAsync();
                yardMuleDriver.shipping_schedules_assigned = shippingSchedules.Count();
            }


            return Ok(yardMuleDrivers);
        }
    }
}
