﻿using onecore.security.utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("login")]
    public class LoginAjaxController : Controller
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();
        private string encryptionKey = "0n3c0r33ncr1pt201804203i0qaw0e2e";

        // GET: ProvidersAjax
        [Route]
        public ActionResult Index()
        {
            if (Session["Login"] != null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        // Agregar Proveedor
        [Route("~/ajax/login")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult PostAjaxProvider(Login credentials)
        {
            var users = db.User.Where(s => s.username == credentials.username);

            if (users.Count() == 0)
            {
                return this.loginFailedResponse();
            }

            var user = users.First();

            string decryptedPassword = EncryptService.Decrypt(user.password, encryptionKey);

            if (!credentials.password.Equals(decryptedPassword))
            {
                Session.Abandon();
                return this.loginFailedResponse();
            }

            if (!credentials.password.Equals(decryptedPassword))
            {
                Session.Abandon();
                return this.loginFailedResponse();
            }

            var authorizedRoleIds = new List<int?> {
                UserRoles.CARRIER,
                UserRoles.TRAFFIC,
                //UserRoles.YARD_MULE_DRIVER,
                UserRoles.PROVIDER,
                UserRoles.ADMINISTRATOR,
                UserRoles.SUPER_ADMIN,
            };
            if (user.auth == null || !authorizedRoleIds.Contains(user.auth))
            {
                Session.Abandon();
                return this.loginNotAllowedResponse();
            }

            Session.Add("Login", user);

            return Json(user, JsonRequestBehavior.AllowGet);
        }

        private ActionResult loginFailedResponse()
        {
            return Json(new { Error = "El usuario o la contraseña son incorrectas. Favor de verificar su información" });
        }

        private ActionResult loginNotAllowedResponse()
        {
            return Json(new { Error = "El nivel del usuario con el que intenta ingresar no tiene permisos de acceso al sistema." });
        }

        // Agregar Proveedor
        [Route("~/ajax/login/session/make")]
        [HttpPost]
        public ActionResult PostAjaxMakeSession(string name, string value)
        {
            var session_current = Session[name];
            if(session_current != null)
            {
                Session.Remove(name);
            }
            if(value != "all")
            {
                Session.Add(name, value);
                session_current = Session[name];
            }

            return Json(new HandlerReturn(HttpStatusCode.OK, false, null, "La sesión se ha creado exitosamente.", Convert.ToString(session_current)), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
