﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("providers")]
    public class ProvidersAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: ProvidersAjax
        [Route]
        public async Task<ActionResult> Index()
        {
            ViewBag.model = "providers";
            ViewBag.modelLabel = "Proveedor";
            var provider = db.Provider.Include(p => p.address).Include(p => p.contact);
            return View(await provider.ToListAsync());
        }

        // GET: ProvidersAjax/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Provider provider = await db.Provider.FindAsync(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        // GET: ProvidersAjax/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.address_id = new SelectList(db.Address, "id", "street");
            ViewBag.contact_id = new SelectList(db.Contact, "id", "id");
            return View();
        }

        // POST: ProvidersAjax/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,name,rfc,address_id,contact_id,activity,account,bank,clabe,certification,deleted")] Provider provider)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Provider.Add(provider);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.address_id = new SelectList(db.Address, "id", "street", provider.address_id);
        //    ViewBag.contact_id = new SelectList(db.Contact, "id", "id", provider.contact_id);
        //    return View(provider);
        //}

        // GET: ProvidersAjax/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Provider provider = await db.Provider.FindAsync(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            ViewBag.address_id = new SelectList(db.Address, "id", "street", provider.address_id);
            ViewBag.contact_id = new SelectList(db.Contact, "id", "id", provider.contact_id);
            return View(provider);
        }

        // POST: ProvidersAjax/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,name,rfc,address_id,contact_id,activity,account,bank,clabe,certification,deleted")] Provider provider)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(provider).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.address_id = new SelectList(db.Address, "id", "street", provider.address_id);
        //    ViewBag.contact_id = new SelectList(db.Contact, "id", "id", provider.contact_id);
        //    return View(provider);
        //}

        // GET: ProvidersAjax/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Provider provider = await db.Provider.FindAsync(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        // POST: ProvidersAjax/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Provider provider = await db.Provider.FindAsync(id);
        //    db.Provider.Remove(provider);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener provedores
        [Route("~/ajax/providers")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxProviders()
        {
            //IQueryable<Provider> Provider = db.Provider.Where(t => t.deleted == false).Include(c => c.contact);
            var Provider = await db.Provider
                .Where(t => t.deleted == false)
                .Include(c => c.contact)
                .Join(
                db.User,
                p => p.contact_id,
                u => u.contact_id,
                (p, u) => new
                {
                    id = p.id,
                    name = u.name,
                    first_last_name = u.first_last_name,
                    second_last_name = u.second_last_name,
                    rfc = p.rfc,
                    zipcode = p.contact.address.zipcode,
                    street = p.contact.address.street,
                    ext_number = p.contact.address.ext_number,
                    int_number = p.contact.address.int_number,
                    neighborhood = p.contact.address.neighborhood,
                    state = p.contact.address.state,
                    country = p.contact.address.country,
                    city = p.contact.address.city
                }).ToListAsync();

            //Provider.Join(db.User, p => p.contact_id, u => User.)
            //List<Provider> provider = await Provider.ToListAsync();

            if (Provider.Count() == 0)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(Provider, JsonRequestBehavior.AllowGet);
        }

        // Obtener provedores con id
        [Route("~/ajax/providers/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxProvidersById(int id)
        {
            //IQueryable<Provider> Provider = db.Provider.Where(t => t.deleted == false).Include(c => c.contact);
            var Provider = await db.Provider
                .Where(t => t.deleted == false && t.id == id)
                .Include(c => c.contact)
                .Join(
                    db.User,
                    p => p.contact_id,
                    u => u.contact_id,
                    (p, u) => new
                    {
                        id = p.id,
                        name = u.name,
                        first_last_name = u.first_last_name,
                        second_last_name = u.second_last_name,
                        rfc = p.rfc,
                        zipcode = p.contact.address.zipcode,
                        street = p.contact.address.street,
                        ext_number = p.contact.address.ext_number,
                        int_number = p.contact.address.int_number,
                        neighborhood = p.contact.address.neighborhood,
                        state = p.contact.address.state,
                        country = p.contact.address.country,
                        city = p.contact.address.city
                    }).FirstAsync();

            //Provider.Join(db.User, p => p.contact_id, u => User.)
            //List<Provider> provider = await Provider.ToListAsync();

            if (Provider == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(Provider, JsonRequestBehavior.AllowGet);
        }

        // Agregar Proveedor
        [Route("~/ajax/providers")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxProvider(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["name"]) ||
                string.IsNullOrEmpty(formData["first_last_name"]) ||
                string.IsNullOrEmpty(formData["second_last_name"]) ||
                string.IsNullOrEmpty(formData["rfc"]) ||
                string.IsNullOrEmpty(formData["zipcode"]) ||
                string.IsNullOrEmpty(formData["street"]) ||
                string.IsNullOrEmpty(formData["ext_number"]) ||
                //string.IsNullOrEmpty(formData["int_number"]) ||
                string.IsNullOrEmpty(formData["neighborhood"]) ||
                //string.IsNullOrEmpty(formData["latitud"]) &&
                //string.IsNullOrEmpty(formData["longitude"]) &&
                string.IsNullOrEmpty(formData["state"]) ||
                string.IsNullOrEmpty(formData["country"]) ||
                string.IsNullOrEmpty(formData["city"]))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el proveedor no existe
            IQueryable<Provider> provider = db.Provider.Where(t => t.deleted == false);

            string rfc = formData["rfc"].ToUpper();
            provider = provider.Where(t => t.rfc.ToUpper() == rfc);

            Provider ExistProvider = await provider.FirstOrDefaultAsync();

            if (ExistProvider != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAP_01", "El RFC ya se usa en otro proveedor, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el proveedor no existe

            // Creamos la dirección
            Address Address = new Address();
            Contact Contact = new Contact();
            // Creamos el cliente
            Provider Provider = new Provider();
            User User = new User();

            foreach (var formKey in formData.AllKeys)
            {
                var value = formData[formKey];
                switch (formKey)
                {
                    // Para Proveedor
                    case "name":
                        Provider.name = value;
                        User.name = value;
                        break;
                    case "rfc":
                        Provider.rfc = value.ToUpper();
                        break;
                    // Para Usuario
                    case "first_last_name":
                        User.first_last_name = value;
                        break;
                    case "second_last_name":
                        User.second_last_name = value;
                        break;
                    // Para Address
                    case "zipcode":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.zipcode = int.Parse(value);
                        }
                        break;
                    case "street":
                        Address.street = value;
                        break;
                    case "ext_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.ext_number = int.Parse(value);
                        }
                        break;
                    case "int_number":
                        if(!string.IsNullOrEmpty(value))
                        {
                            Address.int_number = int.Parse(value);
                        }
                        break;
                    case "neighborhood":
                        Address.neighborhood = value;
                        break;
                    case "latitud":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.latitud = decimal.Parse(value);
                        }
                        break;
                    case "longitude":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.longitude = decimal.Parse(value);
                        }
                        break;
                    case "state":
                        Address.state = value;
                        break;
                    case "country":
                        Address.country = value;
                        break;
                    case "city":
                        Address.city = value;
                        break;
                }
            }

            // Se crea el registor en tblAddress
            db.Address.Add(Address);
            await db.SaveChangesAsync();

            // Se crea el registro en tblContact
            Contact.address_id = Address.id;
            db.Contact.Add(Contact);
            await db.SaveChangesAsync();

            User.auth = UserRoles.PROVIDER;
            User.status = true;
            User.contact_id = Contact.id;

            db.User.Add(User);
            await db.SaveChangesAsync();

            Provider.contact_id = Contact.id;
            Provider.address_id = Address.id;
            Provider.user_id = User.id;
            Provider.deleted = false;
            db.Provider.Add(Provider);
            await db.SaveChangesAsync();

            return Json(Provider, JsonRequestBehavior.AllowGet);
        }

        // Editar proveedor
        [Route("~/ajax/providers")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxProvider(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["id"]) ||
                string.IsNullOrEmpty(formData["name"]) ||
                string.IsNullOrEmpty(formData["first_last_name"]) ||
                string.IsNullOrEmpty(formData["second_last_name"]) ||
                string.IsNullOrEmpty(formData["rfc"]) ||
                string.IsNullOrEmpty(formData["zipcode"]) ||
                string.IsNullOrEmpty(formData["street"]) ||
                string.IsNullOrEmpty(formData["ext_number"]) ||
                //string.IsNullOrEmpty(formData["int_number"]) ||
                string.IsNullOrEmpty(formData["neighborhood"]) ||
                //string.IsNullOrEmpty(formData["latitud"]) &&
                //string.IsNullOrEmpty(formData["longitude"]) &&
                string.IsNullOrEmpty(formData["state"]) ||
                string.IsNullOrEmpty(formData["country"]) ||
                string.IsNullOrEmpty(formData["city"]))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el proveedor no existe
            IQueryable<Provider> provider = db.Provider.Where(t => t.deleted == false);

            string rfc = formData["rfc"].ToUpper();
            int id = int.Parse(formData["id"]);
            provider = provider.Where(t => t.rfc.ToUpper() == rfc && t.id != id);

            Provider ExistProvider = await provider.FirstOrDefaultAsync();

            if (ExistProvider != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAP_01", "El RFC ya se usa en otro proveedor, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el proveedor no existe

            Provider Provider = await db.Provider.FindAsync(id);
            Contact Contact = Provider.contact;
            Address Address = Contact.address;
            User User = await db.User.Where(t => t.contact_id == Contact.id).FirstAsync();

            foreach (var formKey in formData.AllKeys)
            {
                var value = formData[formKey];
                switch (formKey)
                {
                    // Para Proveedor
                    case "name":
                        Provider.name = value;
                        User.name = value;
                        break;
                    case "rfc":
                        Provider.rfc = value.ToUpper();
                        break;
                    // Para Usuario
                    case "first_last_name":
                        User.first_last_name = value;
                        break;
                    case "second_last_name":
                        User.second_last_name = value;
                        break;
                    // Para Address
                    case "zipcode":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.zipcode = int.Parse(value);
                        }
                        break;
                    case "street":
                        Address.street = value;
                        break;
                    case "ext_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.ext_number = int.Parse(value);
                        }
                        break;
                    case "int_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.int_number = int.Parse(value);
                        }
                        break;
                    case "neighborhood":
                        Address.neighborhood = value;
                        break;
                    case "latitud":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.latitud = decimal.Parse(value);

                        }
                        break;
                    case "longitude":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.longitude = decimal.Parse(value);
                        }
                        break;
                    case "state":
                        Address.state = value;
                        break;
                    case "country":
                        Address.country = value;
                        break;
                    case "city":
                        Address.city = value;
                        break;
                }
            }

            db.Entry(Address).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Entry(Provider).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Entry(User).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(Provider, JsonRequestBehavior.AllowGet);
        }

        // Borrar Proveedor
        [Route("~/ajax/providers")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxProvider(int id)
        {
            Provider provider = await db.Provider.FindAsync(id);

            User User = await db.User.Where(t => t.contact_id == provider.contact_id).FirstAsync();

            db.User.Remove(User);

            db.Provider.Remove(provider);
            await db.SaveChangesAsync();

            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El proveedor ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
