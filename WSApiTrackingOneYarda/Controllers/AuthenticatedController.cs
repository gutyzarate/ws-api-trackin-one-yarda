﻿using System.Web.Mvc;
using WSApiTrackingOneYarda.ActionFilters;

namespace WSApiTrackingOneYarda.Controllers
{
    [AuthenticateActionFilter]
    public class AuthenticatedController : Controller
    {
    }
}
