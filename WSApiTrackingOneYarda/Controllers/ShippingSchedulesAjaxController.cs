﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("shipping-schedules")]
    public class ShippingSchedulesAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: ShippingSchedulesAjax
        [Route]
        public async Task<ActionResult> Index()
        {
            //if(Session["Login"] != null)
            //{
            //    var currentUser = Session["Login"] as User;

            //    if (currentUser.auth == UserRoles.PROVIDER)
            //    {
            //        var currentProvider = await db.Provider.Where(t => t.user_id == currentUser.id).FirstOrDefaultAsync();
            //        if (currentProvider == null)
            //        {
            //            @ViewBag.clients = new List<Client> { };
            //        }
            //        else
            //        {
            //            @ViewBag.clients = await db.Client.Where(t => t.provider_id == currentProvider.id).ToListAsync();
            //        }
            //    }
            //    else
            //    {
            //        @ViewBag.clients = await db.Client.ToListAsync();
            //    }
            //}
            //else
            //{
            //    @ViewBag.clients = await db.Client.ToListAsync();
            //}

            @ViewBag.warehouses = await db.Warehouse.ToListAsync();
            @ViewBag.drivers = await db.Driver.ToListAsync();
            @ViewBag.carriers = await db.Carrier.ToListAsync();
            @ViewBag.clientUsers = await db.User.Where(t => t.auth == UserRoles.CLIENT_HOST).ToListAsync();
            @ViewBag.operativeUsers = await db.User.Where(t => t.auth == UserRoles.OPERATIVE).ToListAsync();
            @ViewBag.trailerStatuses = await db.TrailerStatus.ToListAsync();
            @ViewBag.shippingStatuses = await db.ShippingStatus.ToListAsync();

            return View();
        }

        // GET: ShippingSchedulesAjax
        [Route("times-report")]
        public async Task<ActionResult> TimesReport()
        {
            @ViewBag.warehouses = await db.Warehouse.ToListAsync();
            @ViewBag.drivers = await db.Driver.ToListAsync();
            @ViewBag.carriers = await db.Carrier.ToListAsync();
            @ViewBag.clientUsers = await db.User.Where(t => t.auth == UserRoles.CLIENT_HOST).ToListAsync();
            @ViewBag.operativeUsers = await db.User.Where(t => t.auth == UserRoles.OPERATIVE).ToListAsync();
            @ViewBag.trailerStatuses = await db.TrailerStatus.ToListAsync();

            return View();
        }

        // GET: ShippingSchedulesAjax/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule == null)
            {
                return HttpNotFound();
            }
            return View(shippingSchedule);
        }

        // GET: ShippingSchedulesAjax/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.trailer_status_id = new SelectList(db.TrailerStatus, "id", "name");
            return View();
        }

        // POST: ShippingSchedulesAjax/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,is_marked_as_rejected,shipping_number,shipping_status_id,scheduled_date,warehouse_id,carrier_id,driver_id,client_id,trailer_number,truck_number,license_plate,container,trailer_status_id,origin,destination,initial_yard_mule_driver_id,initial_parking_space,ramp,final_parking_space,shipping_priority,final_yard_mule_driver_id,ramp_and_yard_mule_driver_assigned_at,moved_from_parking_space_at,in_ramp_at,moved_from_ramp_at,in_parking_space_at,created_at,creator_id,updated_at,editor_id")] ShippingSchedule shippingSchedule)
        {
            if (ModelState.IsValid)
            {
                db.ShippingSchedules.Add(shippingSchedule);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.trailer_status_id = new SelectList(db.TrailerStatus, "id", "name", shippingSchedule.trailer_status_id);
            return View(shippingSchedule);
        }

        // GET: ShippingSchedulesAjax/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.trailer_status_id = new SelectList(db.TrailerStatus, "id", "name", shippingSchedule.trailer_status_id);
            return View(shippingSchedule);
        }

        // POST: ShippingSchedulesAjax/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,is_marked_as_rejected,shipping_number,shipping_status_id,scheduled_date,warehouse_id,carrier_id,driver_id,client_id,trailer_number,truck_number,license_plate,container,trailer_status_id,origin,destination,initial_yard_mule_driver_id,initial_parking_space,ramp,final_parking_space,shipping_priority,final_yard_mule_driver_id,ramp_and_yard_mule_driver_assigned_at,moved_from_parking_space_at,in_ramp_at,moved_from_ramp_at,in_parking_space_at,created_at,creator_id,updated_at,editor_id")] ShippingSchedule shippingSchedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shippingSchedule).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.trailer_status_id = new SelectList(db.TrailerStatus, "id", "name", shippingSchedule.trailer_status_id);
            return View(shippingSchedule);
        }

        // GET: ShippingSchedulesAjax/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule == null)
            {
                return HttpNotFound();
            }
            return View(shippingSchedule);
        }

        // POST: ShippingSchedulesAjax/Delete/5
        [Route("delete/{id}")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ShippingSchedule shippingSchedule = await db.ShippingSchedules.FindAsync(id);
            db.ShippingSchedules.Remove(shippingSchedule);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener Embarques incluye filtros
        [Route("~/ajax/shipping-schedules")]
        [HttpGet]
        public async Task<ActionResult> GetShippingSchedules(FormCollection FormDatas)
        {
            // Esto es para la verificación del usuario logeado
            // int user_id = int.Parse(Request.Headers.GetValues("user_id").First());
            // User user = await db.User.FindAsync(user_id);
            var currentUser = Session["Login"] as User;
            User user = currentUser;
            ////////////////////////////////////////////////////

            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);

            //await shippingSchedule.OrderBy(t => t.created_at).ToListAsync();

            //return Json(shippingSchedule, JsonRequestBehavior.AllowGet);

            // Esto es para la verificación del usuario logeado
            if (user != null)
            {
                // Si el usuario es Transportista
                if (user.auth == UserRoles.CARRIER)
                {
                    int carrier_id = user.id;
                    shippingSchedule = shippingSchedule.Where(t => t.carrier_id == carrier_id);
                }
                // Si el usuario es Traffic
                if (user.auth == UserRoles.TRAFFIC)
                {
                    int yard_mule_driver_id = user.id;
                    shippingSchedule = shippingSchedule; //.Where(t => t.initial_yard_mule_driver_id == yard_mule_driver_id || t.final_yard_mule_driver_id == yard_mule_driver_id);
                }

                if (user.auth == UserRoles.PROVIDER)
                {
                    shippingSchedule = shippingSchedule;

                    if (Session["filter_provider_client_id"] == null)
                    {
                        var currentProvider = await db.Provider.Where(t => t.user_id == currentUser.id).FirstOrDefaultAsync();
                        var clientIds = new List<int> { };
                        if (currentProvider == null)
                        {
                            clientIds = new List<int> { };
                        }
                        else
                        {
                            clientIds = db.Client.Where(s => s.provider_id == currentProvider.id).Select(t => t.id).ToList();
                        }
                        shippingSchedule = shippingSchedule.Where(t => clientIds.Contains(t.client_id.Value));
                    }
                    else
                    {
                        int filter_provider_client_id = Convert.ToInt32(Session["filter_provider_client_id"]);
                        shippingSchedule = shippingSchedule.Where(t => t.client_id == filter_provider_client_id);
                    }
                    

                    //if (FormDatas["client_id"] == null)
                    //{
                    //    Response.StatusCode = (int)HttpStatusCode.NoContent;
                    //    return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    int client_id = int.Parse(FormDatas["client_id"]);
                    //    shippingSchedule = shippingSchedule.Where(t => t.client_id == client_id);
                    //}
                }
            }
            ///////////////////////////////////////////////////

            //await shippingSchedule.OrderBy(t => t.shipping_priority).ToListAsync();
            var ordenado = await shippingSchedule.OrderBy(v => v.shipping_priority != null ? v.shipping_priority : 4).ThenBy(v => v.shipping_priority).ToListAsync();

            var statusCode = HttpStatusCode.OK;
            Response.StatusCode = (int)statusCode;
            return Json(ordenado, JsonRequestBehavior.AllowGet);
        }

        [Route("~/ajax/shipping-schedules/filter")]
        [HttpPost]
        public async Task<ActionResult> PostShippingSchedulesFilter(FormCollection FormDatas)
        {
            // Esto es para la verificación del usuario logeado
            // int user_id = int.Parse(Request.Headers.GetValues("user_id").First());
            // User user = await db.User.FindAsync(user_id);
            var currentUser = Session["Login"] as User;
            User user = currentUser;
            ////////////////////////////////////////////////////

            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);

            //await shippingSchedule.OrderBy(t => t.created_at).ToListAsync();

            //return Json(shippingSchedule, JsonRequestBehavior.AllowGet);


            //if (!string.IsNullOrEmpty(FormDatas["shipping_number"]) ||
            //    !string.IsNullOrEmpty(FormDatas["trailer_number"]) ||
            //    !string.IsNullOrEmpty(FormDatas["driver_id"]) ||
            //    !string.IsNullOrEmpty(FormDatas["carrier_id"]) ||
            //    !string.IsNullOrEmpty(FormDatas["shipping_status_id"]))
            //{
            //    if (string.IsNullOrEmpty(FormDatas["scheduled_date_start"]) || string.IsNullOrEmpty(FormDatas["scheduled_date_end"]))
            //    {
            //        Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //        return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "SSAE_PSS_01", "Los campos 'Fecha Programada Inicia' y 'Fecha programada Final' son obligatorios."), JsonRequestBehavior.AllowGet);
            //    }

            //    DateTime scheduled_date_start = Convert.ToDateTime(FormDatas["scheduled_date_start"]);
            //    DateTime scheduled_date_end = Convert.ToDateTime(FormDatas["scheduled_date_end"]);

            //    shippingSchedule = shippingSchedule.Where(t => t.scheduled_date >= scheduled_date_start && t.scheduled_date <= scheduled_date_end);
            //}
            //else
            //{
                if (!string.IsNullOrEmpty(FormDatas["scheduled_date_start"]))
                {
                    DateTime scheduled_date_start = Convert.ToDateTime(FormDatas["scheduled_date_start"]);
                    shippingSchedule = shippingSchedule.Where(t => t.scheduled_date >= scheduled_date_start);
                }

                if (!string.IsNullOrEmpty(FormDatas["scheduled_date_end"]))
                {
                    DateTime scheduled_date_end = Convert.ToDateTime(FormDatas["scheduled_date_end"]);
                    shippingSchedule = shippingSchedule.Where(t => t.scheduled_date <= scheduled_date_end);
                }
            //}

            //    "shipping_number", // No. EMbarque
            //    "trailer_number", //No. Remolque"
            //    "driver_id", // Operadores
            //    "carrier_id", // Transportista
            //    "shipping_status_id", // Estatus embarque"
            //    "scheduled_date_start", // Fecha programada Incial and Fecha programada Final
            //    "scheduled_date_end", // Fecha programada Incial and Fecha programada Final

            foreach (var filter in FormDatas.AllKeys)
            {
                var value = FormDatas[filter];

                switch (filter)
                {
                    case "shipping_number":
                        //return Json(new { key = filter, value = value });
                        if (!string.IsNullOrEmpty(FormDatas["shipping_number"]))
                        {
                            shippingSchedule = shippingSchedule.Where(t => t.shipping_number == value);
                        }
                        break;

                    case "trailer_number":
                        //return Json(new { key = filter, value = value });
                        if (!string.IsNullOrEmpty(FormDatas["trailer_number"]))
                        {
                            shippingSchedule = shippingSchedule.Where(t => t.trailer_number == value);
                        }
                        break;

                    case "driver_id":
                        //return Json(new { key = filter, value = value });
                        
                        if (!string.IsNullOrEmpty(FormDatas["driver_id"]))
                        {
                            int drive_id = int.Parse(value);
                            shippingSchedule = shippingSchedule.Where(t => t.driver_id == drive_id);
                        }
                        break;

                    case "carrier_id":
                        //return Json(new { key = filter, value = value });
                        if (!string.IsNullOrEmpty(FormDatas["carrier_id"]))
                        {
                            int carrier_id = int.Parse(value);
                            shippingSchedule = shippingSchedule.Where(t => t.carrier_id == carrier_id);
                        }
                        break;

                    case "shipping_status_id":
                        //return Json(new { key = filter, value = value });
                        if (!string.IsNullOrEmpty(FormDatas["shipping_status_id"]))
                        {
                            int shipping_status_id = int.Parse(value);
                            shippingSchedule = shippingSchedule.Where(t => t.shipping_status_id == shipping_status_id);
                        }
                        break;
                }

            }

            // Esto es para la verificación del usuario logeado
            if (user != null)
            {
                // Si el usuario es Transportista
                if (user.auth == UserRoles.CARRIER)
                {
                    int carrier_id = user.id;
                    shippingSchedule = shippingSchedule.Where(t => t.carrier_id == carrier_id);
                }
                // Si el usuario es Traffic
                if (user.auth == UserRoles.TRAFFIC)
                {
                    int yard_mule_driver_id = user.id;
                    shippingSchedule = shippingSchedule; //.Where(t => t.initial_yard_mule_driver_id == yard_mule_driver_id || t.final_yard_mule_driver_id == yard_mule_driver_id);
                }

                if (user.auth == UserRoles.PROVIDER)
                {
                    shippingSchedule = shippingSchedule;
                    var currentProvider = await db.Provider.Where(t => t.user_id == currentUser.id).FirstOrDefaultAsync();
                    var clientIds = new List<int> { };
                    if (currentProvider == null)
                    {
                        clientIds = new List<int> { };
                    }
                    else
                    {
                        clientIds = db.Client.Where(s => s.provider_id == currentProvider.id).Select(t => t.id).ToList();
                    }

                    shippingSchedule = shippingSchedule.Where(t => clientIds.Contains(t.client_id.Value));

                    //if (FormDatas["client_id"] == null)
                    //{
                    //    Response.StatusCode = (int)HttpStatusCode.NoContent;
                    //    return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    int client_id = int.Parse(FormDatas["client_id"]);
                    //    shippingSchedule = shippingSchedule.Where(t => t.client_id == client_id);
                    //}
                }
            }
            ///////////////////////////////////////////////////

            //await shippingSchedule.OrderBy(t => t.shipping_priority).ToListAsync();
            var ordenado = await shippingSchedule.OrderBy(v => v.shipping_priority != null ? v.shipping_priority : 4).ThenBy(v => v.shipping_priority).ToListAsync();

            var statusCode = HttpStatusCode.OK;
            Response.StatusCode = (int)statusCode;
            return Json(ordenado, JsonRequestBehavior.AllowGet);
        }

        // Obtener Embarques por id
        [Route("~/ajax/shipping-schedules/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetShippingSchedulesById(int Id)
        {

            ShippingSchedule shippingSchedule = await db.ShippingSchedules.FindAsync(Id);

            return Json(shippingSchedule, JsonRequestBehavior.AllowGet);
        }

        // Agregar Embarque(Invividual)
        [Route("~/ajax/shipping-schedules")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostCreateAjax(ShippingSchedule shippingSchedule)
        {
            if (shippingSchedule == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."), JsonRequestBehavior.AllowGet);
            }

            if (!ModelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.
            IQueryable<ShippingSchedule> shippingCarrierCustomer = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);


            shippingCarrierCustomer = shippingCarrierCustomer.Where(t => t.shipping_number == shippingSchedule.shipping_number && t.carrier_id == shippingSchedule.carrier_id && t.client_id == shippingSchedule.client_id);

            ShippingSchedule ShippingCarrierCustomer = await shippingCarrierCustomer.FirstOrDefaultAsync();

            if (ShippingCarrierCustomer != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "SSAE_PCA_01", "El embarque ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.

            shippingSchedule.shipping_status_id = ShippingStatus.ON_REQUEST;
            shippingSchedule.shipping_number = shippingSchedule.shipping_number.Trim();
            shippingSchedule.truck_number = shippingSchedule.truck_number.Trim();
            shippingSchedule.license_plate = shippingSchedule.license_plate.Trim();
            shippingSchedule.origin = shippingSchedule.origin.Trim();
            shippingSchedule.destination = shippingSchedule.destination.Trim();
            shippingSchedule.container = shippingSchedule.container.Trim();

            db.ShippingSchedules.Add(shippingSchedule);
            await db.SaveChangesAsync();
            return Json(shippingSchedule, JsonRequestBehavior.AllowGet);

        }

        // Agregar Embarque(Layout)
        [Route("~/ajax/shipping-schedules/layout")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostLayoutCreateAjax(List<ShippingSchedule> shippingSchedules)
        {
            List<layoutErrors> AlllayoutError = new List<layoutErrors>();
            foreach (var shippingSchedule in shippingSchedules)
            {
                layoutErrors layoutErrors = new layoutErrors();
                layoutErrors.shipping_number = new List<string>();
                layoutErrors.carrier_id = new List<string>();
                layoutErrors.client_id = new List<string>();
                layoutErrors.driver_id = new List<string>();
                //layoutErrors.initial_yard_mule_driver_id = new List<string>();
                layoutErrors.trailer_status_id = new List<string>();
                layoutErrors.warehouse_id = new List<string>();

                //if (shippingSchedule == null)
                //{
                //    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                //    return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."), JsonRequestBehavior.AllowGet);
                //}

                //if (!ModelState.IsValid)
                //{
                //Response.StatusCode = (int)HttpStatusCode.BadRequest;
                //return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);

                //var errorList = ModelState.Values.SelectMany(m => m.Errors)
                //             .Select(e => e.ErrorMessage)
                //             .ToList();
                //layoutErrors.Add(errorList);
                //}

                // Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.
                IQueryable<ShippingSchedule> shippingCarrierCustomer = db.ShippingSchedules.Where(t => t.deleted_at == null)
                    .Include(c => c.carrier)
                    .Include(c => c.client)
                    .Include(c => c.client.contact)
                    .Include(c => c.client.contact.address)
                    .Include(c => c.driver)
                    .Include(c => c.driver.carrier)
                    .Include(c => c.driver.user)
                    .Include(c => c.driver.user.contact)
                    .Include(c => c.driver.user.contact.address)
                    .Include(c => c.driver.user.contact.email)
                    .Include(c => c.initial_yard_mule_driver)
                    .Include(c => c.shipping_status)
                    .Include(c => c.trailer_status)
                    .Include(c => c.warehouse)
                    .Include(c => c.warehouse.address);

                shippingCarrierCustomer = shippingCarrierCustomer.Where(t => t.shipping_number == shippingSchedule.shipping_number && t.carrier_id == shippingSchedule.carrier_id && t.client_id == shippingSchedule.client_id);

                ShippingSchedule ShippingCarrierCustomer = await shippingCarrierCustomer.FirstOrDefaultAsync();

                if (ShippingCarrierCustomer != null)
                {
                    
                    layoutErrors.shipping_number.Add("El embarque " + ShippingCarrierCustomer.shipping_number + " ya existe, favor de validar.");

                    //JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    //string JsonData = javaScriptSerializer.Serialize(ShippingCarrierCustomer);
                    //Response.StatusCode = (int)HttpStatusCode.Found;
                    //return Json(new HandlerReturn(HttpStatusCode.Found, true, "SSAE_PCA_01", "El embarque " + ShippingCarrierCustomer.shipping_number + " ya existe, favor de validar.", JsonData), JsonRequestBehavior.AllowGet);
                }
                // Termina Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.

                if (shippingSchedule.warehouse_id.GetType() != typeof(int))
                {
                    layoutErrors.warehouse_id.Add("El ID Almacén debe ser un número entero");
                }
                Warehouse warehose = await db.Warehouse.FindAsync(shippingSchedule.warehouse_id);
                if (warehose == null)
                {
                    layoutErrors.warehouse_id.Add("El ID Almacén no existe");
                }

                if (shippingSchedule.carrier_id.GetType() != typeof(int))
                {
                    layoutErrors.carrier_id.Add("El ID Transportista debe ser un número entero");
                }
                Carrier carrier = await db.Carrier.FindAsync(shippingSchedule.carrier_id);
                if (carrier == null)
                {
                    layoutErrors.carrier_id.Add("El ID Transportista no existe");
                }

                if (shippingSchedule.driver_id.GetType() != typeof(int))
                {
                    layoutErrors.driver_id.Add("El ID Operador debe ser un número entero");
                }
                Driver driver = await db.Driver.FindAsync(shippingSchedule.driver_id);
                if (driver == null)
                {
                    layoutErrors.driver_id.Add("El ID Transportista no existe");
                }

                //if (shippingSchedule.initial_yard_mule_driver_id.GetType() != typeof(int))
                //{
                //    layoutErrors.initial_yard_mule_driver_id.Add("El ID Mulero debe ser un número entero");
                //}
                //YardMuleDriver yardMuleDriver = await db.YardMuleDriver.FindAsync(shippingSchedule.initial_yard_mule_driver_id);
                //if (yardMuleDriver == null)
                //{
                //    layoutErrors.initial_yard_mule_driver_id.Add("El ID Mulero no existe");
                //}

                if (shippingSchedule.trailer_status_id.GetType() != typeof(int))
                {
                    layoutErrors.trailer_status_id.Add("El ID Estatus Remolque debe ser un número entero");
                }
                TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(shippingSchedule.trailer_status_id);
                if (trailerStatus == null)
                {
                    layoutErrors.trailer_status_id.Add("El ID Estatus Remolque no existe");
                }

                if (shippingSchedule.client_id.GetType() != typeof(int))
                {
                    layoutErrors.client_id.Add("El ID Cliente debe ser un número entero");
                }
                Client client = await db.Client.FindAsync(shippingSchedule.client_id);
                if (client == null)
                {
                    layoutErrors.client_id.Add("El ID Cliente no existe");
                }

                if (layoutErrors.warehouse_id.Count() > 0 || layoutErrors.carrier_id.Count() > 0 || layoutErrors.driver_id.Count() > 0 || layoutErrors.client_id.Count() > 0 || layoutErrors.trailer_status_id.Count() > 0|| layoutErrors.shipping_number.Count() > 0)
                {
                    AlllayoutError.Add(layoutErrors);
                }
            }

            if(AlllayoutError.Count() > 0)
            {
                // Hay errores
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(AlllayoutError, JsonRequestBehavior.AllowGet);
            }
            

            foreach (var shippingSchedule in shippingSchedules)
            {
                shippingSchedule.shipping_status_id = ShippingStatus.ON_REQUEST;
                shippingSchedule.shipping_number = shippingSchedule.shipping_number.Trim();
                shippingSchedule.truck_number = shippingSchedule.truck_number.Trim();
                shippingSchedule.license_plate = shippingSchedule.license_plate.Trim();
                shippingSchedule.origin = shippingSchedule.origin.Trim();
                shippingSchedule.destination = shippingSchedule.destination.Trim();
                shippingSchedule.container = shippingSchedule.container.Trim();

                db.ShippingSchedules.Add(shippingSchedule);
                await db.SaveChangesAsync();
            }

            Response.StatusCode = (int)HttpStatusCode.Accepted;
            return Json(new HandlerReturn(HttpStatusCode.Accepted, false, null, "Layout de Embarques cargado satisfactoriamente."), JsonRequestBehavior.AllowGet);
        }

        // Editar Embarque
        [Route("~/ajax/shipping-schedules")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutEditAjax(ShippingSchedule shippingSchedule)
        {
            if (shippingSchedule == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."), JsonRequestBehavior.AllowGet);
            }

            if (!ModelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.
            IQueryable<ShippingSchedule> shippingSheduleSearch = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSheduleSearch = shippingSheduleSearch.Where(t => t.id != shippingSchedule.id && t.shipping_number == shippingSchedule.shipping_number && t.carrier_id == shippingSchedule.carrier_id && t.client_id == shippingSchedule.client_id);

            ShippingSchedule ShippingCarrierCustomer = await shippingSheduleSearch.FirstOrDefaultAsync();

            if (ShippingCarrierCustomer != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "SSAE_PCA_01", "El embarque " + ShippingCarrierCustomer.shipping_number + " ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.

            ShippingSchedule shippingScheduleCurrent = await db.ShippingSchedules.FindAsync(shippingSchedule.id);

            shippingScheduleCurrent.shipping_number = shippingSchedule.shipping_number.Trim();
            shippingScheduleCurrent.scheduled_date = shippingSchedule.scheduled_date;
            shippingScheduleCurrent.warehouse_id = shippingSchedule.warehouse_id;
            shippingScheduleCurrent.carrier_id = shippingSchedule.carrier_id;
            shippingScheduleCurrent.trailer_number = shippingSchedule.trailer_number.Trim();
            shippingScheduleCurrent.truck_number = shippingSchedule.truck_number.Trim();
            shippingScheduleCurrent.license_plate = shippingSchedule.license_plate.Trim();
            shippingScheduleCurrent.driver_id = shippingSchedule.driver_id;
            shippingScheduleCurrent.trailer_status_id = shippingSchedule.trailer_status_id;
            shippingScheduleCurrent.client_id = shippingSchedule.client_id;
            shippingScheduleCurrent.origin = shippingSchedule.origin.Trim();
            shippingScheduleCurrent.destination = shippingSchedule.destination.Trim();
            shippingScheduleCurrent.container = shippingSchedule.container.Trim();

            db.Entry(shippingScheduleCurrent).State = EntityState.Modified;

            await db.SaveChangesAsync();
            return Json(shippingScheduleCurrent, JsonRequestBehavior.AllowGet);
        }


        // Obtener Reporte de Tiempos de mulero
        [Route("~/ajax/shipping-schedules/report/yard-mule-driver/times")]
        [HttpGet]
        public async Task<ActionResult> GetShippingSchedulesReportYardMuleDriverTimes(FormCollection FormData)
        {
            // Esto es para la verificación del usuario logeado
            int user_id = int.Parse(Request.Headers.GetValues("user_id").First());
            User user = await db.User.FindAsync(user_id);
            ////////////////////////////////////////////////////

            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);


            if (!string.IsNullOrEmpty(FormData["scheduled_date_start"]) && !string.IsNullOrEmpty(FormData["scheduled_date_end"]))
            {
                DateTime scheduled_date_start = Convert.ToDateTime(FormData["scheduled_date_start"]);
                DateTime scheduled_date_end = Convert.ToDateTime(FormData["scheduled_date_end"]);

                shippingSchedule = shippingSchedule.Where(t => t.scheduled_date >= scheduled_date_start && t.scheduled_date <= scheduled_date_end);
            }

            foreach (var filter in FormData.AllKeys)
            {
                var value = FormData[filter];

                switch (filter)
                {
                    case "shipping_number":
                        shippingSchedule = shippingSchedule.Where(t => t.shipping_number == value);
                        break;
                }

            }

            ///////////////////////////////////////////////////
            ///

            shippingSchedule = shippingSchedule.Where(t => t.final_yard_mule_driver_id == user_id || t.initial_yard_mule_driver_id == user_id);

            //await shippingSchedule.OrderBy(t => t.shipping_priority).Select(t => new { t.shipping_number, t.scheduled_date, t.ramp_and_yard_mule_driver_assigned_at, t.moved_from_parking_space_at, t.in_parking_space_at, t.moved_from_ramp_at, t.in_ramp_at, t.created_at, t.updated_at }).ToListAsync();

            var ordenado = await shippingSchedule.OrderBy(v => v.shipping_priority != null ? v.shipping_priority : 4).ThenBy(v => v.shipping_priority).Select(t => new { t.shipping_number, t.scheduled_date, t.ramp_and_yard_mule_driver_assigned_at, t.moved_from_parking_space_at, t.in_parking_space_at, t.moved_from_ramp_at, t.in_ramp_at, t.created_at, t.updated_at }).ToListAsync();


            return Json(ordenado, JsonRequestBehavior.AllowGet);
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
