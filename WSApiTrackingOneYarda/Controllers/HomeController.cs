﻿using System.Web.Mvc;

namespace WSApiTrackingOneYarda.Controllers
{
    public class HomeController : AuthenticatedController
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            //return View();
            return RedirectToAction("Index", "ShippingSchedulesAjax");
        }

        [Route("logout")]
        public ActionResult Logout()
        {
            if (Session["Login"] != null)
            {
                Session.Abandon();
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
