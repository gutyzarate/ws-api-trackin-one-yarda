﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("user-roles")]
    public class UserRolesAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: UserRoles
        [Route]
        public async Task<ActionResult> Index()
        {
            return View();
        }

        // GET: UserRoles/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserRoles userRoles = await db.UserRoles.FindAsync(id);
            if (userRoles == null)
            {
                return HttpNotFound();
            }
            return View(userRoles);
        }

        // GET: UserRoles/Create
        [Route("create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserRoles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,auth,name,label")] UserRoles userRoles)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.UserRoles.Add(userRoles);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(userRoles);
        //}

        // GET: UserRoles/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserRoles userRoles = await db.UserRoles.FindAsync(id);
            if (userRoles == null)
            {
                return HttpNotFound();
            }
            return View(userRoles);
        }

        // POST: UserRoles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,auth,name,label")] UserRoles userRoles)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(userRoles).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(userRoles);
        //}

        // GET: UserRoles/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserRoles userRoles = await db.UserRoles.FindAsync(id);
            if (userRoles == null)
            {
                return HttpNotFound();
            }
            return View(userRoles);
        }

        // POST: UserRoles/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    UserRoles userRoles = await db.UserRoles.FindAsync(id);
        //    db.UserRoles.Remove(userRoles);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener Perfiles
        [Route("~/ajax/user-roles")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxUserRoles()
        {
            IQueryable<UserRoles> UserRoles = db.UserRoles;
            List<UserRoles> userRoles = await UserRoles.ToListAsync();

            if (userRoles == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(userRoles, JsonRequestBehavior.AllowGet);
        }

        // Agregar perfiles
        [Route("~/ajax/user-roles")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxUserRoles(string label)
        {
            if (label == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el perfil no existe
            IQueryable<UserRoles> userRoles = db.UserRoles;
            userRoles = userRoles.Where(t => t.label == label);

            UserRoles ExistUserRole = await userRoles.FirstOrDefaultAsync();

            if (ExistUserRole != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAUR_01", "El perfil ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el perfil no existe

            UserRoles userRole = new UserRoles();
            userRole.label = label;
            userRole.name = label.Replace(' ', '_').ToLower();
            var countUserRoles = await db.UserRoles.ToListAsync();
            userRole.auth = countUserRoles.Count();

            db.UserRoles.Add(userRole);
            await db.SaveChangesAsync();

            return Json(userRole, JsonRequestBehavior.AllowGet);
        }

        // Editar perfiles
        [Route("~/ajax/user-roles")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxUserRoles(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["label"]) || string.IsNullOrEmpty(formData["id"]))
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el perfil no existe
            IQueryable<UserRoles> userRoles = db.UserRoles;
            string label = formData["label"];
            int id = int.Parse(formData["id"]);
            userRoles = userRoles.Where(t => t.label == label && t.id != id);

            UserRoles ExistUserRole = await userRoles.FirstOrDefaultAsync();

            if (ExistUserRole != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAUR_01", "El perfil ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el perfil no existe

            UserRoles userRole = await db.UserRoles.FindAsync(id);
            userRole.label = label;
            userRole.name = label.Replace(' ', '_').ToLower();
            //var countUserRoles = await db.UserRoles.ToListAsync();
            //userRole.auth = countUserRoles.Count();

            db.Entry(userRole).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(userRole, JsonRequestBehavior.AllowGet);
        }

        // Borrar Perfil
        [Route("~/ajax/user-roles")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxUserRoles(int id)
        {
            UserRoles userRole = await db.UserRoles.FindAsync(id);

            db.UserRoles.Remove(userRole);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El perfil ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
