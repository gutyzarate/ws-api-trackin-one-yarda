﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("shipping-statuses")]
    public class ShippingStatusAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: ShippingStatus
        [Route]
        public async Task<ActionResult> Index()
        {
            ViewBag.model = "shipping-statuses";
            ViewBag.modelLabel = "Estatus de Embarque";
            return View(await db.ShippingStatus.ToListAsync());
        }

        // GET: ShippingStatus/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShippingStatus shippingStatus = await db.ShippingStatus.FindAsync(id);
            if (shippingStatus == null)
            {
                return HttpNotFound();
            }
            return View(shippingStatus);
        }

        // GET: ShippingStatus/Create
        [Route("create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ShippingStatus/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,name,label")] ShippingStatus shippingStatus)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ShippingStatus.Add(shippingStatus);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(shippingStatus);
        //}

        // GET: ShippingStatus/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShippingStatus shippingStatus = await db.ShippingStatus.FindAsync(id);
            if (shippingStatus == null)
            {
                return HttpNotFound();
            }
            return View(shippingStatus);
        }

        // POST: ShippingStatus/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,name,label")] ShippingStatus shippingStatus)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(shippingStatus).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(shippingStatus);
        //}

        // GET: ShippingStatus/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShippingStatus shippingStatus = await db.ShippingStatus.FindAsync(id);
            if (shippingStatus == null)
            {
                return HttpNotFound();
            }
            return View(shippingStatus);
        }

        // POST: ShippingStatus/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    ShippingStatus shippingStatus = await db.ShippingStatus.FindAsync(id);
        //    db.ShippingStatus.Remove(shippingStatus);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        // AJAX REQUEST //////////////////////////////////////////////////////////////

        // Obtener Estados de embarque
        [Route("~/ajax/shipping-statuses")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxShippingStatuses()
        {
            IQueryable<ShippingStatus> shippingStatuses = db.ShippingStatus;
            List<ShippingStatus> ShippingStatuses = await shippingStatuses.ToListAsync();

            if (ShippingStatuses.Count() == 0)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(ShippingStatuses, JsonRequestBehavior.AllowGet);
        }

        // Obtener Estados de embarque por id
        [Route("~/ajax/shipping-statuses/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxShippingStatusesById(int id)
        {
            ShippingStatus shippingStatuses = await db.ShippingStatus.FindAsync(id);

            if (shippingStatuses == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(shippingStatuses, JsonRequestBehavior.AllowGet);
        }

        // Agregar perfiles
        [Route("~/ajax/shipping-statuses")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxShippingStatus(ShippingStatus shipping_status)
        {
            if (shipping_status.label == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el estado de embarque no existe
            IQueryable<ShippingStatus> ShippingStatus = db.ShippingStatus;
            ShippingStatus = ShippingStatus.Where(t => t.label == shipping_status.label);

            ShippingStatus ExistShippingStatus = await ShippingStatus.FirstOrDefaultAsync();

            if (ExistShippingStatus != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PASS_01", "El estado de embarque ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el estado de embarque no existe

            ShippingStatus shippingStatus = new ShippingStatus();
            shippingStatus.label = shipping_status.label;
            shippingStatus.name = shipping_status.label.Replace(' ', '_').ToLower();

            db.ShippingStatus.Add(shippingStatus);
            await db.SaveChangesAsync();

            return Json(shippingStatus, JsonRequestBehavior.AllowGet);
        }

        // Editar perfiles
        [Route("~/ajax/shipping-statuses")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxShippingStatus(ShippingStatus shipping_status)
        {
            if (shipping_status.label == null || shipping_status.id == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el perfil no existe
            IQueryable<ShippingStatus> ShippingStatus = db.ShippingStatus;
            string label = shipping_status.label;
            int id = shipping_status.id;
            ShippingStatus = ShippingStatus.Where(t => t.label == label && t.id != id);

            ShippingStatus ExistShippingStatus = await ShippingStatus.FirstOrDefaultAsync();

            if (ExistShippingStatus != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PASS_01", "El estado de embarque ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el perfil no existe

            ShippingStatus shippingStatus = await db.ShippingStatus.FindAsync(id);
            shippingStatus.label = label;
            shippingStatus.name = label.Replace(' ', '_').ToLower();

            db.Entry(shippingStatus).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(shippingStatus, JsonRequestBehavior.AllowGet);
        }

        // Borrar Perfil
        [Route("~/ajax/shipping-statuses")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxShippingStatus(int id)
        {
            ShippingStatus shippingStatus = await db.ShippingStatus.FindAsync(id);

            db.ShippingStatus.Remove(shippingStatus);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El estado de embarque ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
