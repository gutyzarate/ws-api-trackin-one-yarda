﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("drivers")]
    public class DriversAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: Drivers
        [Route]
        public async Task<ActionResult> Index()
        {
            @ViewBag.carriers = await db.Carrier.ToListAsync();

            return View();
        }

        // GET: Drivers/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<Driver> Driver = db.Driver.Where(t => t.deleted_at == null).Include(d => d.user);
            Driver = Driver.Where(t => t.id == id);
            Driver driver = await Driver.FirstOrDefaultAsync();
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // GET: Drivers/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.carrier_id = new SelectList(db.Carrier, "id", "name");
            ViewBag.user_id = new SelectList(db.User, "id", "name");
            return View();
        }

        // POST: Drivers/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[Route("create")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,first_name,last_name,carrier_id,social_security_number,driver_license_number,user_id,created_at,updated_at,deleted_at")] Driver driver)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Driver.Add(driver);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.carrier_id = new SelectList(db.Carrier, "id", "name", driver.carrier_id);
        //    ViewBag.user_id = new SelectList(db.User, "id", "name", driver.user_id);
        //    return View(driver);
        //}

        // GET: Drivers/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<Driver> Driver = db.Driver.Where(t => t.deleted_at == null).Include(d => d.user);
            Driver = Driver.Where(t => t.id == id);
            Driver driver = await Driver.FirstOrDefaultAsync();
            if (driver == null)
            {
                return HttpNotFound();
            }
            ViewBag.carrier_id = new SelectList(db.Carrier, "id", "name", driver.carrier_id);
            ViewBag.user_id = new SelectList(db.User, "id", "name", driver.user_id);
            return View(driver);
        }

        // POST: Drivers/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[Route("edit/{id}")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,first_name,last_name,carrier_id,social_security_number,driver_license_number,user_id,created_at,updated_at,deleted_at")] Driver driver)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(driver).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.carrier_id = new SelectList(db.Carrier, "id", "name", driver.carrier_id);
        //    ViewBag.user_id = new SelectList(db.User, "id", "name", driver.user_id);
        //    return View(driver);
        //}

        // GET: Drivers/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<Driver> Driver = db.Driver.Where(t => t.deleted_at == null).Include(d => d.user);
            Driver = Driver.Where(t => t.id == id);
            Driver driver = await Driver.FirstOrDefaultAsync();
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // POST: Drivers/Delete/5
        //[Route("delete/{id}")]
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Driver driver = await db.Driver.FindAsync(id);
        //    db.Driver.Remove(driver);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener Transportistas
        [Route("~/ajax/drivers")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxDriver(FormCollection filters)
        {
            IQueryable<Driver> Driver = db.Driver.Where(t => t.deleted_at == null);
            Driver = Driver.Include(d => d.carrier).Include(d => d.user);

            foreach (var filter in filters.AllKeys)
            {
                var value = filters[filter];

                switch (filter)
                {
                    case "shipping_number":
                        var shippingSchedule = db.ShippingSchedules.Where(t => t.shipping_number == value).Select(t => t.driver_id).ToList();
                        Driver = Driver.Where(t => shippingSchedule.Contains(t.id));
                        break;

                    case "driver_id":
                        var driver_id = int.Parse(value);
                        Driver = Driver.Where(t => t.id == driver_id);
                        break;

                    case "carrier_id":
                        var carrier_id = int.Parse(value);
                        Driver = Driver.Where(t => t.id == carrier_id);
                        break;
                }
            }

            List<Driver> driver = await Driver.ToListAsync();

            if (driver == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }

            return Json(driver, JsonRequestBehavior.AllowGet);
        }

        // Agregar transportista
        [Route("~/ajax/drivers")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxDriver(FormCollection formData, HttpPostedFileBase picture_file, List<HttpPostedFileBase> document_files)
        {

            if (string.IsNullOrEmpty(formData["first_name"]) ||
                string.IsNullOrEmpty(formData["last_name"]) ||
                string.IsNullOrEmpty(formData["carrier_id"]) ||
                string.IsNullOrEmpty(formData["social_security_number"]) ||
                string.IsNullOrEmpty(formData["driver_license_number"]) ||
                picture_file == null
                )
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            foreach (HttpPostedFileBase document_file in document_files)
            {
                if(document_file == null)
                {
                    return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    continue;
                }
            }

            if (!isPictureAllowed(picture_file))
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "D_PA_05", "La imagen del operador tiene un formato inválido, favor de validar. Formatos permitidos: jpg, png ó gif. Archivo: " + picture_file.FileName), JsonRequestBehavior.AllowGet);
            }
            foreach (HttpPostedFileBase document_file in document_files)
            {
                if (!isDocumentAllowed(document_file))
                {
                    return Json(new HandlerReturn(HttpStatusCode.Found, true, "D_PA_06", "Se encontró un documento con formato inválido, favor de validar. Formatos permitidos: pdf. Archivo: " + document_file.FileName), JsonRequestBehavior.AllowGet);
                }
            }

            string table = "tblDriver";
            string pathMedia = "~/media";

            // Creamos la carpeta de media
            var path_media = Server.MapPath(pathMedia);
            var directory_media = new DirectoryInfo(path_media);

            if (directory_media.Exists == false)
            {
                directory_media.Create();
            }


            // Validar que el operador no existe
            string social_security_number = formData["social_security_number"].Trim();
            IQueryable<Driver> Driver = db.Driver.Where(t => t.deleted_at == null);
            Driver = Driver.Where(t => t.social_security_number == social_security_number);
            var ExistDriver = await Driver.AnyAsync();

            if (ExistDriver)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "D_PA_02", "El número del IMSS ya se usa para otro operador, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el operador no existe

            // Creamos al usuario en la tabla adm.tblUser
            User UserNew = new User();
            UserNew.name = formData["first_name"];
            string[] apellidos = formData["last_name"].Split(' ');
            UserNew.first_last_name = apellidos[0];
            if (apellidos.Count() > 1)
            {
                string concatApellidos = null;

                foreach (var apellido in apellidos)
                {
                    if (apellido == apellidos.First())
                    {
                        continue;
                    }

                    concatApellidos = concatApellidos + apellido + " ";
                }
                UserNew.second_last_name = concatApellidos.Trim();
            }

            UserNew.auth = UserRoles.DRIVER;
            UserNew.status = true;
            db.User.Add(UserNew);
            await db.SaveChangesAsync();

            // Creamos el operador
            Driver driver = new Driver();

            driver.first_name = formData["first_name"];
            driver.last_name = formData["last_name"];
            driver.carrier_id = int.Parse(formData["carrier_id"]);
            driver.social_security_number = formData["social_security_number"];
            driver.driver_license_number = formData["driver_license_number"];
            driver.user_id = UserNew.id;

            db.Driver.Add(driver);
            await db.SaveChangesAsync();
            // End Creamos el operador

            // Insertamos la imagen de perfil en media
            string picture_collection_name = Collection.Picture;
            string picture_name = Collection.Picture + "-" + "driver" + "-" + driver.id + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string picture_mime_type = Path.GetExtension(picture_file.FileName);
            string picture_file_name = picture_name + picture_mime_type;
            string picture_path = Path.Combine(Server.MapPath(pathMedia), Path.GetFileName(picture_file_name));

            Media picture = new Media();

            picture.collection_name = picture_collection_name;
            picture.name = picture_name;
            picture.mime_type = picture_mime_type;
            picture.file_name = picture_file_name;
            picture.table = table;
            picture.object_id = driver.id;

            db.Media.Add(picture);
            await db.SaveChangesAsync();

            picture_file.SaveAs(picture_path);
            // End Insertamos la imagen de perfil en media

            // Insertamos la documentación en media
            int number_file = 1;
            foreach (HttpPostedFileBase document_file in document_files)
            {
                string document_collection_name = Collection.Document;
                string document_name = Collection.Document + "-" + number_file + "-" + "driver" + "-" + driver.id + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
                string document_mime_type = Path.GetExtension(document_file.FileName);
                string document_file_name = document_name + document_mime_type;
                string document_path = Path.Combine(Server.MapPath(pathMedia), Path.GetFileName(document_file_name));

                Media document = new Media();

                document.collection_name = document_collection_name;
                document.name = document_name;
                document.mime_type = document_mime_type;
                document.file_name = document_file_name;
                document.table = table;
                document.object_id = driver.id;

                db.Media.Add(document);
                await db.SaveChangesAsync();

                document_file.SaveAs(document_path);
                number_file++;
            }
            // End Insertamos la documentación en media

            return Json(driver, JsonRequestBehavior.AllowGet);
        }

        // Editar transportista
        [Route("~/ajax/drivers")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PuttAjaxDriver(FormCollection formData, HttpPostedFileBase picture_file, IEnumerable<HttpPostedFileBase> document_files)
        {

            if (string.IsNullOrEmpty(formData["id"]) ||
                string.IsNullOrEmpty(formData["first_name"]) ||
                string.IsNullOrEmpty(formData["last_name"]) ||
                string.IsNullOrEmpty(formData["carrier_id"]) ||
                string.IsNullOrEmpty(formData["social_security_number"]) ||
                string.IsNullOrEmpty(formData["driver_license_number"])
                )
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            if (picture_file != null)
            {
                if (!isPictureAllowed(picture_file))
                {
                    return Json(new HandlerReturn(HttpStatusCode.Found, true, "D_PA_05", "La imagen del operador tiene un formato inválido, favor de validar. Formatos permitidos: jpg, png ó gif. Archivo: " + picture_file.FileName), JsonRequestBehavior.AllowGet);
                }
            }

            if (document_files != null)
            {
                foreach (HttpPostedFileBase document_file in document_files)
                {
                    if (document_file == null)
                    {
                        continue;
                    }

                    if (!isDocumentAllowed(document_file))
                    {
                        return Json(new HandlerReturn(HttpStatusCode.Found, true, "D_PA_06", "Se encontró un documento con formato inválido, favor de validar. Formatos permitidos: pdf. Archivo: " + document_file.FileName), JsonRequestBehavior.AllowGet);
                    }
                }
            }

            string table = "tblDriver";
            string pathMedia = "~/media";

            // Creamos la carpeta de media
            var path_media = Server.MapPath(pathMedia);
            var directory_media = new DirectoryInfo(path_media);

            if (directory_media.Exists == false)
            {
                directory_media.Create();
            }


            // Validar que el operador no existe
            string social_security_number = formData["social_security_number"].Trim();
            int driver_id = int.Parse(formData["id"]);

            IQueryable<Driver> Driver = db.Driver.Where(t => t.deleted_at == null);
            Driver = Driver.Where(t => t.social_security_number == social_security_number && t.id != driver_id);
            var ExistDriver = await Driver.AnyAsync();

            if (ExistDriver)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "D_PA_02", "El No. IMSS ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el operador no existe

            // Editamos el operador
            IQueryable<Driver> DriverToEdit = db.Driver.Where(t => t.deleted_at == null);
            DriverToEdit = DriverToEdit.Where(t => t.id == driver_id);
            Driver driver = await DriverToEdit.FirstAsync();

            driver.first_name = formData["first_name"];
            driver.last_name = formData["last_name"];
            driver.carrier_id = int.Parse(formData["carrier_id"]);
            driver.social_security_number = formData["social_security_number"];
            driver.driver_license_number = formData["driver_license_number"];

            db.Entry(driver).State = EntityState.Modified;
            await db.SaveChangesAsync();
            // End Editamos el operador

            // Editamos al usuario
            User User = driver.user;
            User.name = formData["first_name"];
            string[] apellidos = formData["last_name"].Split(' ');
            User.first_last_name = apellidos[0];
            if (apellidos.Count() > 1)
            {
                string concatApellidos = null;

                foreach (var apellido in apellidos)
                {
                    if (apellido == apellidos.First())
                    {
                        continue;
                    }

                    concatApellidos = concatApellidos + apellido + " ";
                }
                User.second_last_name = concatApellidos.Trim();
            }
            db.Entry(User).State = EntityState.Modified;
            await db.SaveChangesAsync();
            // End Editamos al usuario


            // Actualizamos la imagen de perfil en media
            if (picture_file != null)
            {
                // Obtenemos picture actual
                Media pictureCurrent = await db.Media.Where(t => t.object_id == driver_id && t.collection_name == Collection.Picture && t.deleted_at == null).FirstOrDefaultAsync();
                // Verificamos que si exista una imagen
                if (pictureCurrent != null)
                {
                    // Recreamos el path del archivo
                    string pathPicture = Server.MapPath(pathMedia + "/" + pictureCurrent.file_name);

                    // si existe la eliminamos para poder reemplazarla
                    if (System.IO.File.Exists(pathPicture))
                    {
                        System.IO.File.Delete(pathPicture);
                        db.Media.Remove(pictureCurrent);
                        await db.SaveChangesAsync();
                    }
                }

                // Insertamos la nueva imagen
                string picture_collection_name = Collection.Picture;
                string picture_name = Collection.Picture + "-" + "driver" + "-" + driver.id + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
                string picture_mime_type = Path.GetExtension(picture_file.FileName);
                string picture_file_name = picture_name + picture_mime_type;
                string picture_path = Path.Combine(Server.MapPath(pathMedia), Path.GetFileName(picture_file_name));

                Media picture = new Media();

                picture.collection_name = picture_collection_name;
                picture.name = picture_name;
                picture.mime_type = picture_mime_type;
                picture.file_name = picture_file_name;
                picture.table = table;
                picture.object_id = driver.id;

                db.Media.Add(picture);
                await db.SaveChangesAsync();

                picture_file.SaveAs(picture_path);
            }

            // End Actualizamos la imagen de perfil en media

            // Insertamos la documentación en media
            if (document_files != null)
            {
                int number_file = 1;
                foreach (HttpPostedFileBase document_file in document_files)
                {
                    if (document_file == null)
                    {
                        continue;
                    }

                    string document_collection_name = Collection.Document;
                    string document_name = Collection.Document + "-" + number_file + "-" + "driver" + "-" + driver.id + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    string document_mime_type = Path.GetExtension(document_file.FileName);
                    string document_file_name = document_name + document_mime_type;
                    string document_path = Path.Combine(Server.MapPath(pathMedia), Path.GetFileName(document_file_name));

                    Media document = new Media();

                    document.collection_name = document_collection_name;
                    document.name = document_name;
                    document.mime_type = document_mime_type;
                    document.file_name = document_file_name;
                    document.table = table;
                    document.object_id = driver.id;

                    db.Media.Add(document);
                    await db.SaveChangesAsync();

                    document_file.SaveAs(document_path);
                    number_file++;
                }
            }

            // End Insertamos la documentación en media

            return Json(driver, JsonRequestBehavior.AllowGet);
        }

        // Obtener imagen de perfil Operador
        [Route("~/ajax/drivers/{id:int}/media")]
        [HttpGet]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> GetAjaxDriverPicture(int id)
        {
            // Validar que el operador no existe
            var driver = await db.Driver.FindAsync(id);

            if (driver == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el operador no existe

            var AllMedia = await db.Media.Where(t => t.table == "tblDriver" && t.deleted_at == null && t.object_id == id).ToListAsync();

            List<FileMedia> Files = new List<FileMedia>();

            foreach (var media in AllMedia)
            {
                var file = new FileMedia();
                file.collection = media.collection_name;
                file.name = media.name;
                file.file_name = media.file_name;

                string pathMedia = "media";
                var base_url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                file.uri = base_url + pathMedia + "/" + media.file_name;

                Files.Add(file);
            }

            return Json(Files, JsonRequestBehavior.AllowGet);
        }

        [Route("~/ajax/drivers/{id:int}/documents")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxDriverDocuments(int id)
        {
            string salida = id + "_documentacion.PDF";

            try
            {
                Document document = new Document();
                String URL_Fisica_CT = Server.MapPath("~/media/" + salida);

                using (FileStream newFileStream = new FileStream(URL_Fisica_CT, FileMode.Create))
                {
                    var driver = db.Driver.Find(id);

                    if (driver == null)
                    {
                        return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "D_PA_06", "No se encontró archivo"), JsonRequestBehavior.AllowGet);
                    }

                    var AllMedia = db.Media.Where(t => t.table == "tblDriver" && t.deleted_at == null && t.object_id == id).ToList();

                    PdfCopy writer = new PdfCopy(document, newFileStream);

                    if (writer == null)
                    {
                        return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "D_PA_06", "No se encontró archivo"), JsonRequestBehavior.AllowGet);
                    }

                    document.Open();

                    foreach (var item in AllMedia)
                    {
                        if (item.collection_name == "document")
                        {
                            String nombre_PDF = Server.MapPath("~/media/" + item.file_name);
                            PdfReader reader = new PdfReader(nombre_PDF);
                            reader.ConsolidateNamedDestinations();
                            for (int i = 1; i <= reader.NumberOfPages; i++)
                            {
                                PdfImportedPage page = writer.GetImportedPage(reader, i);
                                writer.AddPage(page);
                            }
                            PRAcroForm form = reader.AcroForm;
                            if (form != null)
                            {
                                writer.CopyAcroForm(reader);
                            }
                            reader.Close();
                        }
                    }

                    writer.Close();
                    document.Close();
                }
            }
            catch (Exception ex)
            {
                // salida = "";

                return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "D_PA_06", "No se encontró archivo"), JsonRequestBehavior.AllowGet);
            }

            string pathMedia = "media";
            var base_url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

            return Json(new { uri = base_url + pathMedia + '/' + salida }, JsonRequestBehavior.AllowGet);
        }

        // Borrar Transportista
        [Route("~/ajax/drivers")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxDriver(int id)
        {
            Driver driver = await db.Driver.FindAsync(id);


            User user = driver.user;
            user.status = false;
            db.Entry(user).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Driver.Remove(driver);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El Operador ha sido eliminado con éxito."));
        }
        // END AJAX REQUEST //////////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool isPictureAllowed(HttpPostedFileBase file)
        {
            string[] mime_types_allowed = { "jpg", "png", "gif" };
            string file_mime_type = Path.GetExtension(file.FileName).ToLower();
            if (mime_types_allowed.Contains(file_mime_type.Replace(".", "")))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private bool isDocumentAllowed(HttpPostedFileBase file)
        {
            string[] mime_types_allowed = { "pdf" };
            string file_mime_type = Path.GetExtension(file.FileName);
            if (mime_types_allowed.Contains(file_mime_type.Replace(".", "")))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
