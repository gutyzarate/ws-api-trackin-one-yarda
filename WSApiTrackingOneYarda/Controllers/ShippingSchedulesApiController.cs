﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;
using IHttpActionResult = System.Web.Http.IHttpActionResult;
using RouteAttribute = System.Web.Http.RouteAttribute;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("api/shipping-schedules")]
    public class ShippingSchedulesApiController : ApiController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: api/ShippingSchedules
        [Route("", Name = "GetShippingSchedules")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> GetShippingSchedules(FilterShippingSchedule filters)
        {
            //return Json(filters);
            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);

            // Se agrega el WHERE a la consulta segun el dato recibido
            if (filters != null)
            {
                foreach (var filter in filters.GetType().GetProperties())
                {
                    var value = filter.GetValue(filters, null);
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    switch (filter.Name)
                    {

                        case "shipping_number":
                            if (value == null)
                            {
                                continue;
                            }
                            shippingSchedule = shippingSchedule.Where(t => t.shipping_number == value.ToString());
                            break;

                        case "carrier_name":
                            if (value == null)
                            {
                                continue;
                            }
                            var carrier = db.Carrier.Where(c => c.name == value.ToString()).First();
                            if (carrier != null)
                            {
                                shippingSchedule = shippingSchedule.Where(t => t.carrier_id == carrier.id);
                            }
                            break;

                        case "license_plate":
                            if (value == null)
                            {
                                continue;
                            }
                            shippingSchedule = shippingSchedule.Where(t => t.license_plate == value.ToString());
                            break;

                        case "container":
                            if (value == null)
                            {
                                continue;
                            }
                            shippingSchedule = shippingSchedule.Where(t => t.container == value.ToString());
                            break;

                        case "trailer_number":
                            if (value == null)
                            {
                                continue;
                            }
                            shippingSchedule = shippingSchedule.Where(t => t.trailer_number == value.ToString());
                            break;

                        case "truck_number":
                            if (value == null)
                            {
                                continue;
                            }
                            shippingSchedule = shippingSchedule.Where(t => t.truck_number == value.ToString());
                            break;
                    }

                }
            }

            //await shippingSchedule.OrderBy(t => t.shipping_priority).ThenByDescending(t => t.shipping_number).ToListAsync();
            var ordenado = await shippingSchedule.OrderBy(v => v.shipping_priority != null ? v.shipping_priority : 4).ThenBy(v => v.shipping_priority).ToListAsync();

            return Ok(ordenado);
        }

        // Obtener embarques pendientes en cajón
        [Route("in-parking-space")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> GetShippingSchedulesInParkingSpace()
        {
            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSchedule = shippingSchedule.Where(t => t.shipping_status_id == 3);
            await shippingSchedule.OrderBy(t => t.created_at).ToListAsync();
            return Ok(shippingSchedule);
        }

        // Obtener Embarques Pendientes en rampa
        [Route("in-ramp")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> GetShippingSchedulesInRamp()
        {
            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSchedule = shippingSchedule.Where(t => t.shipping_status_id == 4);
            await shippingSchedule.OrderBy(t => t.created_at).ToListAsync();
            return Ok(shippingSchedule);
        }

        // GET: api/ShippingSchedules/Mulero
        [Route("yard-mule-driver/{id:int}")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> GetShippingSchedulesByYardMuleDriver(int id)
        {
            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSchedule = shippingSchedule.Where(s => s.initial_yard_mule_driver_id == id);
            await shippingSchedule.OrderBy(t => t.created_at).ToListAsync();
            if (shippingSchedule.Count() == 0)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."));
            }

            return Ok(shippingSchedule);
        }

        // GET: api/ShippingSchedules/me
        [Route("~/api/me/shipping-schedules")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> GetShippingSchedulesByMe()
        {
            if (!Request.Headers.Contains("Authorization"))
            {
                return Json(new HandlerReturn(HttpStatusCode.Unauthorized, true, "SSE_ME_01", "Se espera autorización."));
            }

            int userId = int.Parse(Request.Headers.GetValues("Authorization").First());

            User user = await db.User.FindAsync(userId);
            if (user == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Unauthorized, true, "SSE_ME_02", "Error al validar al usuario."));
            }

            var endOfDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null && t.created_at >= DateTime.Today && t.created_at <= endOfDay)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);

            // Si es nivel Mulero (YardMuleDriver)
            if (user.auth == 8)
            {
                IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.user_id == user.id);
                YardMuleDriver yardMuleDriver = await YardMuleDriver.FirstOrDefaultAsync();

                if (yardMuleDriver == null)
                {
                    return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."));
                }

                shippingSchedule = shippingSchedule.Where(s => s.initial_yard_mule_driver_id == yardMuleDriver.id && s.in_parking_space_at == null);

                var ordenado = await shippingSchedule.OrderBy(v => v.shipping_priority != null ? v.shipping_priority : 4).ThenBy(v => v.shipping_priority).ToListAsync();

                return Ok(ordenado);
                //await shippingSchedule.OrderBy(t => t.shipping_priority).ToListAsync();

                //return Ok(shippingSchedule);
            }
            // Si es Nivel Tráfico (Traffic)
            if (user.auth == 7)
            {
                //await shippingSchedule.OrderBy(t => t.shipping_priority).ToListAsync();
                //return Ok(shippingSchedule);

                var ordenado = await shippingSchedule.OrderBy(v => v.shipping_priority != null ? v.shipping_priority : 4).ThenBy(v => v.shipping_priority).ToListAsync();

                return Ok(ordenado);
            }

            return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."));
        }

        // GET: api/ShippingSchedules/5
        [Route("{id:int}")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> GetShippingSchedule(int id)
        {
            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null).Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSchedule = shippingSchedule.Where(t => t.id == id);
            await shippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule.Count() == 0)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."));
            }

            return Ok(shippingSchedule);
        }

        // PUT: api/ShippingSchedules/5
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutShippingSchedule(int id, ShippingSchedule shippingSchedule)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."));
            }

            if (shippingSchedule == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."));
            }

            if (id != shippingSchedule.id)
            {
                //return BadRequest();
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "SSE_PSS_01", "El id del embarque no exíste o es nulo."));
            }

            //shippingScheduleCurrent.shipping_number = shippingSchedule.shipping_number.Trim();
            //shippingScheduleCurrent.trailer_number = shippingSchedule.trailer_number.Trim();
            //shippingScheduleCurrent.truck_number = shippingSchedule.truck_number.Trim();
            //shippingScheduleCurrent.license_plate = shippingSchedule.license_plate.Trim();
            //shippingScheduleCurrent.origin = shippingSchedule.origin.Trim();
            //shippingScheduleCurrent.destination = shippingSchedule.destination.Trim();
            //shippingScheduleCurrent.container = shippingSchedule.container.Trim();

            ShippingSchedule shippingScheduleCurrent = await db.ShippingSchedules.FindAsync(id);

            shippingScheduleCurrent.shipping_number = shippingSchedule.shipping_number.Trim();
            shippingScheduleCurrent.scheduled_date = shippingSchedule.scheduled_date;
            shippingScheduleCurrent.warehouse_id = shippingSchedule.warehouse_id;
            shippingScheduleCurrent.carrier_id = shippingSchedule.carrier_id;
            shippingScheduleCurrent.trailer_number = shippingSchedule.trailer_number.Trim();
            shippingScheduleCurrent.truck_number = shippingSchedule.truck_number.Trim();
            shippingScheduleCurrent.license_plate = shippingSchedule.license_plate.Trim();
            shippingScheduleCurrent.driver_id = shippingSchedule.driver_id;
            shippingScheduleCurrent.trailer_status_id = shippingSchedule.trailer_status_id;
            shippingScheduleCurrent.client_id = shippingSchedule.client_id;
            shippingScheduleCurrent.origin = shippingSchedule.origin.Trim();
            shippingScheduleCurrent.destination = shippingSchedule.destination.Trim();
            shippingScheduleCurrent.container = shippingSchedule.container.Trim();

            db.Entry(shippingScheduleCurrent).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    //return NotFound();
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

            //return StatusCode(HttpStatusCode.NoContent);
            //return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque fue actualizado con éxito."));
            return CreatedAtRoute("GetShippingSchedules", new { id = shippingSchedule.id }, shippingSchedule);
        }

        //[Route("{id:int}/assign")]
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PostShippingScheduleAssign(int id, ShippingSchedule receivedShippingSchedule)
        //{
        //    IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
        //        .Include(c => c.carrier)
        //        .Include(c => c.client)
        //        .Include(c => c.client.contact)
        //        .Include(c => c.client.contact.address)
        //        .Include(c => c.driver)
        //        .Include(c => c.driver.carrier)
        //        .Include(c => c.driver.user)
        //        .Include(c => c.driver.user.contact)
        //        .Include(c => c.driver.user.contact.address)
        //        .Include(c => c.driver.user.contact.email)
        //        .Include(c => c.initial_yard_mule_driver)
        //        .Include(c => c.shipping_status)
        //        .Include(c => c.trailer_status)
        //        .Include(c => c.warehouse)
        //        .Include(c => c.warehouse.address);
        //    ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
        //    ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
        //    if (shippingSchedule == null)
        //    {
        //        //return NotFound();
        //        return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
        //    }

        //    shippingSchedule.initial_yard_mule_driver_id = receivedShippingSchedule.initial_yard_mule_driver_id;
        //    shippingSchedule.initial_parking_space = receivedShippingSchedule.initial_parking_space;
        //    shippingSchedule.ramp = receivedShippingSchedule.ramp;
        //    shippingSchedule.final_yard_mule_driver_id = receivedShippingSchedule.final_yard_mule_driver_id;
        //    shippingSchedule.final_parking_space = receivedShippingSchedule.final_parking_space;
        //    shippingSchedule.shipping_priority = receivedShippingSchedule.shipping_priority;

        //    db.Entry(shippingSchedule).State = EntityState.Modified;

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ShippingScheduleExists(id))
        //        {
        //            //return NotFound();
        //            return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    //return StatusCode(HttpStatusCode.NoContent);
        //    return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "Asignación exitosa."));
        //}

        // POST: api/ShippingSchedules
        [Route("")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> PostShippingSchedule(ShippingSchedule shippingSchedule)
        {
            if (shippingSchedule == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."));
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.
            IQueryable<ShippingSchedule> ShippingCarrierCustomer = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingCarrierCustomer = ShippingCarrierCustomer.Where(t => t.shipping_number == shippingSchedule.shipping_number && t.carrier_id == shippingSchedule.carrier_id && t.client_id == shippingSchedule.client_id);
            await ShippingCarrierCustomer.FirstOrDefaultAsync();

            if (ShippingCarrierCustomer.Count() > 0)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "SSGE_02", "El embarque ya existe, favor de validar."));
            }
            // Termina Validar que la combinación de Embarque-Transportista-Cliente no exista en el sistema.
            shippingSchedule.shipping_status_id = ShippingStatus.ON_REQUEST;

            shippingSchedule.shipping_number = shippingSchedule.shipping_number.Trim();
            shippingSchedule.truck_number = shippingSchedule.truck_number.Trim();
            shippingSchedule.license_plate = shippingSchedule.license_plate.Trim();
            shippingSchedule.origin = shippingSchedule.origin.Trim();
            shippingSchedule.destination = shippingSchedule.destination.Trim();
            shippingSchedule.container = shippingSchedule.container.Trim();

            db.ShippingSchedules.Add(shippingSchedule);
            await db.SaveChangesAsync();

            return CreatedAtRoute("GetShippingSchedules", new { id = shippingSchedule.id }, shippingSchedule);
            //return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque fue agregado con éxito."));
        }

        // DELETE: api/ShippingSchedules/5
        [Route("{id:int}")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> DeleteShippingSchedule(int id)
        {
            ShippingSchedule shippingSchedule = await db.ShippingSchedules.FindAsync(id);
            if (shippingSchedule == null)
            {
                //return NotFound();
                return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
            }

            db.ShippingSchedules.Remove(shippingSchedule);
            await db.SaveChangesAsync();

            //return Ok(shippingSchedule);
            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque fue eliminado con éxito."));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShippingScheduleExists(int id)
        {
            return db.ShippingSchedules.Count(e => e.id == id) > 0;
        }


        [Route("{id:int}/move-from-parking-space")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> PostMoveFromParkingSpace(int id)
        {
            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSchedule = shippingSchedule.Where(t => t.id == id);
            ShippingSchedule ShippingSchedule = await shippingSchedule.FirstOrDefaultAsync();
            if (ShippingSchedule == null)
            {
                //return NotFound();
                return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));

            }

            ShippingSchedule.shipping_status_id = ShippingStatus.IN_RAMP;
            ShippingSchedule.moved_from_parking_space_at = DateTime.Now;
            db.Entry(ShippingSchedule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    //return NotFound();
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque fue movido desde cajón con éxito."));
            //return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("{id:int}/move-from-ramp")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> PostMoveFromRamp(int id)
        {
            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
            }

            // shippingSchedule.shipping_status_id = ShippingStatus.IN_PARKING_SPACE;
            shippingSchedule.moved_from_ramp_at = DateTime.Now;
            db.Entry(shippingSchedule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    //return NotFound();
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque fue movido desde rampa con éxito."));
        }

        [Route("{id:int}/leave-in-ramp")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> PostLeaveInRamp(int id)
        {
            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule == null)
            {
                //return NotFound();
                return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
            }

            shippingSchedule.shipping_status_id = ShippingStatus.IN_RAMP;
            shippingSchedule.in_ramp_at = DateTime.Now;
            db.Entry(shippingSchedule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    //return NotFound();
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque ha sido dejado en rampa con éxito."));
        }

        [Route("{id:int}/leave-in-parking-space")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> PostLeaveInParkingSpace(int id)
        {
            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();
            if (shippingSchedule == null)
            {
                //return NotFound();
                return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
            }

            shippingSchedule.shipping_status_id = ShippingStatus.FINISHED_IN_PARKING_SPACE;
            shippingSchedule.in_parking_space_at = DateTime.Now;
            db.Entry(shippingSchedule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    //return NotFound();
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque ha sido dejado en cajón con éxito."));
        }

        // Programación de embarques
        [Route("{id:int}/schedule")]
        [ResponseType(typeof(ShippingSchedule))]
        public async Task<IHttpActionResult> PostSchedule(int id, Scheduble scheduble)
        {
            if (scheduble == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."));
            }

            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();

            if (shippingSchedule == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
            }

            if (!ModelState.IsValid)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."));
            }

            // Validar si el cajon y rampa estan ocupados

            //// Validando Rampa
            var rampsBusy = await db.ShippingSchedules.Where(t => t.ramp == scheduble.ramp && t.moved_from_ramp_at == null && t.id != id && t.deleted_at == null).AnyAsync();

            if (rampsBusy)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_PS_01", "La rampa ya se encuentra ocupada."));
            }
            //// End Validando Rampa

            //// Validando Cajón
            var parkingSpaceBusy = await db.ShippingSchedules.Where(t => t.final_parking_space == scheduble.final_parking_space && t.moved_from_parking_space_at == null && t.id != id && t.deleted_at == null).AnyAsync();

            if (parkingSpaceBusy)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_PS_02", "El cajón final ya se encuentra ocupado."));
            }
            //// End Validando Cajón

            // End Validar si el cajon y rampa estan ocupados

            shippingSchedule.initial_yard_mule_driver_id = scheduble.initial_yard_mule_driver_id;
            shippingSchedule.initial_parking_space = scheduble.initial_parking_space;
            shippingSchedule.ramp = scheduble.ramp;
            shippingSchedule.final_yard_mule_driver_id = scheduble.final_yard_mule_driver_id;
            shippingSchedule.final_parking_space = scheduble.final_parking_space;
            shippingSchedule.shipping_priority = scheduble.shipping_priority;
            shippingSchedule.ramp_and_yard_mule_driver_assigned_at = DateTime.Now;

            db.Entry(shippingSchedule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
                return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque ha sido asignado con éxito."));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

        }

        // Programación de embarques
        [Route("{id:int}/assign")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostShippingScheduleAssign(int id, Scheduble scheduble)
        {
            if (scheduble == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."));
            }

            IQueryable<ShippingSchedule> ShippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            ShippingSchedule = ShippingSchedule.Where(t => t.id == id);
            ShippingSchedule shippingSchedule = await ShippingSchedule.FirstOrDefaultAsync();

            if (shippingSchedule == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
            }

            if (!ModelState.IsValid)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."));
            }

            // Validar si el cajon y rampa estan ocupados

            //// Validando Rampa
            var rampsBusy = await db.ShippingSchedules.Where(t => t.ramp == scheduble.ramp && t.moved_from_ramp_at == null && t.id != id && t.deleted_at == null).AnyAsync();

            if (rampsBusy)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_PS_01", "La rampa ya se encuentra ocupada."));
            }
            //// End Validando Rampa

            //// Validando Cajón
            var parkingSpaceBusy = await db.ShippingSchedules.Where(t => t.final_parking_space == scheduble.final_parking_space && t.moved_from_parking_space_at == null && t.id != id && t.deleted_at == null).AnyAsync();

            if (parkingSpaceBusy)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_PS_02", "El cajón final ya se encuentra ocupado."));
            }
            //// End Validando Cajón

            // End Validar si el cajon y rampa estan ocupados

            shippingSchedule.initial_yard_mule_driver_id = scheduble.initial_yard_mule_driver_id;
            shippingSchedule.initial_parking_space = scheduble.initial_parking_space;
            shippingSchedule.ramp = scheduble.ramp;
            shippingSchedule.final_yard_mule_driver_id = scheduble.final_yard_mule_driver_id;
            shippingSchedule.final_parking_space = scheduble.final_parking_space;
            shippingSchedule.shipping_priority = scheduble.shipping_priority;

            if (shippingSchedule.shipping_status_id != 5)
            {
                shippingSchedule.shipping_status_id = ShippingStatus.IN_PARKING_SPACE;
                shippingSchedule.ramp_and_yard_mule_driver_assigned_at = DateTime.Now;
            }

            db.Entry(shippingSchedule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
                return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El embarque ha sido asignado con éxito."));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShippingScheduleExists(id))
                {
                    return Json(new HandlerReturn(HttpStatusCode.NotFound, true, "SSGE_01", "No se encontró ningún embarque con el id solicitado."));
                }
                else
                {
                    throw;
                }
            }

        }

        // Obtener embarques por prioridad
        [Route("priority/{priority_name}")]
        public async Task<IHttpActionResult> GetShippingSchedulebyPriority(string priority_name)
        {
            if (priority_name == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_01", "No se han enviado ningún parametro."));
            }

            int priority_id = 0;

            switch (priority_name)
            {
                case "high":
                    priority_id = shipping_priority.alta;
                    break;
                case "medium":
                    priority_id = shipping_priority.media;
                    break;
                case "low":
                    priority_id = shipping_priority.baja;
                    break;
            }

            IQueryable<ShippingSchedule> shippingSchedule = db.ShippingSchedules.Where(t => t.deleted_at == null)
                .Include(c => c.carrier)
                .Include(c => c.client)
                .Include(c => c.client.contact)
                .Include(c => c.client.contact.address)
                .Include(c => c.driver)
                .Include(c => c.driver.carrier)
                .Include(c => c.driver.user)
                .Include(c => c.driver.user.contact)
                .Include(c => c.driver.user.contact.address)
                .Include(c => c.driver.user.contact.email)
                .Include(c => c.initial_yard_mule_driver)
                .Include(c => c.shipping_status)
                .Include(c => c.trailer_status)
                .Include(c => c.warehouse)
                .Include(c => c.warehouse.address);
            shippingSchedule = shippingSchedule.Where(t => t.shipping_priority == priority_id);
            await shippingSchedule.ToListAsync();

            if (shippingSchedule.Count() == 0)
            {
                HandlerReturn Output = null;

                switch (priority_id)
                {
                    case shipping_priority.alta:
                        Output = new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_GSSBP_01", "No se encontró ningún embarque con prioridad alta.");
                        break;

                    case shipping_priority.media:
                        Output = new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_GSSBP_02", "No se encontró ningún embarque con prioridad media.");
                        break;

                    case shipping_priority.baja:
                        Output = new HandlerReturn(HttpStatusCode.NoContent, true, "SSE_GSSBP_03", "No se encontró ningún embarque con prioridad baja.");
                        break;
                }

                return Json(Output);
            }

            return Ok(shippingSchedule);
        }
    }
}
