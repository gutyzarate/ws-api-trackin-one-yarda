﻿using onecore.security.utils;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("users")]
    public class UsersAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();
        private string encryptionKey = "0n3c0r33ncr1pt201804203i0qaw0e2e";

        // GET: UsersAjax
        [Route]
        public async Task<ActionResult> Index()
        {
            List<UserRoles> UserRoles = db.UserRoles.ToList();
            ViewBag.userRoles = UserRoles;
            return View();
        }

        // GET: UsersAjax/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.User.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: UsersAjax/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.contact_id = new SelectList(db.Contact, "id", "id");
            return View();
        }

        // POST: UsersAjax/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,name,first_last_name,second_last_name,username,password,auth,status,contact_id,company_id")] User user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.User.Add(user);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.contact_id = new SelectList(db.Contact, "id", "id", user.contact_id);
        //    return View(user);
        //}

        // GET: UsersAjax/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.User.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.contact_id = new SelectList(db.Contact, "id", "id", user.contact_id);
            return View(user);
        }

        // POST: UsersAjax/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,name,first_last_name,second_last_name,username,password,auth,status,contact_id,company_id")] User user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(user).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.contact_id = new SelectList(db.Contact, "id", "id", user.contact_id);
        //    return View(user);
        //}

        // GET: UsersAjax/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.User.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: UsersAjax/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    User user = await db.User.FindAsync(id);
        //    db.User.Remove(user);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener usuarios
        [Route("~/ajax/users")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxUsers()
        {
            IQueryable<User> Users = db.User.Where(t => t.status == true).Include(t => t.contact);
            List<User> users = await Users.ToListAsync();

            if (users == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        // Agregar usuario
        [Route("~/ajax/users")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxUser(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["name"]) || string.IsNullOrEmpty(formData["first_last_name"]) || string.IsNullOrEmpty(formData["second_last_name"]) || string.IsNullOrEmpty(formData["email"]))
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el perfil no existe
            IQueryable<User> user = db.User;
            string email = formData["email"];
            user = user.Where(t => t.contact.email.email_address == email);

            User ExistUser = await user.FirstOrDefaultAsync();

            if (ExistUser != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAU_01", "El usuario ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el perfil no existe

            Email Email = new Email();
            Email.email_address = formData["email"];

            db.Email.Add(Email);
            await db.SaveChangesAsync();

            Contact Contact = new Contact();
            Contact.email_id = Email.id;
            db.Contact.Add(Contact);
            await db.SaveChangesAsync();


            User User = new User();
            User.name = formData["name"];
            User.first_last_name = formData["first_last_name"];
            User.second_last_name = formData["second_last_name"];
            User.contact_id = Contact.id;
            User.status = true;

            // Agregamos datos de sesión
            if (!string.IsNullOrEmpty(formData["username"]))
            {
                User.username = formData["username"];
            }
                
            if (!string.IsNullOrEmpty(formData["password"]))
            {
                string encryptedPassword = EncryptService.Encrypt(formData["password"], encryptionKey);
                User.password = encryptedPassword;
            }
            if (!string.IsNullOrEmpty(formData["auth"]))
            {
                User.auth = int.Parse(formData["auth"]);
            }

            db.User.Add(User);
            await db.SaveChangesAsync();

            return Json(User, JsonRequestBehavior.AllowGet);
        }

        // Editar usuario
        [Route("~/ajax/users")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxUser(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["id"]) || string.IsNullOrEmpty(formData["name"]) || string.IsNullOrEmpty(formData["first_last_name"]) || string.IsNullOrEmpty(formData["second_last_name"]) || string.IsNullOrEmpty(formData["email"]))
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el perfil no existe
            IQueryable<User> user = db.User;
            int id = int.Parse(formData["id"]);
            string email = formData["email"];
            user = user.Where(t => t.contact.email.email_address == email && t.id != id);

            User ExistUser = await user.FirstOrDefaultAsync();

            if (ExistUser != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAU_01", "El usuario ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el perfil no existe

            User User = await db.User.FindAsync(id);
            if(User.contact == null)
            {
                Contact contact = new Contact();

                Email Email = new Email();
                Email.email_address = formData["email"];
                db.Email.Add(Email);

                contact.email_id = Email.id;
                db.Contact.Add(contact);

                User.contact_id = contact.id;
            }
            else
            {
                Contact Contact = User.contact;
                if(Contact.email != null)
                {
                    Email Email = Contact.email;
                    Email.email_address = formData["email"];
                    db.Entry(Email).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                else
                {
                    Email Email = new Email();
                    Email.email_address = formData["email"];
                    db.Email.Add(Email);
                    Contact.email_id = Email.id;
                }
            }

            User.name = formData["name"];
            User.first_last_name = formData["first_last_name"];
            User.second_last_name = formData["second_last_name"];

            // Agregamos datos de sesión
            if (!string.IsNullOrEmpty(formData["username"]))
            {
                User.username = formData["username"];
            }

            if (!string.IsNullOrEmpty(formData["password"]))
            {
                string encryptedPassword = EncryptService.Encrypt(formData["password"], encryptionKey);
                User.password = encryptedPassword;
            }
            if (!string.IsNullOrEmpty(formData["auth"]))
            {
                User.auth = int.Parse(formData["auth"]);
            }

            db.Entry(User).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(User, JsonRequestBehavior.AllowGet);
        }

        // Borrar Usuario
        [Route("~/ajax/users")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxUser(int id)
        {
            User user = await db.User.FindAsync(id);

            db.User.Remove(user);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El usuario ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
