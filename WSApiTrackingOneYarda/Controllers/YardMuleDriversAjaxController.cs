﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("yard-mule-drivers")]
    public class YardMuleDriversAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: YardMuleDriversAjax
        [Route]
        public async Task<ActionResult> Index()
        {
            return View();
        }

        // GET: YardMuleDriversAjax/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            YardMuleDriver = YardMuleDriver.Where(t => t.id == id);
            YardMuleDriver yardMuleDriver = await YardMuleDriver.FirstOrDefaultAsync();
            if (yardMuleDriver == null)
            {
                return HttpNotFound();
            }
            return View(yardMuleDriver);
        }

        // GET: YardMuleDriversAjax/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.incidence_id = new SelectList(db.Incidence, "id", "table");
            ViewBag.user_id = new SelectList(db.User, "id", "name");
            return View();
        }

        // POST: YardMuleDriversAjax/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[Route("create")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,first_name,last_name,incidence_id,social_security_number,driver_license_number,user_id,created_at,updated_at,deleted_at")] YardMuleDriver yardMuleDriver)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.YardMuleDriver.Add(yardMuleDriver);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.incidence_id = new SelectList(db.Incidence, "id", "table", yardMuleDriver.incidence_id);
        //    ViewBag.user_id = new SelectList(db.User, "id", "name", yardMuleDriver.user_id);
        //    return View(yardMuleDriver);
        //}

        // GET: YardMuleDriversAjax/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            YardMuleDriver = YardMuleDriver.Where(t => t.id == id);
            YardMuleDriver yardMuleDriver = await YardMuleDriver.FirstOrDefaultAsync();
            if (yardMuleDriver == null)
            {
                return HttpNotFound();
            }
            ViewBag.incidence_id = new SelectList(db.Incidence, "id", "table", yardMuleDriver.incidence_id);
            ViewBag.user_id = new SelectList(db.User, "id", "name", yardMuleDriver.user_id);
            return View(yardMuleDriver);
        }

        // POST: YardMuleDriversAjax/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("edit/{id}")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,first_name,last_name,incidence_id,social_security_number,driver_license_number,user_id,created_at,updated_at,deleted_at")] YardMuleDriver yardMuleDriver)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(yardMuleDriver).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.incidence_id = new SelectList(db.Incidence, "id", "table", yardMuleDriver.incidence_id);
        //    ViewBag.user_id = new SelectList(db.User, "id", "name", yardMuleDriver.user_id);
        //    return View(yardMuleDriver);
        //}

        // GET: YardMuleDriversAjax/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            YardMuleDriver = YardMuleDriver.Where(t => t.id == id);
            YardMuleDriver yardMuleDriver = await YardMuleDriver.FirstOrDefaultAsync();
            if (yardMuleDriver == null)
            {
                return HttpNotFound();
            }
            return View(yardMuleDriver);
        }

        // POST: YardMuleDriversAjax/Delete/5
        //[Route("delete/{id}")]
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    YardMuleDriver yardMuleDriver = await db.YardMuleDriver.FindAsync(id);
        //    db.YardMuleDriver.Remove(yardMuleDriver);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        // AJAX REQUEST //////////////////////////////////////////////////////////////////

        // Obtener Mulero
        [Route("~/ajax/yard-mule-drivers")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxYardMuleDrivers()
        {
            IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            YardMuleDriver = YardMuleDriver.Include(y => y.incidence).Include(y => y.user);

            List<YardMuleDriver> yardMuleDrivers = await YardMuleDriver.ToListAsync();

            if (yardMuleDrivers.Count() == 0)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }

            return Json(yardMuleDrivers, JsonRequestBehavior.AllowGet);
        }

        // Agregar Mulero
        [Route("~/ajax/yard-mule-drivers")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxYardMuleDriver(YardMuleDriver yardMuleDriver)
        {
            if (yardMuleDriver.first_name == null || yardMuleDriver.last_name == null || yardMuleDriver.social_security_number == null || yardMuleDriver.driver_license_number == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            YardMuleDriver = YardMuleDriver.Where(t => t.social_security_number.ToUpper() == yardMuleDriver.social_security_number.ToUpper() || t.driver_license_number.ToUpper() == yardMuleDriver.driver_license_number.ToUpper());

            var ExistYardMuleDriver = await YardMuleDriver.AnyAsync();

            if (ExistYardMuleDriver)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "YMD_PAYMD_01", "El número del IMSS y/o licencia ya se usa para otro mulero, favor de validar."), JsonRequestBehavior.AllowGet);
            }

            // Creamos al usuario en la tabla adm.tblUser
            User UserNew = new User();
            UserNew.name = yardMuleDriver.first_name;
            string[] apellidos = yardMuleDriver.last_name.Split(' ');
            UserNew.first_last_name = apellidos[0];
            if (apellidos.Count() > 1)
            {
                string concatApellidos = null;

                foreach (var apellido in apellidos)
                {
                    if (apellido == apellidos.First())
                    {
                        continue;
                    }

                    concatApellidos = concatApellidos + apellido + " ";
                }
                UserNew.second_last_name = concatApellidos.Trim();
            }

            UserNew.auth = UserRoles.YARD_MULE_DRIVER;
            UserNew.status = true;
            db.User.Add(UserNew);
            await db.SaveChangesAsync();

            yardMuleDriver.user_id = UserNew.id;

            db.YardMuleDriver.Add(yardMuleDriver);
            await db.SaveChangesAsync();

            return Json(yardMuleDriver, JsonRequestBehavior.AllowGet);
        }

        // Editar mulero
        [Route("~/ajax/yard-mule-drivers")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxYardMuleDriver([Bind(Include = "id,first_name,last_name,incidence_id,social_security_number,driver_license_number,user_id")] YardMuleDriver yardMuleDriver)
        {
            if (yardMuleDriver.first_name == null || yardMuleDriver.last_name == null || yardMuleDriver.social_security_number == null || yardMuleDriver.driver_license_number == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            YardMuleDriver = YardMuleDriver.Where(t => (t.social_security_number.ToUpper() == yardMuleDriver.social_security_number.ToUpper() || t.driver_license_number.ToUpper() == yardMuleDriver.driver_license_number.ToUpper()) && t.id != yardMuleDriver.id);

            var ExistYardMuleDriver = await YardMuleDriver.AnyAsync();

            if (ExistYardMuleDriver)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "YMD_PAYMD_01", "El número del IMSS y/o licencia ya se usa para otro mulero, favor de validar."), JsonRequestBehavior.AllowGet);
            }

            // Obtenemos al usuario
            var user_id = await db.YardMuleDriver.Where(t => t.id == yardMuleDriver.id).Select(t => t.user_id).FirstAsync();
            User user = await db.User.FindAsync(user_id);
            user.name = yardMuleDriver.first_name;
            string[] apellidos = yardMuleDriver.last_name.Split(' ');
            user.first_last_name = apellidos[0];
            if (apellidos.Count() > 1)
            {
                string concatApellidos = null;

                foreach (var apellido in apellidos)
                {
                    if (apellido == apellidos.First())
                    {
                        continue;
                    }

                    concatApellidos = concatApellidos + apellido + " ";
                }
                user.second_last_name = concatApellidos.Trim();
            }

            db.Entry(user).State = EntityState.Modified;
            await db.SaveChangesAsync();

            yardMuleDriver.user_id = user.id;

            db.Entry(yardMuleDriver).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(yardMuleDriver, JsonRequestBehavior.AllowGet);
        }

        // Borrar Mulero
        [Route("~/ajax/yard-mule-drivers")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxYardMuleDriver(int id)
        {
            YardMuleDriver yardMuleDriver = await db.YardMuleDriver.FindAsync(id);

            User user = yardMuleDriver.user;
            user.status = false;
            db.Entry(user).State = EntityState.Modified;
            await db.SaveChangesAsync();

            Incidence incidence = yardMuleDriver.incidence;
            if (incidence != null)
            {
                db.Incidence.Remove(incidence);
                await db.SaveChangesAsync();
            }

            db.YardMuleDriver.Remove(yardMuleDriver);
            await db.SaveChangesAsync();

            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El mulero ha sido eliminado con éxito."));
        }

        // GET: ShippingSchedulesAjax
        [Route("login-report")]
        [HttpGet]
        public async Task<ActionResult> LoginReport()
        {
            return View();
        }

        [Route("~/ajax/yard-mule-drivers/login-report")]
        [HttpGet]
        public async Task<ActionResult> GetLoginReportYardMuleDrivers()
        {
            IQueryable<ActivityLog> ActivityLog = db.ActivityLog.Where(t => t.deleted_at == null);

            ActivityLog = ActivityLog.Where(t => t.user.auth == UserRoles.YARD_MULE_DRIVER);
            //List<ActivityLog> ActivityLog = db.ActivityLog.Where(s => s.user.auth == UserRoles.YARD_MULE_DRIVER).ToList();

            //IQueryable<YardMuleDriver> YardMuleDriver = db.YardMuleDriver.Where(t => t.deleted_at == null);
            ActivityLog = ActivityLog.Include(y => y.user);

            List<ActivityLog> activityLog = await ActivityLog.OrderByDescending(t => t.created_at).ToListAsync();

            if (activityLog.Count() == 0)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }

            return Json(activityLog, JsonRequestBehavior.AllowGet);
        }


        // END AJAX REQUEST //////////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
