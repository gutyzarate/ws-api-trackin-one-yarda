﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("trailer-statuses")]
    public class TrailerStatusAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: TrailerStatusAjax
        [Route]
        public async Task<ActionResult> Index()
        {
            ViewBag.model = "trailer-statuses";
            ViewBag.modelLabel = "Estatus de Remolque";
            return View(await db.TrailerStatus.ToListAsync());
        }

        // GET: TrailerStatusAjax/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);
            if (trailerStatus == null)
            {
                return HttpNotFound();
            }
            return View(trailerStatus);
        }

        // GET: TrailerStatusAjax/Create
        [Route("create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrailerStatusAjax/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,name,label")] TrailerStatus trailerStatus)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.TrailerStatus.Add(trailerStatus);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(trailerStatus);
        //}

        // GET: TrailerStatusAjax/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);
            if (trailerStatus == null)
            {
                return HttpNotFound();
            }
            return View(trailerStatus);
        }

        // POST: TrailerStatusAjax/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,name,label")] TrailerStatus trailerStatus)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(trailerStatus).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(trailerStatus);
        //}

        // GET: TrailerStatusAjax/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);
            if (trailerStatus == null)
            {
                return HttpNotFound();
            }
            return View(trailerStatus);
        }

        // POST: TrailerStatusAjax/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);
        //    db.TrailerStatus.Remove(trailerStatus);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


        // AJAX REQUEST //////////////////////////////////////////////////////////////

        // Obtener Estados de remolque
        [Route("~/ajax/trailer-statuses")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxTrailerStatuses()
        {
            IQueryable<TrailerStatus> trailerStatus = db.TrailerStatus;
            List<TrailerStatus> TrailerStatuses = await trailerStatus.ToListAsync();

            if (TrailerStatuses == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(TrailerStatuses, JsonRequestBehavior.AllowGet);
        }

        // Obtener Estados de remolque
        [Route("~/ajax/trailer-statuses/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxTrailerStatusesById(int id)
        {
            TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);

            if (trailerStatus == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }

            return Json(trailerStatus, JsonRequestBehavior.AllowGet);
        }

        // Agregar estado de remolque
        [Route("~/ajax/trailer-statuses")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxTrailerStatus(TrailerStatus trailer_status)
        {
            if (trailer_status.label == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el estado de embarque no existe
            IQueryable<TrailerStatus> TrailerStatus = db.TrailerStatus;
            TrailerStatus = TrailerStatus.Where(t => t.label == trailer_status.label);

            TrailerStatus ExistTrailerStatus = await TrailerStatus.FirstOrDefaultAsync();

            if (ExistTrailerStatus != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PATS_01", "El estado de remolque ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el estado de embarque no existe

            TrailerStatus trailerStatus = new TrailerStatus();
            trailerStatus.label = trailer_status.label;
            trailerStatus.name = trailer_status.label.Replace(' ', '_').ToLower();

            db.TrailerStatus.Add(trailerStatus);
            await db.SaveChangesAsync();

            return Json(trailerStatus, JsonRequestBehavior.AllowGet);
        }

        // Editar estado de remolque
        [Route("~/ajax/trailer-statuses")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxTrailerStatus(TrailerStatus trailer_status)
        {
            if (trailer_status.label == null || trailer_status.id == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el estado de embarque no existe
            IQueryable<TrailerStatus> TrailerStatus = db.TrailerStatus;
            string label = trailer_status.label;
            int id = trailer_status.id;
            TrailerStatus = TrailerStatus.Where(t => t.label == label && t.id != id);

            TrailerStatus ExistTrailerStatus = await TrailerStatus.FirstOrDefaultAsync();

            if (ExistTrailerStatus != null)
            {
                Response.StatusCode = (int)HttpStatusCode.Found;
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PATS_01", "El estado de remolque ya existe, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el estado de embarque no existe

            TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);
            trailerStatus.label = label;
            trailerStatus.name = label.Replace(' ', '_').ToLower();

            db.Entry(trailerStatus).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(trailerStatus, JsonRequestBehavior.AllowGet);
        }

        // Borrar estado de remolque
        [Route("~/ajax/trailer-statuses")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxTrailerStatus(int id)
        {
            TrailerStatus trailerStatus = await db.TrailerStatus.FindAsync(id);

            db.TrailerStatus.Remove(trailerStatus);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El estado de remolque ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
