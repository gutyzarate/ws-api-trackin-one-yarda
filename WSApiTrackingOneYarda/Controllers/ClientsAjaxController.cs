﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Extensions;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("clients")]
    public class ClientsAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: Clients
        [Route]
        public async Task<ActionResult> Index()
        {
            @ViewBag.providers = await db.Provider
                .Where(t => t.deleted == false)
                .Include(c => c.contact)
                .ToListAsync();

            return View();
        }

        // GET: Clients/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = await db.Client.FindAsync(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: Clients/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.contact_id = new SelectList(db.Contact, "id", "id");
            return View();
        }

        // POST: Clients/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "id,name,first_lastname,second_lastname,contact_id,active")] Client client)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Client.Add(client);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.contact_id = new SelectList(db.Contact, "id", "id", client.contact_id);
        //    return View(client);
        //}

        // GET: Clients/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = await db.Client.FindAsync(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.contact_id = new SelectList(db.Contact, "id", "id", client.contact_id);
            return View(client);
        }

        // POST: Clients/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,name,first_lastname,second_lastname,contact_id,active")] Client client)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(client).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.contact_id = new SelectList(db.Contact, "id", "id", client.contact_id);
        //    return View(client);
        //}

        // GET: Clients/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = await db.Client.FindAsync(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: Clients/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Client client = await db.Client.FindAsync(id);
        //    db.Client.Remove(client);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener clientes
        [Route("~/ajax/clients/{mode?}")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxClient(string mode)
        {

            IQueryable<Client> Client = db.Client.Include(c => c.contact).OrderBy(t => t.id);
            if (mode != null)
            {
                if (mode == "activate")
                {
                    Client = Client.Where(t => t.active == true);
                }
            }
            
            List<Client> client = new List<Client>();
            
            if (Session["Login"] != null)
            {
                var currentUser = Session["Login"] as User;

                if (currentUser.auth == UserRoles.PROVIDER)
                {
                    var currentProvider = await db.Provider.Where(t => t.user_id == currentUser.id).FirstOrDefaultAsync();
                    if (currentProvider == null)
                    {
                        client = new List<Client>();
                    }
                    else
                    {
                        Client.Where(t => t.provider_id == currentProvider.id);
                        client = await Client.ToListAsync();
                    }
                }
                else
                {
                    client = await Client.ToListAsync();
                }
            }
            else
            {
                client = await Client.ToListAsync();
            }

            
            if (client == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(client, JsonRequestBehavior.AllowGet);
        }

        // Agregar cliente
        [Route("~/ajax/clients")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxClient(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["name"]) || 
                string.IsNullOrEmpty(formData["first_lastname"]) || 
                string.IsNullOrEmpty(formData["second_lastname"]) || 
                string.IsNullOrEmpty(formData["rfc"]) || 
                string.IsNullOrEmpty(formData["zipcode"]) || 
                string.IsNullOrEmpty(formData["street"]) || 
                string.IsNullOrEmpty(formData["ext_number"]) || 
                //string.IsNullOrEmpty(formData["int_number"]) || 
                string.IsNullOrEmpty(formData["neighborhood"]) || 
                string.IsNullOrEmpty(formData["provider_id"]) || 
                //string.IsNullOrEmpty(formData["latitud"]) && 
                //string.IsNullOrEmpty(formData["longitude"]) && 
                string.IsNullOrEmpty(formData["state"]) || 
                string.IsNullOrEmpty(formData["country"]) || 
                string.IsNullOrEmpty(formData["city"])
                )
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el cliente no existe
            IQueryable<Client> client = db.Client.Where(t => t.active == true);

            string rfc = formData["rfc"].ToUpper();
            client = client.Where(t => t.rfc.ToUpper() == rfc);

            Client ExistClient = await client.FirstOrDefaultAsync();

            if (ExistClient != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAC_01", "El RFC ya se usa en otro cliente, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el cliente no existe

            // Creamos la dirección
            Address Address = new Address();
            Contact Contact = new Contact();
            // Creamos el cliente
            Client Client = new Client();

            foreach (var formKey in formData.AllKeys)
            {
                var value = formData[formKey];
                switch (formKey)
                {
                    // Para Cliente
                    case "name":
                        Client.name = value;
                        break;
                    case "first_lastname":
                        Client.first_lastname = value;
                        break;
                    case "second_lastname":
                        Client.second_lastname = value;
                        break;
                    case "rfc":
                        Client.rfc = value.ToUpper();
                        break;
                    case "provider_id":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Client.provider_id = int.Parse(value);
                        }
                        break;
                    // Para Address
                    case "zipcode":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.zipcode = int.Parse(value);
                        }
                        break;
                    case "street":
                        Address.street = value;
                        break;
                    case "ext_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.ext_number = int.Parse(value);
                        }
                        break;
                    case "int_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.int_number = int.Parse(value);
                        }
                        break;
                    case "neighborhood":
                        Address.neighborhood = value;
                        break;
                    case "latitud":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.latitud = decimal.Parse(value);
                        }
                        break;
                    case "longitude":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.longitude = decimal.Parse(value);
                        }
                        break;
                    case "state":
                        Address.state = value;
                        break;
                    case "country":
                        Address.country = value;
                        break;
                    case "city":
                        Address.city = value;
                        break;
                }
            }

            // Se crea el registor en tblAddress
            db.Address.Add(Address);
            await db.SaveChangesAsync();

            // Se crea el registro en tblContact
            Contact.address_id = Address.id;
            db.Contact.Add(Contact);
            await db.SaveChangesAsync();

            Client.contact_id = Contact.id;
            Client.active = true;

            db.Client.Add(Client);
            await db.SaveChangesAsync();

            return Json(Client, JsonRequestBehavior.AllowGet);
        }

        // Editar cliente
        [Route("~/ajax/clients")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxClient(FormCollection formData)
        {
            if (string.IsNullOrEmpty(formData["id"]) ||
                string.IsNullOrEmpty(formData["name"]) ||
                string.IsNullOrEmpty(formData["first_lastname"]) ||
                string.IsNullOrEmpty(formData["second_lastname"]) ||
                string.IsNullOrEmpty(formData["rfc"]) ||
                string.IsNullOrEmpty(formData["zipcode"]) ||
                string.IsNullOrEmpty(formData["street"]) ||
                string.IsNullOrEmpty(formData["ext_number"]) ||
                //string.IsNullOrEmpty(formData["int_number"]) || 
                string.IsNullOrEmpty(formData["neighborhood"]) ||
                string.IsNullOrEmpty(formData["provider_id"]) ||
                //string.IsNullOrEmpty(formData["latitud"]) && 
                //string.IsNullOrEmpty(formData["longitude"]) && 
                string.IsNullOrEmpty(formData["state"]) ||
                string.IsNullOrEmpty(formData["country"]) ||
                string.IsNullOrEmpty(formData["city"])
                )
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el transportista no existe
            IQueryable<Client> clientToValidate = db.Client.Where(t => t.active == true);
            string rfc = formData["rfc"].ToUpper();
            int id = int.Parse(formData["id"]);
            clientToValidate = clientToValidate.Where(t => t.rfc.ToUpper() == rfc && t.id != id);

            Client ExistClient = await clientToValidate.FirstOrDefaultAsync();

            if (ExistClient != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PAC_01", "El RFC ya se usa en otro cliente, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el transportista no existe

            // Obtenemos las relaciones correspondientes a editar

            // Obtenemos el Transportista a editar
            Client Client = await db.Client.FindAsync(id);

            // Obtenemos la dirección relacionada
            Address Address = Client.contact.address;

            foreach (var formKey in formData.AllKeys)
            {
                var value = formData[formKey];
                switch (formKey)
                {
                    // Para Cliente
                    case "name":
                        Client.name = value;
                        break;
                    case "first_lastname":
                        Client.first_lastname = value;
                        break;
                    case "second_lastname":
                        Client.second_lastname = value;
                        break;
                    case "rfc":
                        Client.rfc = value.ToUpper();
                        break;
                    case "provider_id":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Client.provider_id = int.Parse(value);
                        }
                        break;
                    // Para Address
                    case "zipcode":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.zipcode = int.Parse(value);
                        }
                        break;
                    case "street":
                        Address.street = value;
                        break;
                    case "ext_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.ext_number = int.Parse(value);
                        }
                        break;
                    case "int_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.int_number = int.Parse(value);
                        }
                        break;
                    case "neighborhood":
                        Address.neighborhood = value;
                        break;
                    case "latitud":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.latitud = decimal.Parse(value);
                        }
                        break;
                    case "longitude":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.longitude = decimal.Parse(value);
                        }
                        break;
                    case "state":
                        Address.state = value;
                        break;
                    case "country":
                        Address.country = value;
                        break;
                    case "city":
                        Address.city = value;
                        break;
                }
            }

            db.Entry(Client).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Entry(Address).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Json(Client, JsonRequestBehavior.AllowGet);
        }

        // Borrar Cliente
        [Route("~/ajax/clients")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxClient(int id)
        {
            Client client = await db.Client.FindAsync(id);

            db.Client.Remove(client);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El cliente ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // DATATABLE ///////////////////////////////////////
        [Route("~/ajax/clients/datatable")]
        [HttpPost]
        public JsonResult CustomServerSideSearchAction(DataTableAjaxPostModel model)
        {
            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = YourCustomSearchFunc(model, out filteredResultsCount, out totalResultsCount);

            var result = new List<Client>(res.Count);
            foreach (var m in res)
            {
                // simple remapping adding extra info to found dataset
                result.Add(new Client
                {
                    id = m.id,
                    rfc = m.rfc,
                    name = m.name,
                    first_lastname = m.first_lastname,
                    second_lastname = m.second_lastname,
                    contact_id = m.contact_id,
                    provider_id = m.provider_id,
                    active = m.active,
                    contact = m.contact,
                    provider = m.provider,
                });
            };

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        private IList<Client> YourCustomSearchFunc(DataTableAjaxPostModel model, out int filteredResultsCount_, out int totalResultsCount_)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;

            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            // search the dbase taking into consideration table sorting and paging
            var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount_, out totalResultsCount_);
            if (result == null)
            {
                // empty collection...
                return new List<Client>();
            }
            return result;
        }

        private List<Client> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            // the example datatable used is not supporting multi column ordering
            // so we only need get the column order from the first column passed to us.        
            var whereClause = BuildDynamicWhereClause(db, searchBy);

            //if (String.IsNullOrEmpty(searchBy))
            //{
            //    // if we have an empty search then just order the results by Id ascending
            //    sortBy = "id";
            //    sortDir = true;
            //}
            sortBy = "id";
            sortDir = true;

            var result = db.Client
                           //.AsExpandable()
                           .Where(whereClause)
                           //.Select(m => new Client
                           //{
                           //    id = m.id,
                           //    rfc = m.rfc,
                           //    name = m.name,
                           //    first_lastname = m.first_lastname,
                           //    second_lastname = m.second_lastname,
                           //    contact_id = m.contact_id,
                           //    provider_id = m.provider_id,
                           //    active = m.active,
                           //    //contact = m.contact,
                           //    //provider = m.provider

                           //})
                           .OrderBy(sortBy, sortDir) // have to give a default order when skipping .. so use the PK
                           .Skip(skip)
                           .Take(take)
                           .ToList();

            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            //filteredResultsCount = db.Client.AsExpandable().Where(whereClause).Count();
            filteredResultsCount = db.Client.Where(whereClause).Count();
            totalResultsCount = db.Client.Count();

            return result;
        }

        private Expression<Func<Client, bool>> BuildDynamicWhereClause(LogisticsTrackingDBContext entities, string searchValue)
        {
            // simple method to dynamically plugin a where clause
            var predicate = PredicateBuilder.New<Client>(true); // true -where(true) return all
            if (String.IsNullOrWhiteSpace(searchValue) == false)
            {
                // as we only have 2 cols allow the user type in name 'firstname lastname' then use the list to search the first and last name of dbase
                var searchTerms = searchValue.Split(' ').ToList().ConvertAll(x => x.ToLower());

                predicate = predicate.Or(s => searchTerms.Any(srch => s.rfc.ToLower().Contains(srch)));
                predicate = predicate.Or(s => searchTerms.Any(srch => s.first_lastname.ToLower().Contains(srch)));
                predicate = predicate.Or(s => searchTerms.Any(srch => s.second_lastname.ToLower().Contains(srch)));
            }
            return predicate;
        }
        // END DATATABLE ///////////////////////////////////
    }
}
