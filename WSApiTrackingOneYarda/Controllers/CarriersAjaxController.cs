﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("carriers")]
    public class CarriersAjaxController : AuthenticatedController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();

        // GET: Carriers
        [Route]
        public async Task<ActionResult> Index()
        {
            IQueryable<Carrier> carrier = db.Carrier.Where(t => t.deleted_at == null).Include(c => c.user);
            return View(await carrier.ToListAsync());
        }

        // GET: Carriers/Details/5
        [Route("details/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<Carrier> Carrier = db.Carrier.Where(t => t.deleted_at == null).Include(c => c.user);
            Carrier = Carrier.Where(t => t.id == id);
            Carrier carrier = await Carrier.FirstOrDefaultAsync();
            if (carrier == null)
            {
                return HttpNotFound();
            }
            return View(carrier);
        }

        // GET: Carriers/Create
        [Route("create")]
        public ActionResult Create()
        {
            ViewBag.user_id = new SelectList(db.User, "id", "name");
            return View();
        }

        // POST: Carriers/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[Route("create")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create(Carrier carrier)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Carrier.Add(carrier);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.user_id = new SelectList(db.User, "id", "name", carrier.user_id);
        //    return View(carrier);
        //}

        // GET: Carriers/Edit/5
        [Route("edit/{id}")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<Carrier> Carrier = db.Carrier.Where(t => t.deleted_at == null).Include(c => c.user);
            Carrier = Carrier.Where(t => t.id == id);
            Carrier carrier = await Carrier.FirstOrDefaultAsync();
            if (carrier == null)
            {
                return HttpNotFound();
            }
            ViewBag.user_id = new SelectList(db.User, "id", "name", carrier.user_id);
            return View(carrier);
        }

        // POST: Carriers/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[Route("edit/{id}")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "id,name,rfc,address,user_id,created_at,updated_at,deleted_at")] Carrier carrier)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(carrier).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.user_id = new SelectList(db.User, "id", "name", carrier.user_id);
        //    return View(carrier);
        //}

        // GET: Carriers/Delete/5
        [Route("delete/{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IQueryable<Carrier> Carrier = db.Carrier.Where(t => t.deleted_at == null).Include(c => c.user);
            Carrier = Carrier.Where(t => t.id == id);
            Carrier carrier = await Carrier.FirstOrDefaultAsync();
            if (carrier == null)
            {
                return HttpNotFound();
            }
            return View(carrier);
        }

        //// POST: Carriers/Delete/5
        //[Route("delete/{id}")]
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Carrier carrier = await db.Carrier.FindAsync(id);
        //    db.Carrier.Remove(carrier);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        // AJAX REQUEST //////////////////////////////////////////////////////////////
        // Obtener Transportistas
        [Route("~/ajax/carriers")]
        [HttpGet]
        public async Task<ActionResult> GetAjaxCarrier()
        {
            IQueryable<Carrier> Carrier = db.Carrier.Where(t => t.deleted_at == null).Include(c => c.user);
            List<Carrier> carrier = await Carrier.ToListAsync();

            if (carrier == null)
            {
                return Json(new HandlerReturn(HttpStatusCode.NoContent, true, "GE_02", "No se encontraron resultados."), JsonRequestBehavior.AllowGet);
            }
            return Json(carrier, JsonRequestBehavior.AllowGet);
        }

        // Agregar transportista
        [Route("~/ajax/carriers")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PostAjaxCarrier(FormCollection formNewCarrier)
        {
            if (string.IsNullOrEmpty(formNewCarrier["name"]))
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el transportista no existe
            IQueryable<Carrier> carrier = db.Carrier.Where(t => t.deleted_at == null);

            if (!string.IsNullOrEmpty(formNewCarrier["rfc"]))
            {
                string rfc = formNewCarrier["rfc"].ToUpper();
                string name = formNewCarrier["name"];
                carrier = carrier.Where(t => t.name == name || t.rfc.ToUpper() == rfc);
            }
            else
            {
                string name = formNewCarrier["name"];
                carrier = carrier.Where(t => t.name == name);
            }

            Carrier ExistCarrier = await carrier.FirstOrDefaultAsync();

            if (ExistCarrier != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PA_01", "El RFC ya se usa en otro transportista, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el transportista no existe

            // Creamos la dirección
            Address AddressNew = new Address();
            Email EmailNew = new Email();
            Contact ContactNew = new Contact();
            User UserNew = new User();

            foreach (var formKey in formNewCarrier.AllKeys)
            {
                var value = formNewCarrier[formKey];
                switch (formKey)
                {
                    // Para Address
                    case "zipcode":
                        if (!string.IsNullOrEmpty(value))
                        {
                            AddressNew.zipcode = int.Parse(value);
                        }
                        break;
                    case "street":
                        AddressNew.street = value;
                        break;
                    case "ext_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            AddressNew.ext_number = int.Parse(value);
                        }
                        break;
                    case "int_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            AddressNew.int_number = int.Parse(value);
                        }
                        break;
                    case "neighborhood":
                        AddressNew.neighborhood = value;
                        break;
                    case "latitud":
                        if (!string.IsNullOrEmpty(value))
                        {
                            AddressNew.latitud = decimal.Parse(value);
                        }
                        break;
                    case "longitude":
                        if (!string.IsNullOrEmpty(value))
                        {
                            AddressNew.longitude = decimal.Parse(value);
                        }
                        break;
                    case "state":
                        AddressNew.state = value;
                        break;
                    case "country":
                        AddressNew.country = value;
                        break;
                    case "city":
                        AddressNew.city = value;
                        break;
                    // Para Email
                    case "email_address":
                        EmailNew.email_address = value;
                        break;
                }
            }

            // Se crea el registor en tblAddress
            db.Address.Add(AddressNew);
            await db.SaveChangesAsync();

            // Se crea el registor en tblEmail
            db.Email.Add(EmailNew);
            await db.SaveChangesAsync();

            // Se crea el registro en tblContact
            ContactNew.address_id = AddressNew.id;
            ContactNew.email_id = EmailNew.id;
            db.Contact.Add(ContactNew);
            await db.SaveChangesAsync();

            // Creamos al usuario
            UserNew.name = formNewCarrier["name"];
            UserNew.auth = UserRoles.CARRIER;
            UserNew.status = true;
            UserNew.contact_id = ContactNew.id;
            db.User.Add(UserNew);
            await db.SaveChangesAsync();

            // Creamos el transportista
            Carrier CarrierNew = new Carrier();

            // Guardamos la RFC el mayusculas
            if (formNewCarrier["rfc"] != null)
            {
                CarrierNew.rfc = formNewCarrier["rfc"].ToUpper();
            }
            CarrierNew.name = formNewCarrier["name"];
            CarrierNew.address_id = AddressNew.id;
            CarrierNew.user_id = UserNew.id;

            db.Carrier.Add(CarrierNew);
            await db.SaveChangesAsync();

            return Json(CarrierNew, JsonRequestBehavior.AllowGet);
        }

        // Editar transportista
        [Route("~/ajax/carriers")]
        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> PutAjaxCarrier(FormCollection formNewCarrier)
        {
            if (string.IsNullOrEmpty(formNewCarrier["id"]) ||
                string.IsNullOrEmpty(formNewCarrier["name"]) ||
                string.IsNullOrEmpty(formNewCarrier["rfc"]) ||
                //formNewCarrier["zipcode"] == null ||
                string.IsNullOrEmpty(formNewCarrier["street"])
                //formNewCarrier["ext_number"] == null ||
                //formNewCarrier["int_number"] == null ||
                //formNewCarrier["neighborhood"] == null ||
                //formNewCarrier["latitud"] == null &&
                //formNewCarrier["longitude"] == null &&
                //formNewCarrier["state"] == null ||
                //formNewCarrier["country"] == null ||
                //formNewCarrier["city"] == null)
                )
            {
                return Json(new HandlerReturn(HttpStatusCode.BadRequest, true, "GE_03", "Los datos enviados no están completos o son inválidos, verifique su información."), JsonRequestBehavior.AllowGet);
            }

            // Validar que el transportista no existe
            IQueryable<Carrier> carrierToValidate = db.Carrier.Where(t => t.deleted_at == null);
            string rfc = formNewCarrier["rfc"].ToUpper();
            string name = formNewCarrier["name"];
            int id = int.Parse(formNewCarrier["id"]);
            carrierToValidate = carrierToValidate.Where(t => (t.name == name || t.rfc.ToUpper() == rfc) && t.id != id);

            Carrier ExistCarrier = await carrierToValidate.FirstOrDefaultAsync();

            if (ExistCarrier != null)
            {
                return Json(new HandlerReturn(HttpStatusCode.Found, true, "C_PA_01", "El RFC ya se usa en otro transportista, favor de validar."), JsonRequestBehavior.AllowGet);
            }
            // Termina Validar que el transportista no existe

            // Obtenemos las relaciones correspondientes a editar

            // Obtenemos el Transportista a editar
            Carrier Carrier = await db.Carrier.FindAsync(id);

            // Obtenemos la dirección relacionada
            Address Address = Carrier.address;

            // Obtenemos al usuario relacionada
            User User = Carrier.user;

            // Obtenemos al contacto relacionado
            //Contact Contact = User.contact;

            // Obtenemos al email relacionado
            //Email Email = Contact.email;

            foreach (var formKey in formNewCarrier.AllKeys)
            {
                var value = formNewCarrier[formKey];
                switch (formKey)
                {
                    // Para Transportista
                    case "name":
                        Carrier.name = value;
                        User.name = value;
                        break;
                    case "rfc":
                        Carrier.rfc = value.ToUpper();
                        break;
                    // Para Address
                    case "zipcode":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.zipcode = int.Parse(value);
                        }
                        break;
                    case "street":
                        Address.street = value;
                        break;
                    case "ext_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.ext_number = int.Parse(value);
                        }
                        break;
                    case "int_number":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.int_number = int.Parse(value);
                        }
                        break;
                    case "neighborhood":
                        Address.neighborhood = value;
                        break;
                    case "latitud":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.latitud = decimal.Parse(value);
                        }
                        break;
                    case "longitude":
                        if (!string.IsNullOrEmpty(value))
                        {
                            Address.longitude = decimal.Parse(value);
                        }
                        break;
                    case "state":
                        Address.state = value;
                        break;
                    case "country":
                        Address.country = value;
                        break;
                    case "city":
                        Address.city = value;
                        break;
                        // Para Email
                        //case "email_address":
                        //    Email.email_address = value;
                        //    break;
                }
            }

            db.Entry(Carrier).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Entry(User).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Entry(Address).State = EntityState.Modified;
            await db.SaveChangesAsync();

            //db.Entry(Email).State = EntityState.Modified;
            //await db.SaveChangesAsync();

            return Json(Carrier, JsonRequestBehavior.AllowGet);
        }

        // Borrar Transportista
        [Route("~/ajax/carriers")]
        [HttpDelete]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAjaxCarrier(int id)
        {
            Carrier carrier = await db.Carrier.FindAsync(id);


            User user = carrier.user;
            user.status = false;
            db.Entry(user).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Carrier.Remove(carrier);
            await db.SaveChangesAsync();


            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "El transportista ha sido eliminado con éxito."));
        }

        // END AJAX REQUEST //////////////////////////////////////////////////////////////

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
