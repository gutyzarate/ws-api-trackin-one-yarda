﻿using onecore.security.utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using WSApiTrackingOneYarda.Data;
using WSApiTrackingOneYarda.Models;
using IHttpActionResult = System.Web.Http.IHttpActionResult;
using RouteAttribute = System.Web.Http.RouteAttribute;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;

namespace WSApiTrackingOneYarda.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginApiController : ApiController
    {
        private LogisticsTrackingDBContext db = new LogisticsTrackingDBContext();
        private string encryptionKey = "0n3c0r33ncr1pt201804203i0qaw0e2e";

        // POST: api/Login
        [Route("")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostLogin(Login credentials)
        {
            var users = db.User.Where(s => s.username == credentials.username);

            if (users.Count() == 0)
            {
                return this.loginFailedResponse();
            }

            var user = users.First();

            string decryptedPassword = EncryptService.Decrypt(user.password, encryptionKey);

            
            if (!credentials.password.Equals(decryptedPassword))
            {
                return this.loginFailedResponse();
            }

            var authorizedRoleIds = new List<int?> {
                //UserRoles.CARRIER,
                UserRoles.TRAFFIC,
                UserRoles.YARD_MULE_DRIVER,
                //UserRoles.PROVIDER,
                //UserRoles.ADMINISTRATOR,
                //UserRoles.SUPER_ADMIN,
            };

            if (user.auth == null || !authorizedRoleIds.Contains(user.auth))
            {
                //Session.Abandon();
                return this.loginNotAllowedResponse();
            }

            if (user.is_yard_mule_driver)
            {
                ActivityLog activityLog = new ActivityLog
                {
                    log_name = "login",
                    description = "login",
                    subject_id = user.id,
                    subject_table = "tblUser",
                    causer_id = user.id,
                    causer_table = "tblUser",
                };
                db.ActivityLog.Add(activityLog);
                await db.SaveChangesAsync();

                user.activity_log_id = activityLog.id;
            }

            return Ok(user);
        }

        [HttpPost]
        [Route("~/api/me/login/comment")]
        public async Task<IHttpActionResult> PostMeLoginComment(ActivityLogComment activity)
        {
            ActivityLog activityLog = await db.ActivityLog.FindAsync(activity.id);

            activityLog.comments = activity.comment;

            db.Entry(activityLog).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Json(new HandlerReturn(HttpStatusCode.Conflict, true, "EDB", Convert.ToString(ex)));
            }

            return Json(new HandlerReturn(HttpStatusCode.NoContent, false, null, "Comentario guardado exitosamente"));
        }

        private IHttpActionResult loginFailedResponse()
        {
            return Json(new { Error = "El usuario o la contraseña son incorrectas. Favor de verificar su información" });
        }

        private IHttpActionResult loginNotAllowedResponse()
        {
            return Json(new { Error = "El nivel del usuario con el que intenta ingresar no tiene permisos de acceso al sistema." });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShippingScheduleExists(int id)
        {
            return db.ShippingSchedules.Count(e => e.id == id) > 0;
        }
    }
}
