﻿using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WSApiTrackingOneYarda.ActionFilters;
using WSApiTrackingOneYarda.Data;

namespace WSApiTrackingOneYarda
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Para evitar revision de migraciones, de manera temporal.
            Database.SetInitializer<LogisticsTrackingDBContext>(null);

            GlobalFilters.Filters.Add(new CurrentUserActionFilter(), 0);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HttpConfiguration config = GlobalConfiguration.Configuration;
        }
    }
}
